<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrer extends Model
{
    protected $fillable = ['name','status'];

    public function Groups()
    {
        return $this->hasMany('App\Group');
    }

}
