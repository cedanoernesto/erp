<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodAdmission extends Model
{
	protected $table = 'periods_admission';
    protected $fillable = [
        'start_date', 'end_date', 'status', 'institutions_id',
    ];

    public function institution() {
        return $this->belongsTo('App\Institution', 'institutions_id');
    }
}
