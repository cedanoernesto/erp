<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = ['name', 'status'];

    public function payment(){
        return $this->hasMany('App\Payment');
    }
}
