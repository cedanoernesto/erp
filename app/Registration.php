<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $fillable = [
        'type', 'initial_character', 'status'
    ];
    public function institution(){
        return $this->hasOne('App\Institution');
    }
}
