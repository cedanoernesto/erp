<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'status', 'institution_id', 'types_payment_id', 'currency_id',
    ];

    public function types_payment(){
        return $this->belongsTo('App\TypesPayment');
    }

    public function currency(){
        return $this->belongsTo('App\Currency');
    }


    public function institution(){
        return $this->belongsTo('App\Institution');
    }
}
