<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InscriptionPeriod extends Model
{
    protected $fillable=['start_date','end_date','status'];

    public function periods() {
        return $this->hasMany('App\Periods');
    }
}
