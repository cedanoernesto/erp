<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    protected $fillable = ['name', 'status', 'address', 'shift_id','registration_id', 'zip_code', 'city_id', 'email', 'neighborhood', 'phone', 'initial_character'];

    public function registration(){
        return $this->belongsTo('App\Registration');
    }

    public function shift(){
        return $this->belongsTo('App\Shift');
    }

    public function payment(){
        return $this->hasMany('App\Payment');
    }
    public function phone(){
        return $this->belongsTo('App\Phone');
    }
    public function city(){
        return $this->belongsTo('App\City');
    }

}
