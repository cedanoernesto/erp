<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['name', 'icon', 'status', 'level', 'parent_id', 'url'];

    public function modules() {
        return $this->hasMany('App\Module')->where('status', 1);
    }
}
