<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $fillable = ['name', 'floors', 'school_id'];

    public function rooms() {
        return $this->hasMany('App\Room');
    }

    public function schools() {
        return $this->belongsTo('App\Schools');
    }
}
