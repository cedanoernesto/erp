<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = [
        'name', 'country_id', 'status'
    ];

    public function countries(){
        return $this->belongsToMany('App\Country');
    }
}
