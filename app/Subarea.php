<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subarea extends Model
{
    protected $fillable = ['name', 'area_id']; # Variables que pueden ser manejadas por el controlador

    public function area() {
        return $this->belongsTo('App\Area');
    }

    # Relacion con la tabla rooms
    public function rooms(){
        return $this->hasMany('App\Room');
    }

    # Relacion con la tabla areas
    public function areas(){
        return $this->belongsTo('App\Area', 'area_id', 'id');
    }
}
