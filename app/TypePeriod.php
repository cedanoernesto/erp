<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypePeriod extends Model
{
    protected $fillable=['name','status'];

    public function periods() {
        return $this->hasMany('App\Periods');
    }
}
