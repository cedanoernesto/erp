<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infrastructure extends Model
{
    protected $fillable = ['name', 'ubication', 'rooms', 'floors']; # Variables que pueden ser manejadas por el controlador

    # Relacion con la tabla rooms
    public function room(){
        return $this->hasMany('App\Room')->where('status', 1);
    }
}
