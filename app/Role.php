<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'description']; # Variables que pueden ser manejadas por el controlador

    # Relacion con la tabla profiles
    public function profiles () {
        return $this->hasMany('App\Profile');
    }

    # Relacion con la tabla modules
    public function modules() {
        return $this->belongsToMany('App\Module', 'module_role');
    }
}
