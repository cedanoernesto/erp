<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $fillable = ['name','status','state_id'];

    public function states(){
        return $this->hasMany('App\State');
    }

    public function institution(){
        return $this->hasMany('App\Institution');
    }



}
