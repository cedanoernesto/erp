<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['name', 'description'];

    public function users() {
        return $this->hasMany('App\User');
    }

    public function roles() {
        return $this->belongsToMany('App\Role');
    }
}
