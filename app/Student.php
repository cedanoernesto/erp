<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['enrollment', 'aspirants_id', 'users_id'];

    public function users(){
      return $this->belongsTo('App\User');
    }

    public function group(){
        return $this->belongsToMany('App\Group');
    }
}
