<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['name', 'capacity', 'ubication', 'infrastructure_id', 'subarea_id']; # Variables que pueden ser manejadas por el controlador

    # Relacion con la tabla infrastructures
    public function infrastructures(){
        return $this->belongsTo('App\Infrastructure');
    }

    # Relacion con la tabla subareas
    public function subareas(){
        return $this->belongsTo('App\Subarea');
    }
}
