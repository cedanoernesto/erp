<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = ['name']; # Variables que pueden ser manejadas por el controlador

    # Relacion con la tabla Subareas
    public function subareas(){
        return $this->hasMany('App\Subarea');
    }
}
