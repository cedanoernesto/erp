<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    protected $fillable = [
        'type', 'status'
    ];
    public function institution(){
        return $this->hasMany('App\Institution');
    }
}
