<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	protected $table = 'courses';
	
    protected $fillable=['name','capacitance','status','periods_admission_id'];

}
