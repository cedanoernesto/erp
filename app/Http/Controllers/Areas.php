<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use App\Subarea;
use Validator;

class Areas extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     # Función para listar informacion de las áreas con paginacion
    public function index()
    {
        $areas = Area::where('status', 1)->paginate(8);
        return $areas;
    }
    # Función para listar informacion de las áreas sin paginacion
    public function allAreas()
    {
        $areas = Area::where('status', 1)->get();
        return $areas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     # Función para guardar una área
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45'
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
		
		if($this->checkName($request->name, 0)){
			return response()->json([
                'message' => 'Ya existe un área con el mismo nombre'
            ], 400);
		}

        $area = new Area;
        $area->fill($request->all());
        $area->save();

        return $area;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para mostrar un área en especifico.
    public function show($id)
    {
        $area = Area::where('status', 1)->find($id);
        return $area;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para actualizar un área en especifico.
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45'
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
		
		if($this->checkName($request->name, $id)){
			return response()->json([
                'message' => 'Ya existe un área con el mismo nombre'
            ], 400);
		}

        $area = Area::where('status', 1)->find($id);
        $area->fill($request->all());
        $area->save();

        return $area;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para elminiar de manera logica un área.
    public function destroy($id)
    {
        $var = $this->checkSubareas($id);
		if(count($var)>0)
		{
			return response()->json([
				'errors' => 'No se puede eliminar el área ya que cuenta con subáreas relacionadas. No. de Subáreas: '.count($var)
			], 400);
		}
		else
		{
			$area = Area::where('status', 1)->find($id);
			$area->status = 0;
			$area->save();

			return $area;
		}
    }

    # Función para obtener las subareas con las que cuenta determinada área.
    public function getSubareas($id)
    {
        $subareas;
        if (Area::where('status', 1)->find($id) != NULL)    {
            $subareas = (string) Area::where('status', 1)->find($id)->subareas;
        }   else {
            return response()->json([
                'Message' => "Those subareas don't exists"
            ], 404);
        }
        return $subareas;
    }
	
	public function checkName($area, $id)
	{
		if($id != 0)
		{
			$area = Area::where([['status','=', 1], ['name', 'like', $area], ['id', '<>', $id]])->get();
		}
		else
		{
			$area = Area::where([['status','=', 1], ['name', 'like', $area]])->get();
		}
		$flag = false;
		if(count($area)>0) $flag=true;
		return $flag;
	}
	
	public function checkSubareas ($id)
	{
		$subareas = Subarea::where([
		['status', '=', 1],
		['area_id', '=', $id]
		])->get();
		return $subareas;
	}
}
