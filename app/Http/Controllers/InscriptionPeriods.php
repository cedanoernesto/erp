<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InscriptionPeriod;
use Validator;

class InscriptionPeriods extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inscription=InscriptionPeriod::where('status',1)->get();
        return $inscription;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),
            [
             'start_date' =>'required|date',
             'end_date' => 'required|date'
            ]);

        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $incrip=new InscriptionPeriod;
        $incrip->fill($request->all());
        $incrip->save();

        return $incrip;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $in=InscriptionPeriod::where('status',1)->find($id);
        return $in;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $validator=Validator::make($request->all(),
            [
              'start_date' =>'required|date',
              'end_date' => 'required|date'
            ]);

            if($validator->fails())
              {
                  return response()->json([
                      'error' => $validator->messages()
                  ], 400);
              }

        $in=InscriptionPeriod::where('status',1)->find($id);
        $in->fill($request->all());
        $in->save();
        return $in;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $in=InscriptionPeriod::where('status',1)->find($id);
        $in->status=0;
        $in->save();

        return $in;
    }
}
