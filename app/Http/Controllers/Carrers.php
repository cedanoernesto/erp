<?php

namespace App\Http\Controllers;

use App\Carrer;
use App\Group;
use Illuminate\Http\Request;
use Validator;

class Carrers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carrers = Carrer::where('status', 1)->get();
        foreach ($carrers as $carrer) {
            $carrer->groups_count = Group::where('carrer_id', '=', $carrer->id)->count();
        }
        return $carrers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $carrer = new Carrer();
        $carrer->fill($request->all());
        $carrer->save();

        return $carrer;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $carrer = Carrer::where('status', 1)->find($id);
        return $carrer;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:45'
        ]);

        $carrer = Carrer::where('status', 1)->find($id);
        $carrer->fill($request->all());
        $carrer->save();
        return $carrer;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carrer = Carrer::where('status', 1)->find($id);
        $carrer->status = 0;
        $carrer->save();
        return $carrer;
    }
}
