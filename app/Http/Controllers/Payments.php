<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use Validator;
class Payments extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::where('status','1')->with('currency','types_payment','institution')->get();
        return $payments;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            //'status' => 'required|numeric',
            'institution_id' => 'required|numeric',
            'types_payment_id' => 'required|numeric',
            'currencies_id' => 'required|numeric',
        ]);
        $payment = new Payment;
        $payment->fill($request->all());
        $payment->save();

        return $payment;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = Payment::where('status', 1)->find($id);
        return $payment;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            //'status' => 'required|numeric',
            'institution_id' => 'required|numeric',
            'types_payment_id' => 'required|numeric',
            'currency_id' => 'required|numeric',
        ]);

        $payment = Payment::where('status', 1)->find($id);
        $payment->fill($request->all());
        $payment->save();
        return $payment;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment = Payment::where('status', 1)->find($id);
        $payment->status = 0;
        $payment->save();

        return $payment;
    }
}
