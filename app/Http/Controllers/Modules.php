<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use Validator;

class Modules extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    # Función para cargar los modulos con paginación.
    public function index()
    {
        $modules = Module::with('section')
            ->where('status', 1)
            //->where('editable', 1)
            ->paginate(5);
        return $modules;
    }

    # Función para cargar todos los registros en la tabla modules
    public function indexAll()
    {
        $modules = Module::with('section')
            ->where('status', 1)
            ->get();
        return $modules;
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**all
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */

    # Función para guardar un modulo.
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:191',
            'description' => 'required|min:6',
            'section_id' => 'required',
            'url' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
		
		$messages=[];
		if($this->checkName($request->name, 0))
		{
			$messages[] = 'Ya existe un módulo con el mismo nombre';
		}
		if($this->checkUrl($request->url, 0))
		{
			$messages[] = 'Ya existe un módulo con la misma url';
		}
		if(count($messages)>0){
			return response()->json([
				'errors' => $messages
			], 400);
		}
		

        $module = new Module;
        $module->fill($request->all());
        $module->icon = '/img/image.png';

        if ($request->hasFile('icon') && $request->file('icon')->isValid()) {
            $path = '/storage/'.$request->icon->store('modules_icons', 'public');
            $module->icon = $path;
        }

        $module->save();

        return $module;
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    # Función para cargar un modulo en especifico.
    public function show($id)
    {
        $module = Module::where('status', 1)->find($id);
        return $module;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    # Función para actualizar información de un modulo en especifico
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:191',
            'description' => 'required|min:6',
            'section_id' => 'required',
            'url' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
		
		$messages=[];
		if($this->checkName($request->name, $id))
		{
			$messages[] = 'Ya existe un módulo con el mismo nombre';
		}
		if($this->checkUrl($request->url, $id))
		{
			$messages[] = 'Ya existe un módulo con la misma url';
		}
		if(count($messages)>0){
			return response()->json([
				'errors' => $messages
			], 400);
		}

        $module = Module::where('status', 1)->find($id);
        $module->fill($request->all());

        if ($request->hasFile('icon') && $request->file('icon')->isValid()) {
            $path = '/storage/'.$request->icon->store('modules_icons', 'public');
            $module->icon = $path;
        }

        $module->save();
        return $module;
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    # Función para borrar de manera logica un modulo en especifico.
    public function destroy($id)
    {
        $module = Module::where('status', 1)->find($id);
        $module->status = 0;
        $module->save();

        return $module;
    }
	
	public function checkName($module, $id)
	{
		if($id != 0)
		{
			$module = Module::where([['status','=', 1], ['name', 'like', $module], ['id', '<>', $id]])->get();
		}
		else
		{
			$module = Module::where([['status','=', 1], ['name', 'like', $module]])->get();
		}
		$flag = false;
		if(count($module)>0) $flag=true;
		return $flag;
	}
	
	public function checkUrl($url, $id)
	{
		if($id != 0)
		{
			$url = Module::where([['status','=', 1], ['url', 'like', $url], ['id', '<>', $id]])->get();
		}
		else
		{
			$url = Module::where([['status','=', 1], ['url', 'like', $url]])->get();
		}
		$flag = false;
		if(count($url)>0) $flag=true;
		return $flag;
	}
}
