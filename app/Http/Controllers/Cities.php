<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use Validator;

class Cities extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cities = City::where('status',1)->get();
        return $cities;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'countries_id' => 'required|numeric',
            'states_id' => 'required|numeric',
            'states_countries_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $city = new City;
        $city->fill($request->all());
        $city->save();
        return $city;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = City::where('status', 1)->find($id);
        return $city;
    }
    
    public function getByState($state_id) {
        $cities = City::where('state_id', $state_id)->get();
        return $cities;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'countries_id' => 'required|numeric',
            'states_id' => 'required|numeric',
            'states_countries_id' => 'required|numeric',
        ]);

        $city = City::where('status', 1)->find($id);
        $city->fill($request->all());
        $city->save();
        return $city;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::where('status', 1)->find($id);
        $city->status = 0;
        $city->save();

        return $city;
    }
}
