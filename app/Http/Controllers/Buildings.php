<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Building;
use Validator;

class Buildings extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index ()
    {
        $buildings = Building::where('status', 1)->paginate(8);
        return $buildings;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:45',
            'floors' => 'required',
            'school_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $building = new Building;
        $building->fill($request->all());
        $building->save();

        return $building;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id)
    {
        $building = Building::where('status',1)->find($id);
        return $building;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:45',
            'floors' => 'required',
            'school_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $building = Building::where('status', 1)->find($id);
        $building->fill($request->all());
        $building->save();

        return $building;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $building = Buildings::where('status', 1)->find($id);
        $building->status = 0;
        $module->save();

        return $module;
    }

    public function loadRooms ($id) {
        $rooms;
        if (Building::where('status', 1)->find($id) != NULL)    {
            $rooms = (string) Building::where('status', 1)->find($id)->rooms;
        }   else {
            return response()->json([
                'Message' => "That role doesn't exist"
            ], 404);
        }
        return $rooms;
    }
}
