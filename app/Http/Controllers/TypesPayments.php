<?php

namespace App\Http\Controllers;

use App\TypesPayment;
use Illuminate\Http\Request;
use Validator;


class TypesPayments extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types_payments = TypesPayment::where('status', 1)->get();
        return $types_payments;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $types_payment = new TypesPayment;
        $types_payment->fill($request->all());
        $types_payment->save();

        return $types_payment;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $types_payment = TypesPayment::where('status', 1)->find($id)->get();
        return $types_payment;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:45',
            'description' => 'required',
        ]);

        $types_payment = TypesPayment::where('status', 1)->find($id);
        $types_payment->fill($request->all());
        $types_payment->save();
        return $types_payment;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $types_payment = TypesPayment::where('status', 1)->find($id);
        $types_payment->status = 0;
        $types_payment->save();

        return $types_payment;
    }
}
