<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room_type;
use Validator;

class Room_types extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $room_types = Room_type::where('status', 1)->paginate(8);
        return $room_types;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $room_type = new Room_type;
        $room_type->fill($request->all());
        $room_type->save();

        return $room_type;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $room_type = Room_type::where('status', 1)->find($id);
        return $room_type;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request, $id) {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $room_type = Room_type::where('status', 1)->find($id);
        $room_type->fill($request->all());
        $room_type->save();

        return $room_type;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy ($id) {
        $room_type = Room_type::where('status', 1)->find($id);
        $room_type->status = 0;
        $room_type->save();

        return $room_type;
    }

    public function showRooms ($id) {
        $rooms;
        if (Room_type::where('status', 1)->find($id) != NULL)    {
            $rooms = (string) Room_type::where('status', 1)->find($id)->rooms;
        }   else {
            return response()->json([
                'Message' => "That type of room doesn't exist"
            ], 404);
        }
        return $rooms;
    }
}
