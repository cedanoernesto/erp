<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Validator;

class Students extends Controller
{
    public function index(){
      $students = Student::where('status',1)->with('enrollment')->get();
      return $students;
    }

    public function store(Request $request){
      $validator = Validator::make($request->all(), [
          'enrollment' => 'required|max:45',
          'aspirants_id' => 'required|numeric',
          'users_id' => 'required|numeric'
      ]);
      if ($validator->fails()) {
          return response()->json([
              'error' => $validator->messages()
          ], 400);
      }

      $student = new Student;
      $student->fill($request->all());
      $student->save();

      return $student;
    }

    public function show($id)
    {
        $student = Student::where('status', 1)->find($id);
        return $student;
    }
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'enrollment' => 'required|max:45',
            'aspirants_id' => 'required|numeric',
            'users_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
        $student = Student::where('status', 1)->find($id);
        $student->fill($request->all());
        $student->save();
        return $student;
    }

    public function destroy($id)
    {
        $student = Student::where('status', 1)->find($id);
        $student->status = 0;
        $student->save();

        return $student;
    }
}
