<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Validator;
use App\UserImage;

class Users extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('profile', 'image')
            ->where('status', 1)
            //->where('editable', 1)
            ->paginate(8);
        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**all
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required|min:3|max:16',
            'email' => 'required|email|max:191',
            'password' => 'required|max:50',
            //'profile_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        if($this->checkName($request->email, 0)){
			return response()->json([
                'error' => [
                    'email' => 'Ya existe un usuario con el mismo email'
                ]
            ], 400);
		}

        $user = new User;
        $user->fill($request->all());
        $user->password = Hash::make($request->password);
        $user->save();

        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $user_image = new UserImage;

            $path = $request->photo->store('user_images', 'public');
            $user_image->url_image = $path;
            $user_image->user_id = $user->id;
            $user_image->save();

            $user->user_image = $user_image;
        }

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('profile', 'image')->where('status', 1)->find($id);
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required|min:3|max:16',
            'email' => 'required|email|max:191',
            'password' => 'max:50',
            //'profile_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        if($this->checkName($request->email, $id)){
			return response()->json([
                'error' => [
                    'email' => 'Ya existe un usuario con el mismo email'
                ]
            ], 400);
		}

        $user = User::where('status', 1)->find($id);
        $user->fill($request->all());
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }
        $user->save();

        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            if ($user->image) {
                $user->image->status = 0;
                $user->image->save();
            }

            $user_image = new UserImage;
            $path = $request->photo->store('user_images', 'public');
            $user_image->url_image = $path;
            $user_image->user_id = $user->id;
            $user_image->save();

            $user->image = $user_image;
        }

        return User::with('profile', 'image')->where('status', 1)->find($user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('status', 1)->find($id);
        $user->status = 0;
        $user->save();

        return $user;
    }

    public function checkName($email, $id)
	{
		if($id != 0)
		{
			$email = User::where([['status','=', 1], ['email', 'like', $email], ['id', '<>', $id]])->get();
		}
		else
		{
			$email = User::where([['status','=', 1], ['email', 'like', $email]])->get();
		}
		$flag = false;
		if(count($email)>0) $flag=true;
		return $flag;
	}
}
