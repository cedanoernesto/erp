<?php

namespace App\Http\Controllers;
use App\Period;
use Illuminate\Http\Request;
use Validator;

class Periods extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $peri=Period::with('type_period', 'inscription')->where('status',1)->paginate(8);
        return $peri;
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),
        [
            'name' => 'required',
            'start_date' =>'required|date',
            'end_date' => 'required|date',
            'inscription_period_id' => 'required|numeric',
            'type_period_id' => 'required|numeric'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $peri=new Period;
        $peri->fill($request->all());
        $peri->save();

        return $peri;
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $peri=Period::where('status',1)->find($id);
        return $peri;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        $validator=Validator::make($request->all(),
        [
            'name' => 'required',
            'start_date' =>'required',
            'end_date' => 'required',
            'inscription_period_id' => 'required',
            'type_period_id' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $peri=Period::where('status',1)->find($id);
        $peri->fill($request->all());
        $peri->save();

        return $peri;
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $peri=Period::where('status',1)->find($id);
        $peri->status=0;
        $peri->save();

        return $peri;
    }
}
