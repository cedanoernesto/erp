<?php

namespace App\Http\Controllers;
use App\PeriodAdmission;
use Illuminate\Http\Request;
use Validator;

class PeriodsAdmission extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peri = PeriodAdmission::where('status', 1)->get();
        return $peri;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),
        [
            'start_date' => 'required|date',
            'end_date' =>'required|date',
            'institution_id' => 'required|numeric'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $peri=new PeriodAdmission;
        $peri->fill($request->all());
        $peri->save();

        return $peri;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peri=PeriodAdmission::where('status',1)->find($id);
        return $peri;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),
        [
            'start_date' => 'required|date',
            'end_date' =>'required|date',
            'institution_id' => 'required|numeric'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $peri=PeriodAdmission::where('status',1)->find($id);
        $peri->fill($request->all());
        $peri->save();

        return $peri;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peri=PeriodAdmission::where('status',1)->find($id);
        $peri->status=0;
        $peri->save();

        return $peri;
    }
}
