<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subarea;
use Validator;

class Subareas extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     # Función para cargar informacion de las subareas con paginación
    public function index()
    {
        $subareas = Subarea::with('area')->where('status', 1)->paginate(8);
        return $subareas;
    }

    # Función para listar todas las subareas.
    public function allSubareas()
    {
        return Subarea::where('status', 1)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     # Función para guardar una subarea.
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'area_id' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
		
		if($this->checkName($request->name, 0)){
			return response()->json([
                'message' => 'Ya existe una subárea con el mismo nombre'
            ], 400);
		}

        $subarea = new Subarea;
        $subarea->fill($request->all());
        $subarea->save();

        return $subarea;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para mostrar una subarea en especifico.
    public function show($id)
    {
        $subarea = Subarea::where('status', 1)->find($id);
        return $subarea;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para actualizar información de una subarea en especifico
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'area_id' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
		
		if($this->checkName($request->name, $id)){
			return response()->json([
                'message' => 'Ya existe una subárea con el mismo nombre'
            ], 400);
		}

        $subarea = Subarea::where('status', 1)->find($id);
        $subarea->fill($request->all());
        $subarea->save();

        return $subarea;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para borrar logicamente una subarea en especifico.
    public function destroy($id)
    {
        $subarea = Subarea::where('status', 1)->find($id);
        $subarea->status = 0;
        $subarea->save();

        return $subarea;
    }

    # Función para obtener los cuartos que se relacionan con una subarea.
    public function getRooms($id)
    {
        $rooms;
        if (Subarea::where('status', 1)->find($id) != NULL) {
            $rooms = Subarea::where('status', 1)->find($id)->rooms;
        } else {
            return response()->json([
                'Message' => "Those rooms don't exists"
            ], 404);
        }
        return $rooms;
    }
	
	public function checkName($subarea, $id)
	{
		$result;
		if($id != 0)
		{
			$result = Subarea::where([['status','=', 1], ['name', 'like', $subarea], ['id', '<>', $id]])->get();
		}
		else
		{
			$result = Subarea::where([['status','=', 1], ['name', 'like', $subarea]])->get();
		}
		$flag = false;
		if(count($result)>0) $flag=true;
		return $flag;
	}
}
