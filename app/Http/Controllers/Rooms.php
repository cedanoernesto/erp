<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use Validator;

class Rooms extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     # Función para cargar los registros de cuartos con paginación.
    public function index()
    {
        $rooms = Room::with('infrastructures')->where('status', 1)->paginate(8);
        return $rooms;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     # Función para guardar un cuarto.
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:45',
            'capacity' => 'required|numeric',
            'ubication' => 'required|max:45',
            'infrastructure_id' => 'required|numeric',
            'subarea_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
		
		if($this->checkName($request->name, 0)){
			return response()->json([
                'message' => 'Ya existe un cuarto con el mismo nombre'
            ], 400);
		}

        $room = new Room;
        $room->fill($request->all());
        $room->save();

        return $room;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para mostrar un cuarto en especifico.
    public function show($id)
    {
        $room = Room::where('status', 1)->find($id);
        return $room;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para actualizar información de un cuarto en especifico.
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:45',
            'capacity' => 'required|numeric',
            'ubication' => 'required|max:45',
            'infrastructure_id' => 'required|numeric',
            'subarea_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
		
		if($this->checkName($request->name, $id)){
			return response()->json([
                'message' => 'Ya existe un cuarto con el mismo nombre'
            ], 400);
		}

        $room = Room::where('status', 1)->find($id);
        $room->fill($request->all());
        $room->save();

        return $room;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para eliminar de manera logica un cuarto en especifico.
    public function destroy($id)
    {
        $room = Room::where('status', 1)->find($id);
        $room->status = 0;
        $room->save();

        return $room;
    }
	
	public function checkName($room, $id)
	{
		if($id != 0)
		{
			$room = Room::where([['status','=', 1], ['name', 'like', $room], ['id', '<>', $id]])->get();
		}
		else
		{
			$room = Room::where([['status','=', 1], ['name', 'like', $room]])->get();
		}
		$flag = false;
		if(count($room)>0) $flag=true;
		return $flag;
	}
}
