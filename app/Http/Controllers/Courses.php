<?php

namespace App\Http\Controllers;
use App\Course;
use Illuminate\Http\Request;
use Validator;

class Courses extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $course=Course::where('status',1)->get();
        return $course;
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),
        [
            'name' => 'required|max:45',
            'capacitance' =>'required|max:45',
            'periods_admission_id' => 'required|numeric'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $course=new Course;
        $course->fill($request->all());
        $course->save();

        return $course;
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $course=Course::where('status',1)->find($id);
        return $course;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        $validator=Validator::make($request->all(),
        [
            'name' => 'required|max:45',
            'capacitance' =>'required|max:45',
            'periods_admission_id' => 'required|numeric'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $course=Course::where('status',1)->find($id);
        $course->fill($request->all());
        $course->save();

        return $course;
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $course=Course::where('status',1)->find($id);
        $course->status=0;
        $course->save();

        return $course;
    }
}
