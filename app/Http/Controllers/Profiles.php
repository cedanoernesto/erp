<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Role;
use Validator;


class Profiles extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $profiles = Profile::with('roles')->where('status', 1)->get();
        return $profiles;
    }

    public function index_page() {
        $profiles = Profile::with('roles')->where('status', 1)->paginate(8);
        return $profiles;
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**all
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $profile = new Profile;
        $profile->fill($request->all());
        $profile->save();

        $profile->roles()->detach();
        $profile->roles()->attach($request->roles);

        return $profile;
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $profile = Profile::where('status', 1)->find($id);
        return $profile;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required'
        ]);

        $profile = Profile::where('status', 1)->find($id);
        $profile->fill($request->all());

        $profile->roles()->detach();
        $profile->roles()->attach($request->roles);

        $profile->save();
        return $profile;
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $profile = Profile::where('status', 1)->find($id);
        $profile->status = 0;
        $profile->save();

        return $profile;
    }

    public function assign(Request $request, $id) {
        $profile = Profile::where('status', 1)->find($id);
        $profile->roles()->detach();
        $profile->roles()->attach($request->roles);

        return ['success' => true];
    }
}
