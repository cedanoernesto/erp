<?php

namespace App\Http\Controllers;
use App\TypePeriod;
use Illuminate\Http\Request;
use Validator;

class TypePeriods extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type=TypePeriod::where('status',1)->get();
        return $type;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),
            [
             'name' => 'required'
            ]);


        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $type=new TypePeriod;
        $type->fill($request->all());
        $type->save();

        return $type;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type=TypePeriod::where('status',1)->find($id);

        return $type;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validator=Validator::make($request->all(),
        [
          'name' => 'required'
        ]);

        if($validator->fails())
          {
              return response()->json([
                  'error' => $validator->messages()
              ], 400);
          }


        $type=TypePeriod::where('status',1)->find($id);
        $type->fill($request->all());
        $type->save();

        return $type;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type=TypePeriod::where('status',1)->find($id);
        $type->status=0;
        $type->save();

        return $type;
    }
}
