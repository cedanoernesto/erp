<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Validator;

class Roles extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    # Función para listar todos los roles.
    public function index()
    {
        $roles = Role::where('status', 1)->get();
        return $roles;
    }

    # Función para listar roles con paginacion.
    public function index_page()
    {
        $roles = Role::where('status', 1)->paginate(8);
        return $roles;
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**all
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */

    # Función para guardar un rol.
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $role = new Role;
        $role->fill($request->all());
        $role->save();

        return $role;
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    # Función para mostrar un rol en especifico.
    public function show($id)
    {
        $role = Role::where('status', 1)->find($id);
        return $role;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    # Función para actualizar la información de un rol en especifico.
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required'
        ]);

        $role = Role::where('status', 1)->find($id);
        $role->fill($request->all());
        $role->save();
        return $role;
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    # Función para eliminar logicamente un rol en especifico.
    public function destroy($id)
    {
        $role = Role::where('status', 1)->find($id);
        $role->status = 0;
        $role->save();

        return $role;
    }

    # Función para asignar modulos a un determinado rol.
    public function assign(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            //'module_id' => 'required',
            'role_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $role = Role::where('status', 1)->find($id);
        $role->modules()->detach();
        $role->modules()->attach($request->module_id);

        return ['success' => true];
    }

    # Función para cargar los modulos que fueron asignados a un determinado rol.
    public function loadModules ($id) {
        $role_modules;
        if (Role::where('status', 1)->find($id) != NULL)    {
            $role_modules = (string) Role::where('status', 1)->find($id)->modules;
        }   else {
            return response()->json([
                'Message' => "That role doesn't exist"
            ], 404);
        }
        return $role_modules;
    }
}
