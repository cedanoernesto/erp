<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Infrastructure;
use App\Room;
use Validator;

class Infrastructures extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     # Función para listar con paginado las infraestructuras
    public function index()
    {
        $infraestructures = Infrastructure::where('status', 1)->paginate(8);
        return $infraestructures;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     # Función para guardar una infraestructura.
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' =>'required|max:45',
            'ubication' =>'required|max:45',
            'rooms' =>'required|numeric',
            'floors' =>'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
		
		if($this->checkName($request->name, 0)){
			return response()->json([
                'message' => 'Ya existe una infraestructura con el mismo nombre'
            ], 400);
		}

        $infraestructure = new Infrastructure;
        $infraestructure->fill($request->all());
        $infraestructure->save();

        return $infraestructure;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para mostrar una infraestructura en especifico.
    public function show($id)
    {
        $infraestructure = Infrastructure::where('status', 1)->find($id);
        return $infraestructure;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para actualizar informacion de una infraestructura en espicifico.
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' =>'required|max:45',
            'ubication' =>'required|max:45',
            'rooms' =>'required|numeric',
            'floors' =>'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }
		
		if($this->checkName($request->name, $id)){
			return response()->json([
                'message' => 'Ya existe una infraestructura con el mismo nombre'
            ], 400);
		}

        $infraestructure = Infrastructure::where('status', 1)->find($id);
        $infraestructure->fill($request->all());
        $infraestructure->save();

        return $infraestructure;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     # Función para borrar logicamente una infraestructura.
    public function destroy($id)
    {
        $infraestructure = Infrastructure::where('status', 1)->find($id);
        $infraestructure->status = 0;
        $infraestructure->save();

        return $infraestructure;
    }

    # Función para mostrar los cuartos de una infraestructura.
    public function getRoom($id)
    {
        /*$rooms = Room::with('infrastructures')->where('status', 1)->where('infrastructure_id', $id)->get();
        return $rooms;*/
        $rooms = Infrastructure::with('room')->where('status', 1)->where('id', $id)->get()[0];
        return $rooms;
    }
	
	public function checkName($infrastructure, $id)
	{
		if($id != 0)
		{
			$infrastructure = Infrastructure::where([['status','=', 1], ['name', 'like', $infrastructure], ['id', '<>', $id]])->get();
		}
		else
		{
			$infrastructure = Infrastructure::where([['status','=', 1], ['name', 'like', $infrastructure]])->get();
		}
		$flag = false;
		if(count($infrastructure)>0) $flag=true;
		return $flag;
	}
}
