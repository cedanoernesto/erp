<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Institution;
use App\Currency;
use App\Registration;
use App\City;
use App\State;
use App\Country;
use App\TypesPayment;
use App\Phone;
use App\Shift;
use App\Payment;
use Validator;
class Institutions extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $institutions = Institution::where('status', 1)->with('registration','city','shift')->get();
        foreach ($institutions as $institution) {
            $institution->state = State::where('status', 1)->find($institution->city["state_id"]);
            $institution->country = Country::where('status', 1)->find($institution->state->country_id);
            $institution->phones = Phone::where([['status', 1], ['institution_id', $institution->id]])->get();
            $institution->payment = Payment::where('institution_id', $institution->id)
                ->with('currency', 'types_payment')->get();
        }
        return $institutions;
    }

    public function fillViewInstitution ()
    {
        $institutions = Institution::where('status', 1)->with('registration','city','shift')->get();
        foreach ($institutions as $institution) {
            $institution->state = State::where('status', 1)->find($institution->city["state_id"]);
            $institution->country = Country::where('status', 1)->find($institution->state->country_id);
            $institution->phones = Phone::where([['status', 1], ['institution_id', $institution->id]])->get();
        }
        $currencies = Currency::where('status', 1)->get();
        $registrations = Registration::where('status', 1)->get();
        $cities = City::where('status',1)->get();
        $state = State::where('status', 1)->get();
        $countries = Country::all();
        $payments = TypesPayment::all();
        $shifts = Shift::all();

        return [$institutions, $currencies, $registrations, $cities, $state, $countries, $payments, $shifts];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'email' => 'required|email',
            'address' => 'required',
            'neighborhood' => 'required',
            'phone' => 'required|max:13',
            'initial_character' => 'required',
            'shift_id' => 'required|numeric',
            'registration_id' => 'required||numeric',
            'zip_code' => 'required',
            'city_id' => 'required||numeric',
            'shift_id' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $institution = new Institution;
        $institution->fill($request->all());
        $institution->image_url = '';

        if ($request->hasFile('image_url') && $request->file('image_url')->isValid()) {
            $path = $request->image_url->store('institution_images', 'public');
            $institution->image_url = $path;
        }

        $institution->save();

        return $institution;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $institution = Institution::where('status', 1)->with('registration','city','shift')->find(1);
        return $institution;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'email' => 'required|email',
            'address' => 'required',
            'neighborhood' => 'required',
            'initial_character' => 'required',
            'phone' => 'required|max:13',
            'shift_id' => 'required|numeric',
            'registration_id' => 'required||numeric',
            'zip_code' => 'required',
            'city_id' => 'required||numeric',
            'shift_id' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $institution = Institution::where('status', 1)->find($id);
        $institution->fill($request->all());

        if ($request->hasFile('image_url') && $request->file('image_url')->isValid()) {
            $path = $request->image_url->store('institution_images', 'public');
            $institution->image_url = $path;
        }
        $institution->save();
        return $institution;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $institution = Institution::where('status', 1)->find($id);
        $institution->status = 0;
        $institution->save();

        return $institution;
    }
}
