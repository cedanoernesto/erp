<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use App\Module;
use JWTAuth;
use DB;
use Validator;

class Sections extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $sections = Section::where('status', 1)
            //->where('editable', 1)
            ->paginate(8);
        return $sections;
    }

    public function indexAll()
    {
        $sections = Section::where('status', 1)
            ->get();
        return $sections;
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**all
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $section = new Section;
        $section->fill($request->all());
        $section->icon = '';
        $section->level = 0;

        if ($request->hasFile('icon') && $request->file('icon')->isValid()) {
            $path = '/storage/'.$request->icon->store('sections_icons', 'public');
            $section->icon = $path;
        }

        $section->save();

        return $section;
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $section = Section::where('status', 1)->find($id);
        return $section;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $section = Section::where('status', 1)->find($id);
        $section->fill($request->all());
        $section->level = 0;

        if ($request->hasFile('icon') && $request->file('icon')->isValid()) {
            $path = '/storage/'.$request->icon->store('sections_icons', 'public');
            $section->icon = $path;
        }

        $section->save();
        return $section;
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
		$var = $this->checkModules($id);
		if(count($var)>0)
		{
			return response()->json([
				'errors' => 'No se puede eliminar la sección ya que cuenta con módulos relacionados'
			], 400);
		}
		else
		{
			$section = Section::where('status', 1)->find($id);
			$section->status = 0;
			$section->save();
			return $section;
		}
    }

    public function menuAll() {
        $sections = Section::with('modules')->where('status', 1)->get();
        return $sections;
    }

    public function menu() {
        $sections = Section::where('status', 1)->get();
        $user = JWTAuth::parseToken()->toUser();
        $roles = array_map(function ($role) {
            return $role['id'];
        }, $user->profile->roles->toArray());

        $menu = [];

        foreach ($sections as $index => $section) {
            $sections[$index]['modules'] = DB::table('modules')
                ->join('module_role', 'modules.id', '=', 'module_role.module_id')
                ->select('modules.*')
                ->where('modules.status', 1)
                ->whereIn('module_role.role_id', $roles)
                ->where('modules.section_id', $section->id)
                ->get();

            if (count($sections[$index]['modules']) > 0) {
                $menu[] = $sections[$index];
            }
        }

        return $menu;
    }
	
	public function checkModules ($id)
	{
		$modules = Module::where([
		['status', '=', 1],
		['section_id', '=', $id]
		])->get();
		return $modules;
	}
}
