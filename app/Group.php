<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name','status','carrer_id','period_id'];

    public function carrer()
    {
        return $this->belongsTo('App\Carrer');
    }

    public function period(){

        return $this->belongsTo('App\Period');
    }

    public function student(){
        return $this->hasMany('App\Student');
    }

}
