<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = ['name', 'description', 'url', 'icon', 'section_id']; # Variables que pueden ser manejadas por el controlador

    # Relacion con la tabla roles
    public function roles() {
        return $this->belongsToMany('App\Role', 'module_role');
    }

    # Relacion con la tabla section
    public function section() {
        return $this->belongsTo('App\Section');
    }
}
