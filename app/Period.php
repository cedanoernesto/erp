<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $fillable=['name','start_date','end_date','status','inscription_period_id','type_period_id'];

    public function inscription() {
        return $this->belongsTo('App\InscriptionPeriod', 'inscription_period_id');
    }

    public function type_period() {
        return $this->belongsTo('App\TypePeriod');
    }

    public function groups(){

        return $this->hasMany('App\TypePeriod');

    }
}
