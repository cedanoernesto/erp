<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $fillable = [
        'number', 'status', 'institution_id'
    ];
    public function institutions(){
        return $this->hasMany('App\Institution');
    }
}
