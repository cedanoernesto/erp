<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypesPayment extends Model
{
    protected $fillable = ['name', 'description','status'];

    public function payment(){
        return $this->hasMany('App\Payment');
    }
}
