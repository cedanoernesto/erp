<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/modules', function () {
    return view('modules/modules');
});

Route::get('/assign', function () {
    return view('modules/assign');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/roles', function () {
    return view('roles/index');
});

Route::get('/roles/index', function () {
    return view('roles/index');
});

Route::get('/profiles', function () {
    return view('profiles/index');
});

Route::get('/periods',function(){
    return view('periods/index');
});

Route::get('/profiles/index', function () {
    return view('profiles/index');
});

Route::get('/institutions', function () {
    return view('institutions/index');
});

Route::get('/institutions/index', function () {
    return view('institutions/index');
});

Route::get('/groups/students', function () {
    return view('groups/students');
});

Route::get('/users', function () {
    return view('users/index');
});

Route::get('/users/index', function () {
    return view('users/index');
});

Route::get('/users/new', function () {
    return view('users/new');
});

Route::get('/users/1', function () {
    return view('users/show');
});

Route::get('/users/1/edit', function () {
    return view('users/edit');
});

Route::get('/sections', function () {
    return view('sections/index');
});

Route::get('/sections/index', function () {
    return view('sections/index');
});

Route::get('/sections/new', function () {
    return view('sections/new');
});

Route::get('/sections/1', function () {
    return view('sections/show');
});

Route::get('/sections/1/edit', function () {
    return view('sections/edit');
});

Route::get('/', function () {
    return view('modules/index');
});
///// Generar esta ruta para poder tomar los datos de la base de datos
Route::get('/modules/show', 'Modules@index');

Route::get('/infrastructures', function () {
    return view('infrastructures/index');
});

Route::get('/areas', function () {
    return view('areas/index');
});

Route::get('/subareas', function () {
    return view('subareas/index');
});
