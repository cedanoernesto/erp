<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\User;
use App\Profile;

Route::post('authenticate', 'Authentication@authenticate');

Route::middleware('jwt.auth')->get('/me', function (Request $request) {
    $user = JWTAuth::parseToken()->toUser();
    $user->image = $user->image;
    return $user;
});

Route::resource('/users', 'Users', [ 'except' => [ 'create', 'edit' ] ]);

/*Rutas utilizadas para las funciones de roles*/
Route::get('/roles/paginate', 'Roles@index_page');
Route::resource('/roles', 'Roles', [ 'except' => [ 'create', 'edit' ] ]);
Route::post('/roles/{role}/assign', 'Roles@assign');
Route::get('/roles/{role}/modules', 'Roles@loadModules');
/*Fin roles*/
Route::get('/profiles/paginate', 'Profiles@index_page');
Route::resource('/profiles', 'Profiles', [ 'except' => [ 'create', 'edit' ] ]);
Route::post('/profiles/{profile}/assign', 'Profiles@assign');

/*Rutas utilizadas para las funciones de modulos*/
Route::get('/modules/all', 'Modules@indexAll');
Route::resource('/modules', 'Modules', [ 'except' => [ 'create', 'edit' ] ]);
/*Fin modulos*/

Route::get('/sections/menu', 'Sections@menu');
Route::get('/sections/menuAll', 'Sections@menuAll');
Route::get('/sections/all', 'Sections@indexAll');
Route::resource('/sections', 'Sections', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/cities', 'Cities', [ 'except' => [ 'create', 'edit' ] ]);
Route::get('/states/{state_id}/cities', 'Cities@getByState');

Route::resource('/students', 'Students', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/states', 'States', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/countries', 'Countries', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/institutions', 'Institutions', [ 'except' => [ 'create', 'edit' ] ]);
Route::get('/institution_info', 'Institutions@fillViewInstitution');

Route::resource('/registrations', 'Registrations', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/payments', 'Payments', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/shifts', 'Shifts', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/groups', 'Groups', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/carrers', 'Carrers', [ 'except' => [ 'create', 'edit' ] ]);

/*Rutas utilizadas para las funciones de cuartos*/
Route::resource('/rooms', 'Rooms', [ 'except' => [ 'create', 'edit' ] ]);
/*Fin cuartos*/

Route::resource('/phones', 'Phones', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/currencies', 'Currencies', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/types_payments', 'TypesPayments', [ 'except' => [ 'create', 'edit' ] ]);

Route::resource('/adresses', 'Address', [ 'except' => [ 'create', 'edit' ] ]);

/*Rutas utilizadas para las funciones de edificios*/
Route::resource('/buildings', 'Buildings', [ 'except' => [ 'create', 'edit' ] ]);
Route::get('/buildings/{building}/rooms', 'Buildings@loadRooms');
/*Fin edificios*/

Route::resource('/room_types', 'Room_types', [ 'except' => [ 'create', 'edit' ] ]);
Route::get('/room_types/{room_type}/rooms', 'Room_types@showRooms');

Route::resource('/inscriptions', 'InscriptionPeriods', [ 'except' => [ 'create', 'edit' ] ]);
Route::resource('/periods', 'Periods', [ 'except' => [ 'create', 'edit' ] ]);
Route::resource('/type_periods', 'TypePeriods', [ 'except' => [ 'create', 'edit' ] ]);
Route::post('/periods/{periodo}/assign', 'Periods@assign');

Route::get('create_demo_user', function() {
    $users = User::where('email', 'demo@demo.com')->get();
    if (count($users) == 0) {
        $profile = new Profile;
        $profile->name = 'Administrador';
        $profile->description = '';
        $profile->save();

        $user = new User;
        $user->user = 'Demo';
        $user->email = 'demo@demo.com';
        $user->password = \Hash::make('1234');
        $user->profile_id = $profile->id;
        $user->save();
        $users[] = $user;
    }

    return $users;
});
/*Rutas utilizadas para las funciones de areas*/
Route::resource('/areas', 'Areas', ['except' => ['create', 'edit'] ]);
Route::get('/areas/{area}/subareas', 'Areas@getSubareas');
Route::get('/all/areas', 'Areas@allAreas');
/*Fin areas*/

/*Rutas utilizadas para las funciones de infraestructuras*/
Route::resource('/infrastructures', 'Infrastructures', ['except' => ['create', 'edit'] ]);
Route::get('/infrastructures/{id}/room', 'Infrastructures@getRoom');
/*Fin infraestructuras*/

/*Rutas utilizadas para las funciones de subareas*/
Route::get('/subareas/all', 'Subareas@allSubareas');
Route::get('/subareas/{subarea}/rooms', 'Subareas@getRooms');
Route::resource('/subareas', 'Subareas', ['except' => ['create', 'edit'] ]);
/*Fin subareas*/

/*Rutas utilizadas para TypesPayments*/
Route::resource('types_payment', 'TypesPayments', ['except' => ['create', 'edit']]);
/**Fin TypesPayments*/
