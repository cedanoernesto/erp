<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CourseTest extends TestCase
{
    use DatabaseTransactions;

    /*public function testExample()
    {
        $this->assertTrue(true);
    }*/

    public function testCoursesWithValidData()
    {
        $this->post('api/courses',
        [
          'name' =>'prueba',
          'capacitance' => 'jijiji',
          'status' => 1,
          'periods_admission_id' => 1
        ]
        )->assertStatus(200);
    }

    public function testCreateCoursesIncorrect()
    {
    	$this->post('api/courses',
    		[
    			'name' =>'hihai',
    			'periods_admission_id' => 1
    		])->assertStatus(400);
    }


    public function testUpdateCoursesWithValidData ()   {
        $course = $this->post('api/courses',
        [
          'name' =>'prueba',
          'capacitance' => 'jijiji',
          'status' => 1,
          'periods_admission_id' => 1
        ]
        )->decodeResponseJson();

        $this->put("api/courses/".$course['id'],
        [
          'name' =>'holaxdxdxd',
          'capacitance' => 'juju',
          'status' => 1,
          'periods_admission_id' => 1
        ]
        )->assertStatus(200);
    }

    public function testUpdateCoursesWithInvalidData ()   {
        $course = $this->post('api/courses',
        [
          'name' =>'prueba',
          'capacitance' => 'jijiji',
          'status' => 1,
          'periods_admission_id' => 1
        ]
        )->decodeResponseJson();

        $this->put("api/courses/".$course['id'], [
            'name' => NULL
        ])->assertStatus(400);
    }

    public function testShowCourses ()  {
        $course = $this->post('api/courses',
        [
         'name' =>'prueba',
          'capacitance' => 'jijiji',
          'status' => 1,
          'periods_admission_id' => 1
        ]
        )->decodeResponseJson();

        $response = $this->get('api/courses/'.$course['id']);

        $response->assertStatus(200);
    }

    public function testDestroyCoursesWithValidData()
    {
        $course = $this->post('api/courses',
        [
          'name' =>'prueba',
          'capacitance' => 'jijiji',
          'status' => 1,
          'periods_admission_id' => 1
        ]
        )->decodeResponseJson();

        $this->delete('api/courses/'.$course['id'])->assertStatus(200);
    }

    public function testDestroyCoursessWithInvalidData()
    {
        $this->delete('api/courses/5000')->assertStatus(500);
    }


}
