<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomTypesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCreateRoom_TypesWithValidData () {
        $this->post('api/room_types', [
            'name' => 'bathroom',
            'description' => 'quiet place'
        ])->assertStatus(200);
    }

    public function testCreateRoom_TypesWithInvalidData () {
        $this->post('api/room_types', [
            'name' => 'bathroom'
        ])->assertStatus(500);

        $this->post('api/room_types', [
            'description' => 'quiet place'
        ])->assertStatus(400);
    }

	public function testUpdateRoom_TypesWithValidData()
    {
        $roomsT = $this->post('api/room_types', [
            'name' => 'bedroom',
            'description' => 'Im sleep very well',
            ])->decodeResponseJson();

        $this->put("api/room_types/".$roomsT['id'], [
            'name' => 'bedroom',
            'description' => 'Im sleep very bad',
            'status' => 0,
        ])->assertStatus(200);
    }

    public function testUpdateRoom_TypesWithInvalidData()
    {
        $this->put("api/room_types/1", [
            'name' => ''
        ])->assertStatus(400);
    }

    public function testShowRooms()
    {
        $roomsT = $this->post('api/room_types', [
            'name' => 'bedroom',
            'description' => 'I sleep',
            'status' => 1,
            ])->decodeResponseJson();

        $response = $this->get('api/room_types/'.$roomsT['id']);

        $response->assertStatus(200);
    }

    public function testDestroyRoomsWithValidData()
    {
        $roomsT = $this->post('api/room_types', [
            'name' => 'bedroom',
            'description' => 'I sleep',
            'status' => 1,

            ])->decodeResponseJson();

        $this->delete('api/room_types/'.$roomsT['id'])->assertStatus(200);
    }

    public function testDestroyRoomsWithInvalidData()
    {
        $this->delete('api/room_types/5000')->assertStatus(500);
    }
}
