<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ModulesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCreateModulesWithValidData ()   {

        $this->post('api/modules', [
            'name' => 'Users',
            'description' => 'Modify information about users in the system',
            'url' => '/users',
            'icon' => 'users.png',
            'section_id' => 1
        ])->assertStatus(200);
    }

    public function testCreateModulesWithInvalidData () {
        $this->post('api/modules', [
            'name' => 'Users',
            'description' => 'Modify information about users in the system',
            'url' => '/users',
            'icon' => 'users.png',
            'section_id' => 75
        ])->assertStatus(500);

        $this->post('api/modules', [
            'url' => '/users',
            'icon' => 'users.png',
            'section_id' => 1
        ])->assertStatus(400);
    }

    public function testUpdateModulesWithValidData () {
        $module = $this->post('api/modules', [
                    'name' => 'Modules',
                    'description' => 'Manage different modules in the system',
                    'url' => '/modules',
                    'icon' => 'modules.png',
                    'section_id' => 1
                ])->decodeResponseJson();
        $this->put("api/modules/".$module['id'], [
            'name' => 'Module',
            'description' => 'Manage different modules in the system',
            'url' => '/modules',
            'icon' => 'modules.png',
            'section_id' => 1
        ])->assertStatus(200);
    }

    public function testUpdateModulesWithInvalidData () {
        $module = $this->post('api/modules', [
                    'name' => 'Modules',
                    'description' => 'Manage different modules in the system',
                    'url' => '/modules',
                    'icon' => 'modules.png',
                    'section_id' => 1
                ])->decodeResponseJson();

        $this->put("api/modules/".$module['id'], [
            'name' => 'Module',
            'url' => '/modules',
            'icon' => 'modules.png',
            'section_id' => 1
        ])->assertStatus(302);
    }

    public function testShowModules ()  {
        $module = $this->post('api/modules', [
                    'name' => 'Modules',
                    'description' => 'Manage different modules in the system',
                    'url' => '/modules',
                    'icon' => 'modules.png',
                    'section_id' => 1
                ])->decodeResponseJson();

        $response = $this->get('api/modules/'.$module['id']);

        $response->assertStatus(200);
    }

    public function testDestroyModulesWithValidData ()   {
        $module = $this->post('api/modules', [
                    'name' => 'Modules',
                    'description' => 'Manage different modules in the system',
                    'url' => '/modules',
                    'icon' => 'modules.png',
                    'section_id' => 1
                ])->decodeResponseJson();
        $this->delete('api/modules/'.$module['id'])->assertStatus(200);
    }

    public function testDestroyModulesWithInvalidData ()    {
        $this->delete('api/modules/5000')->assertStatus(500);
    }
}
