<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomsTest extends TestCase
{
    /**
     * A basic test example.RoomsTest
     *
     * @return void
     */
    public function testCreateRoomsWithValidData()
    {
        $this->post('api/rooms', [
            'name' => 'Mac',
            'capacity' => 1,
            'ubication' => 1,
            'infrastructure_id' => 1,
            'subarea_id' => 1
        ])->assertStatus(200);
    }

    public function testCreateRoomsWithInvalidData()
    {

        $this->post('api/rooms', [
            'name' => 'rooms',
            'capacity' => 1,
            'status' => 1,
        ])->assertStatus(400);

    }

    public function testUpdateRoomsWithValidData()
    {
        $area = $this->post('api/areas', [
            'name' => 'Sonora_10'
        ])->decodeResponseJson();

        $subarea = $this->post('api/subareas', [
            'name' => 'SVillaverde',
            'area_id' => $area['id']
        ])->decodeResponseJson();

        $rooms = $this->post('api/rooms', [
            'name' => 'Mac',
            'capacity' => 1,
            'ubication' => 1,
            'status' => 1,
            'infrastructure_id' => 1,
            'subarea_id' => $subarea['id']
        ])->decodeResponseJson();

        $this->put("api/rooms/".$rooms['id'], [
            'name' => 'Mac',
            'capacity' => 7,
            'ubication' => 2,
            'status' => 1,
            'infrastructure_id' => 1,
            'subarea_id' => 1
        ])->assertStatus(200);
    }

    public function testUpdateRoomsWithInvalidData()
    {
        $rooms = $this->post('api/rooms', [
            'name' => 'Mac',
            'capacity' => 1,
            'ubication' => 1,
            'status' => 1,
            'infrastructure_id' => 1,
            'subarea_id' => 1
        ])->decodeResponseJson();

        $this->put("api/rooms/".$rooms['id'], [
            'status' => 1,
            'infrastructure_id' => 1,
            'subarea_id' => 1
        ])->assertStatus(400);
    }

    public function testShowRooms()
    {
        $rooms = $this->post('api/rooms', [
            'name' => 'Mac',
            'capacity' => 1,
            'ubication' => 1,
            'status' => 1,
            'infrastructure_id' => 1,
            'subarea_id' => 1
            ])->decodeResponseJson();

        $response = $this->get('api/rooms/'.$rooms['id']);

        $response->assertStatus(200);
    }

    public function testDestroyRoomsWithValidData()
    {
        $rooms = $this->post('api/rooms', [
            'name' => 'Mac',
            'capacity' => 1,
            'ubication' => 1,
            'status' => 1,
            'infrastructure_id' => 1,
            'subarea_id' => 1
            ])->decodeResponseJson();

        $this->delete('api/rooms/'.$rooms['id'])->assertStatus(200);
    }

    public function testDestroyRoomsWithInvalidData()
    {
        $this->delete('api/rooms/5000')->assertStatus(500);
    }


}
