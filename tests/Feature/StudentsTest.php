<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
     use DatabaseTransactions;

    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCreateStudentsWithValidData ()
     {
        $this->post('api/students', [
          'enrollment'=> 'lermashu',
          'aspirants_id'=>1,
          'users_id' =>1
        ]
        )->assertStatus(200);
    }

    public function testCreateStudentsWithInvalidData()
    {
        $this->post('api/students', [
            'status' => 'x',
        ])->assertStatus(400);
    }

    public function testUpdateStudentsWithValidData () {
        $students = $this->post('api/students',
        [
          'enrollment' =>'lermashu',
          'status' => 1,
          'aspirants_id'=> 1,
          'users_id'=> 1
        ])->decodeResponseJson();

        $this->put("api/students/".$students['id'],
        [
          'enrollment' =>'conchalord',
          'status' => 1,
          'aspirants_id'=> 1,
          'users_id'=> 1
        ])->assertStatus(200);
    }

    public function testUpdateStudentsWithInvalidData () {
        $students = $this->post('api/students',
        [
          'enrollment' =>'lermashu',
          'status' => 1,
          'aspirants_id'=> 1,
          'users_id'=> 1
        ])->decodeResponseJson();

        $this->put("api/students/".$students['id'],
        [
          'enrollment' =>'conchalord',
          'status' => 1,
        ])->assertStatus(400);
    }

    public function testShowStudents()
    {
        $students = $this->post('api/students', [
            'enrollment' => 'lermashu',
            'status' => 1,
            'aspirants_id' => 1,
            'users_id' => 1
            ])->decodeResponseJson();

        $response = $this->get('api/students/'.$students['id']);

        $response->assertStatus(200);
    }

    public function testDestroyStudentsWithValidData()
    {
        $students = $this->post('api/students', [
            'enrollment' => 'lermashu',
            'status' => 1,
            'aspirants_id' => 1,
            'users_id' => 1
            ])->decodeResponseJson();

        $this->delete('api/students/'.$students['id'])->assertStatus(200);
    }

    public function testDestroyStudentsWithInvalidData()
    {
        $this->delete('api/students/5000')->assertStatus(500);
    }
}
