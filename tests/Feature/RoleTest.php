<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testListRolesWithoutData(){
        $this->get('api/roles')->assertStatus(200);
    }
    public function testCreateroleWithInvalidData() {
        $this->post('api/roles', [
            'name' => 'John Doe'
        ])->assertStatus(400);

        $this->post('api/roles', [
            'name' => 'John Doe',
            'description' => 'Lorem Ipsum delirium',
        ])->assertStatus(200);
    }
    public function testUpdateWithInvalidData() {

        $this->post('api/roles', [
            'name' => 'John Doe'
        ])->assertStatus(400);

        $this->post('api/roles', [
            'name' => 'John Doe',
            'description' => 'Lorem Ipsum delirium',
        ])->assertStatus(200);
    }
}
