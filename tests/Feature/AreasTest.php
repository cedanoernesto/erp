<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AreasTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCreateAreasWithValidData ()   {
        $this->post('api/areas', [
            'name' => 'villaunion'
        ])->assertStatus(200);
    }

    public function testCreateAreasWithInvalidData ()   {
        $this->post('api/areas', [
            'name' => NULL
        ])->assertStatus(400);
    }

    public function testUpdateAreasWithValidData ()   {
        $area = $this->post('api/areas', [
            'name' => 'villaunion'
        ])->decodeResponseJson();

        $this->put("api/areas/".$area['id'], [
            'name' => 'Sonora'
        ])->assertStatus(200);
    }

    public function testUpdateAreasWithInvalidData ()   {
        $area = $this->post('api/areas', [
            'name' => 'villaunion'
        ])->decodeResponseJson();

        $this->put("api/areas/".$area['id'], [
            'name' => NULL
        ])->assertStatus(400);
    }

    public function testShowAreas ()  {
        $area = $this->post('api/areas', [
            'name' => 'chihuahua'
        ])->decodeResponseJson();

        $response = $this->get('api/areas/'.$area['id']);

        $response->assertStatus(200);
    }

    public function testDestroyAreasWithValidData()
    {
        $area = $this->post('api/areas', [
            'name' => 'chihuahua'
        ])->decodeResponseJson();

        $this->delete('api/areas/'.$area['id'])->assertStatus(200);
    }

    public function testDestroyAreasWithInvalidData()
    {
        $this->delete('api/areas/5000')->assertStatus(500);
    }

    public function testGetSubareas ()
    {
        $response = $this->get('api/areas/1/subareas');

        $response->assertStatus(200);
    }

    public function testGetSubareasWithNotExistingData ()
    {
        $response = $this->get('api/areas/9999/subareas');

        $response->assertStatus(404);
    }
}
