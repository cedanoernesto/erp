<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TypePeriodTest extends TestCase
{
   use DatabaseTransactions;
    public function testExample()
    {
        $this->assertTrue(true);
    }

        public function testCreateTypePeriodsWithValidData ()   {
            $this->post('api/type_periods',
            [
              'name' =>'lermashu',
              'status' => 1
            ]
            )->assertStatus(200);
        }

        public function testCreateTypePeriodsWithInvalidData ()   {
            $this->post('api/type_periods',
            [
              'status' => 'lmermasji'
            ]
            )->assertStatus(400);
        }


        public function testUpdateTypePeriodsWithValidData () {
            $typePeriod = $this->post('api/type_periods',
            [
              'name' =>'lermashu',
              'status' => 1
            ])->decodeResponseJson();

            $this->put("api/type_periods/".$typePeriod['id'],
            [
              'name' =>'conchalord',
              'status' => 1
            ])->assertStatus(200);
        }

        public function testUpdateTypePeriodsWithInvalidData ()   {
            $period = $this->post('api/type_periods',
            [
              'name' =>'lermashu',
              'status' => 1,
            ]
            )->decodeResponseJson();

            $this->put("api/type_periods/".$period['id'],  [
              'name' => NULL
            ])->assertStatus(400);
        }

        public function testShowTypePeriods ()  {
            $typeperiod = $this->post('api/type_periods',
            [
              'name' =>'lermashu',
              'status' => 1
            ]
            )->decodeResponseJson();

            $response = $this->get('api/type_periods/'.$typeperiod['id']);

            $response->assertStatus(200);
        }

        public function testDestroyPeriodsWithValidData()
        {
            $typeperiod = $this->post('api/type_periods',
            [
              'name' =>'lermashu',
              'status' => 1,
            ]
            )->decodeResponseJson();

            $this->delete('api/type_periods/'.$typeperiod['id'])->assertStatus(200);
        }

        public function testDestroyTypePeriodsWithInvalidData()
        {
            $this->delete('api/periods/5000')->assertStatus(500);
        }


}
