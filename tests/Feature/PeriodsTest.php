<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PeriodsTest extends TestCase
{
    use DatabaseTransactions;
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCreatePeriodsWithValidData ()   {
        $this->post('api/periods',
        [
          'name' =>'lermashu',
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'inscription_period_id' => 1,
          'type_period_id' => 1
        ]
        )->assertStatus(200);
    }

    public function testCreatePeriodsIncorrect()
    {
    	$this->post('api/periods',
    		[
    			'name' =>'hihai',
    			'inscription_period_id' => 1,
    			'type_period_id' => 1
    		])->assertStatus(400);
    }


    public function testUpdatePeriodsWithValidData ()   {
        $period = $this->post('api/periods',
        [
          'name' =>'lermashu',
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'inscription_period_id' => 1,
          'type_period_id' => 1
        ]
        )->decodeResponseJson();

        $this->put("api/periods/".$period['id'],
        [
          'name' =>'conchalord',
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'inscription_period_id' => 1,
          'type_period_id' => 1
        ]
        )->assertStatus(200);
    }

    public function testUpdatePeriodsWithInvalidData ()   {
        $period = $this->post('api/periods',
        [
          'name' =>'lermashu',
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'inscription_period_id' => 1,
          'type_period_id' => 1
        ]
        )->decodeResponseJson();

        $this->put("api/periods/".$period['id'], [
            'name' => NULL
        ])->assertStatus(400);
    }

    public function testShowPeriods ()  {
        $period = $this->post('api/periods',
        [
          'name' =>'lermashu',
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'inscription_period_id' => 1,
          'type_period_id' => 1
        ]
        )->decodeResponseJson();

        $response = $this->get('api/periods/'.$period['id']);

        $response->assertStatus(200);
    }

    public function testDestroyPeriodsWithValidData()
    {
        $period = $this->post('api/periods',
        [
          'name' =>'lermashu',
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'inscription_period_id' => 1,
          'type_period_id' => 1
        ]
        )->decodeResponseJson();

        $this->delete('api/periods/'.$period['id'])->assertStatus(200);
    }

    public function testDestroyPeriodsWithInvalidData()
    {
        $this->delete('api/periods/5000')->assertStatus(500);
    }


}
