<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubareasTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCreateSubareasWithValidData ()   {
        $area = $this->post('api/areas', [
            'name' => 'villaunion'
        ])->decodeResponseJson();

        $this->post('api/subareas', [
            'name' => 'Sonora_1',
            'area_id' => $area['id']
        ])->assertStatus(200);
    }

    public function testCreateSubareasWithInvalidData ()   {
        $this->post('api/subareas', [
            'name' => NULL
        ])->assertStatus(400);
    }

    public function testUpdateSubareasWithValidData ()   {

        $area = $this->post('api/areas', [
            'name' => 'villaunion'
        ])->decodeResponseJson();

        $subareas = $this->post('api/subareas', [
            'name' => 'Sonora_2',
            'area_id' => $area['id']
        ])->decodeResponseJson();

        $this->put("api/subareas/".$subareas['id'], [
            'name' => 'Sonora_2.1',
            'area_id' => $area['id']
        ])->assertStatus(200);
    }

    public function testUpdateSubareasWithInvalidData ()   {

        $area = $this->post('api/areas', [
            'name' => 'villaunion'
        ])->decodeResponseJson();

        $subareas = $this->post('api/subareas', [
            'name' => 'Sonora_3',
            'area_id' => $area['id']
        ])->decodeResponseJson();

        $this->put("api/subareas/".$subareas['id'], [
            'name' => NULL
        ])->assertStatus(400);
    }

    public function testShowSubareas ()  {
        $area = $this->post('api/areas', [
            'name' => 'villaunion'
        ])->decodeResponseJson();

        $subareas = $this->post('api/subareas', [
            'name' => 'Sonora_3',
            'area_id' => $area['id']
        ])->decodeResponseJson();

        $response = $this->get('api/subareas/'.$subareas['id']);

        $response->assertStatus(200);
    }

    public function testDestroySubareasWithValidData()
    {
        $area = $this->post('api/areas', [
            'name' => 'villaunion'
        ])->decodeResponseJson();

        $subareas = $this->post('api/subareas', [
            'name' => 'Sonora_3',
            'area_id' => $area['id']
        ])->decodeResponseJson();

        $this->delete('api/subareas/'.$subareas['id'])->assertStatus(200);
    }

    public function testDestroyRoomsWithInvalidData()
    {
        $this->delete('api/subareas/9999')->assertStatus(500);
    }

    public function testGetRooms ()
    {
        $response = $this->get('api/subareas/1/rooms');

        $response->assertStatus(200);
    }

    public function testGetRoomsWithNotExistingData ()
    {
        $response = $this->get('api/subareas/9999/rooms');

        $response->assertStatus(404);
    }
}
