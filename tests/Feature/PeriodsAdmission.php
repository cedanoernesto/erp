<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PeriodsAdmission extends TestCase
{
    use DatabaseTransactions;
    

    public function testExample()
    {
        $this->assertTrue(true);
    }
    
    public function testPeriodsAdmissionWithValidData ()   {
        $this->post('api/periods_admission',
        [
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'institution_id' => 1
        ]
        )->assertStatus(200);
    }

    public function testPeriodsAdmissionIncorrect()
    {
    	$this->post('api/periods_admission',
    		[
    			'start_date' => '17-06-11',
    			'institution_id' => 1
    		])->assertStatus(400);
    }


    /* public function testUpdatePeriodsAdmissionWithValidData ()   {
        $period = $this->post('api/periods_admission',
        [
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'institution_id' => 1
        ]
        )->decodeResponseJson();

        $this->put("api/periods_admission/".$period['id'],
        [
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'institution_id' => 1
        ]
        )->assertStatus(200);
    }

    public function testUpdatePeriodsAdmissionWithInvalidData ()   {
        $period = $this->post('api/periods_admission',
        [
          'start_date' => '17-06-11',
          'status' => 1,
          'institution_id' => 1
        ]
        )->decodeResponseJson();

        $this->put("api/periods_admission/".$period['id'], [
            'name' => NULL
        ])->assertStatus(400);
    }

    public function testShowPeriodsAdmission ()  {
        $period = $this->post('api/periods_admission',
        [
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'institution_id' => 1
        ]
        )->decodeResponseJson();

        $response = $this->get('api/periods_admission/'.$period['id']);

        $response->assertStatus(200);
    }

    public function testDestroyPeriodsAdmissionWithValidData()
    {
        $period = $this->post('api/periods_admission',
        [
          'start_date' => '17-06-11',
          'end_date' => '17-06-15',
          'status' => 1,
          'institution_id' => 1
        ]
        )->decodeResponseJson();

        $this->delete('api/periods_admission/'.$period['id'])->assertStatus(200);
    }

    public function testDestroyPeriodsAdmissionWithInvalidData()
    {
        $this->delete('api/periods_admission/5000')->assertStatus(500);
    }
    */
}
