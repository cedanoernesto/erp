<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testListUsersWithoutData()
    {
        $this->get('api/users')
            ->assertExactJson([]);
    }

    public function testCreateuserWithInvalidData() {
        $this->post('api/users', [
            'user' => 'Joel',
            'password' => '1234'
        ])->assertStatus(400);

        $this->post('api/users', [
            'user' => 'Joel',
            'email' => 'mail',
            'password' => '1234'
        ])->assertStatus(400);
    }

    public function testCreateuserWithValidData() {
        $profile = $this->post('api/profiles', [
            'name' => 'Prueba',
            'description' => 'Prueba'
        ])->decodeResponseJson();

        $this->post('api/users', [
            'user' => 'Joel',
            'email' => 'mail@mail.com',
            'password' => '1234',
            'profile_id' => $profile['id']
        ])->assertStatus(200);
    }
}
