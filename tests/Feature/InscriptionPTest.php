<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InscriptionPTest extends TestCase
{
     use DatabaseTransactions;
    public function testExample()
    {
        $this->assertTrue(true);
    }



        public function testCreateInscriptionsWithValidData ()   {
            $this->post('api/inscriptions',
            [
              'start_date' => '17-09-11',
              'end_date' => '17-09-15',
              'status' => 1
            ]
            )->assertStatus(200);
        }

        public function testCreateInscriptionsWithInvalidData ()   {
            $this->post('api/inscriptions',
            [
              'start_date' => '17-09-14',
              'status' => 1
            ]
            )->assertStatus(400);
        }
/*
        public function testUpdateInscriptionsWithValidData ()   {
            $inscription = $this->post('api/inscriptions',
            [
              'start_date' => '17-09-11',
              'end_date' => '17-09-15',
              'status' => 1
            ]
            )->decodeResponseJson();

            $this->put("api/inscriptions/".$inscription['id'],
            [
              'start_date' => '17-09-11',
              'end_date' => '17-09-15',
              'status' => 1
            ]
            )->assertStatus(200);
        }

        public function testUpdateInscriptionsWithInvalidData ()   {
            $inscription = $this->post('api/inscriptions',
            [
              'start_date' => '17-09-11',
              'end_date' => '17-09-15',
              'status' => 1
            ]
            )->decodeResponseJson();

            $this->put("api/inscriptions/".$inscription['id'],
            [
              'start_date' => '17-09-11',
              'end_date' => '17-09-15',
              'status' => NULL
            ]
            )->assertStatus(400);
        }
*/
        public function testShowInscriptions ()  {
            $inscription = $this->post('api/inscriptions',
            [
              'start_date' => '17-09-11',
              'end_date' => '17-09-15',
              'status' => 1
            ]
            )->decodeResponseJson();

            $response = $this->get('api/inscriptions/'.$inscription['id']);

            $response->assertStatus(200);
        }

        public function testDestroyInscriptionsWithValidData()
        {
            $inscription = $this->post('api/inscriptions',
            [
              'start_date' => '17-09-11',
              'end_date' => '17-09-15',
              'status' => 1
            ]
            )->decodeResponseJson();

            $this->delete('api/inscriptions/'.$inscription['id'])->assertStatus(200);
        }

        public function testDestroyInscriptionsWithInvalidData()
        {
            $this->delete('api/inscriptions/5000')->assertStatus(500);
        }

}
