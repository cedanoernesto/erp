<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RolesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /*public function testExample()
    {
        $this->assertTrue(true);
    }*/

    public function testCreateRolesWithInvalidData () {
        $this->post('api/roles', [
            'name' => 'users'
        ])->assertStatus(400);

        $this->post('api/roles', [
            'description' => 'Manage users in the ERP'
        ])->assertStatus(400);
    }

    public function testCreateRolesWithValidData () {
        $this->post('api/roles', [
            'name' => 'users',
            'description' => 'Manage users in the ERP'
        ])->assertStatus(200);
    }

    public function testAssignModulesToRolesWithValidData ()    {
        $role = $this->post('api/roles', [
            'name' => 'users',
            'description' => 'Manage users in the ERP'
        ])->decodeResponseJson();

        $section = $this->post('api/sections', [
            'name' => 'Administration'
        ])->decodeResponseJson();

        $module = $this->post('api/modules', [
            'name' => 'Users',
            'description' => 'Modify information about users in the system',
            'url' => '/users',
            'icon' => 'users.png',
            'section_id' => $section['id']
        ])->decodeResponseJson();

        $this->post('api/roles/'.$role['id'].'/assign',[
            'module_id' => $module['id'],
            'role_id' => $role['id']
        ])->assertStatus(200);
    }

    public function testAssignModulesToRolesWithInvalidData ()  {
        $this->post('api/roles/1/assign', [
            'module_id' => 35
        ])->assertStatus(400);
    }

    public function testLoadModulesWithExistingData ()  {
        $response = $this->get('api/roles/1/modules');

        $response->assertStatus(200);
    }

    public function testLoadModulesWithNotExistingData ()  {
        $response = $this->get('api/roles/9999/modules');

        $response->assertStatus(404);
    }
}
