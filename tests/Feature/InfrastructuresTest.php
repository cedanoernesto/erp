<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InfrastructuresTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCreateInfrastructuresWithValidData ()   {
        $this->post('api/infrastructures', [
            'name' => 'UAS',
            'ubication' => 'porahi',
            'rooms' => 5,
            'floors' => 2,
            'status' => 1
        ])->assertStatus(200);
    }

    public function testCreateInfrastructuresWithInvalidData ()   {
        $this->post('api/infrastructures', [
            'name' => 'UAS',
            'ubication' => 'porahi',
            'rooms' => 5
        ])->assertStatus(400);
    }

    public function testUpdateInfrastructuresWithValidData ()   {
        $infrastructure = $this->post('api/infrastructures', [
            'name' => 'UAS',
            'ubication' => 'porahi',
            'rooms' => 5,
            'floors' => 2,
            'status' => 1
        ])->decodeResponseJson();

        $this->put("api/infrastructures/".$infrastructure['id'], [
            'name' => 'UPSIN',
            'ubication' => 'por aqui',
            'rooms' => 7,
            'floors' => 3,
            'status' => 1
        ])->assertStatus(200);
    }

    public function testUpdateInfrastructuresWithInvalidData ()   {
        $infrastructure = $this->post('api/infrastructures', [
            'name' => 'UAS',
            'ubication' => 'porahi',
            'rooms' => 5,
            'floors' => 2,
            'status' => 1
        ])->decodeResponseJson();

        $this->put("api/infrastructures/".$infrastructure['id'], [
            'rooms' => 7,
            'floors' => 3,
            'status' => 1
        ])->assertStatus(400);
    }

    public function testShowInfrastructure ()  {
        $infrastructure = $this->post('api/infrastructures', [
            'name' => 'UAS',
            'ubication' => 'porahi',
            'rooms' => 5,
            'floors' => 2,
            'status' => 1
        ])->decodeResponseJson();

        $response = $this->get('api/infrastructures/'.$infrastructure['id']);

        $response->assertStatus(200);
    }

    public function testDestroyInfrastructureWithValidData()
    {
        $infrastructure = $this->post('api/infrastructures', [
            'name' => 'UAS',
            'ubication' => 'porahi',
            'rooms' => 5,
            'floors' => 2,
            'status' => 1
        ])->decodeResponseJson();

        $this->delete('api/infrastructures/'.$infrastructure['id'])->assertStatus(200);
    }

    public function testDestroyRoomsWithInvalidData()
    {
        $this->delete('api/infrastructures/5000')->assertStatus(500);
    }

    public function testGetRooms ()
    {
        $response = $this->get('api/infrastructures/1/rooms');

        $response->assertStatus(200);
    }

    public function testGetRoomsWithNotExistingData ()
    {
        $response = $this->get('api/infrastructures/9999/rooms');

        $response->assertStatus(404);
    }
}
