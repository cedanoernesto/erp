@extends('index')
@section('content')
    <style media="screen">
        .ui .card:not(.blue-card) {
          width: 350px !important;
        }
    </style>

    <div class="ui twelve wide column grid">
        <!-- Columna de imagenes -->
		<p class="ui medium header" id="institución_title">Institución</p>
        <div class="two column row">
            <div class="column">
                <div class="ui card">
                    <div class="image">
                        <img id="instImg" src="/img/image.png">
                    </div>
                    <div class="content">
                        <a href="" class="header">Dirección</a>
                        <div class="description">
                            <div id="direccion" class="content">
                            </div>
                        </div>
                    </div>
                    <div class="extra content">
                        <p id="instName" class="extra-left"><span></span></p>
                        <p id="btnEdit" class="extra-right">Editar</p>
                    </div>
                </div>
            </div>
            <div class="column">
                <p class="header">Contacto</p>
                <div id="contacto" class="content"></div>
                <div class="ui divider"></div>
                <p class="header">Contabilidad</p>
                <div id="moneda" class="content"></div>
                <div class="ui divider"></div>
                <p class="header">Matriculación</p>
                <div id="matriculacion" class="content"></div>
                <div class="ui divider"></div>
                <p class="header">Turnos</p>
                <div id="turno" class="content"></div>
            </div>
        </div>
</div>

<div id="edit_institution" class="ui modal">
    <div class="header">
        Institución
        <i id="btnClose" class="close icon right" style="cursor:pointer;"></i>
    </div>
    <form id="frmInstitution" class="ui form segment">
        <div class="two fields">
            <div class="ui center aligned container field">
                <img id="institution_img" src="img/user.png" style="width: 64px; height: 64px;">
                <input type="file" id="picture" name="photo" value="" accept="image/x-png,image/bmp,image/jpeg">
            </div>
            <div class="field">
                <label>Nombre de la institución</label>
                <input id="txtName" placeholder="Nombre" name="name" type="text">
            </div>
        </div>
        <div class="ui top attached tabular menu">
            <a class="item active" data-tab="general_information">Información general</a>
            <a class="item" data-tab="configuration">Configuración</a>
        </div>
        <div class="ui bottom attached tab segment active" data-tab="general_information">
            <div class="two fields">
                <div class="field">
                   <label>Nombre de calle:</label>
                    <input id="txtStreet" placeholder="Calle" name="neighborhood" type="text">
                </div>
                <div class="field">
                    <label>Número de teléfono:</label>
                    <input id="txtPhone" placeholder="Teléfono" name="phone" type="text"/>
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Nombre de colonia:</label>
                    <input id="txtAddress" placeholder="Colonia" name="address" type="text">
                </div>
            </div>
            <div class="three fields">
                <div class="field">
                    <select class="ui dropdown" id="cmbState" name="state">
                        <option disabled selected>Estado</option>
                    </select>
                </div>
                <div class="field">
                    <select class="ui dropdown" id="cmbCity" name="city">
                        <option disabled selected>Ciudad</option>
                    </select>
                </div>
                <div class="field">
                    <input id="txtZipCode" placeholder="Código postal" name="zip_code" type="text">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <input id="txtEmail" placeholder="E-mail" name="email" type="text">
                </div>
            </div>
        </div>
        <div class="ui bottom attached tab segment" data-tab="configuration">
            <div class="two fields">
                <div class="field">
                    <label>Moneda</label>
                    <select class="ui dropdown" id="cmbCurrency" name="currency">
                        <option disabled selected>Moneda</option>
                    </select>
                </div>
                <div class="field">
                    <label>Tipo de matriculación</label>
                    <select class="ui dropdown" id="cmbRegistration" name="registration">
                        <option disabled selected>Matriculación</option>
                    </select>
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Tipo de pago</label>
                    <select class="ui dropdown" id="cmbPayType" name="pay_type">
                        <option disabled selected>Tipo de pago</option>
                    </select>
                </div>
                <div class="field">
                    <label>Carácter inicial</label>
                    <input id="txtInitialCharacter" class="ui dropdown" name="initial_character" type="text">
                </div>
            </div>
            <div class="two fields">
                <div class="field"></div>
                <div class="field">
                    <label>Turno</label>
                    <select class="ui dropdown" id="cmbShift" name="shift">
                        <option disabled selected>Turno</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="ui primary submit button" id="btnSaveInst">
            Guardar
        </div>
        <div class="ui error message"></div>
    </form>
</div>
<script type="text/javascript" src="{{ URL::asset('js/institutions.js') }}"></script>
@endsection
