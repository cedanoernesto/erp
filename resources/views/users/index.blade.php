@extends('index')
@section('path')
    /usuarios
@endsection
@section('content')
    <div class="ui twelve wide column">
        <div class="padding-30">
			<p class="ui medium header">Usuarios</p>
            <table class="ui selectable celled table">
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Imagen</th>
                        <th>Perfil</th>
                        <th>Email</th>
                        <th width="250">Acciones</th>
                    </tr>
                </thead>
                <tbody id="users_table">
                </tbody>
            </table>
            <div class="ui pagination menu">
                <a class="active item prev_all">
                    <i class="angle double left icon"></i>
                </a>
                <a class="active item prev">
                    <i class="angle left icon"></i>
                </a>
                <a class="active item current">1</a>
                <a class="active item next">
                    <i class="angle right icon"></i>
                </a>
                <a class="active item next_all">
                    <i class="angle double right icon"></i>
                </a>
            </div>

            <button id="user_add" class="ui right floated positive button user_new">
                Agregar usuario
            </button>
        </div>


        <!--
        Modals de usuarios
        Agreguen todos sus 'modals' dentro del contenedor id="modals"
    -->
    <div id="modals" class="ui dimmer modals page transition">
        <!--
        Modal para eliminar usuario
    -->
    <div id="modal_delete" class="ui small first coupled modal">
        <div class="header">¿Está seguro que quiere eliminar este registro?</div>
        <div class="actions">
            <div class="ui negative button">No</div>
            <div id="yes" class="ui approve blue button">Sí</i></div>
        </div>
    </div>
    <div id="modal_delete_success" class="ui mini modal">
        <div class="header">
            Operación exitosa
        </div>
        <div class="content">¡Registro eliminado con éxito!</div>
    </div>

    <!--
    Modal para agregar usuario
-->
<div id="modal_new" class="ui small modal">
    <div class="header">
        Agregar Usuario
        <i class="close icon right" style="cursor:pointer;"></i>
    </div>
    <form id="form_new_user" class="ui form form_new">
        <div class="ui two column padded grid">
            <div class="column">
                <div class="field">
                    <label>Nombre:</label> <input type="text" name="user" />
                </div>

                <div class="field">
                    <label>Correo:</label> <input type="email" name="email" />
                </div>

                <div class="field">
                    <label>Contraseña:</label> <input type="password" name="password" />
                </div>

                <div class="field">
                    <label for="profile_id">Perfil:</label>
                    <select class="profiles" name="profile_id">
                    </select>
                </div>
            </div>

            <div class="ui column middle aligned content">
                <div class="ui center aligned container field">
                    <label for="picture">
                        <i class="ui bordered inverted black massive photo icon placeholder"></i>
                        <img src="" alt="" class="img_preview" style="display: none;">
                    </label>
                    <input type="file" id="picture" name="photo" accept="image/*" value="">
                </div>

                <div class="ui container field">
                    <button id="btn_new_user" class="ui blue right button">Guardar</button>
                </div>
            </div>
        </div>
        <div class="ui error message"></div>
    </form>
</div>

<div id="modal_new_success" class="ui mini modal">
    <div class="header">
        Operación exitosa
    </div>
    <div class="content">¡Registro agregado con éxito!</div>
</div>

<!--
Modal para modificar usuario
-->
<div id="modal_modify" class="ui small modal">
    <!--<i class="close icon"></i>-->
    <div class="header">
        Editar Usuario
        <i class="close icon right" style="cursor:pointer;"></i>
    </div>

    <form id="form_modify_user" class="ui form form_modify">
        <div class="ui two column padded grid">
            <div class="column">
                <span id="id_to_update" style="display: none"></span>
                <div class="field">
                    <label for="first_name">Nombre:</label><input type="text" name="user" />
                </div>
                <div class="field">
                    <label for="email">Correo:</label><input type="text" name="email" />
                </div>

                <div class="field">
                    <label for="password">Contraseña:</label><input type="password" name="password" />
                </div>

                <div class="field">
                    <label for="profile_id">Perfil:</label>
                    <select class="profiles" name="profile_id">
                    </select>
                </div>
            </div>

            <div class="ui column middle aligned content">
                <div class="ui center aligned container field">
                    <label for="photo">
                        <i class="ui bordered inverted black massive photo icon placeholder"></i>
                        <img src="" alt="" class="img_preview" style="display: none;">
                    </label>
                    <input type="file" id="photo" accept="image/*" name="photo" value="">
                </div>

                <div class="ui center aligned container field">
                    <button id="btn_modify_user" class="ui blue approve right button">Guardar</button>
                </div>
            </div>
        </div>
        <div class="ui error message"></div>
    </form>
</div>

<div id="modal_modify_success" class="ui mini modal">
    <div class="header">
        Operación exitosa
    </div>
    <div class="content">¡Registro modificado con éxito!</div>
</div>
</div>
</div>

<script type="text/javascript">
$(function() {
    // Petición GET para cargar las opciones en el select de tipo de usuario
    jQuery.ajax({
        type: 'GET',
        url: '/api/profiles',
        dataType: 'json',
        success: function(data) {
            var profiles_options = '';
            $(data).each(function(index,value){
                profiles_options += '<option value="' + value.id + '">';
                profiles_options += value.name;
                profiles_options += '</option>'
            });
            $('.profiles').append(profiles_options);
        },
        error: function(){
            new PNotify({
                title: 'Operación fallida',
                text: 'Ha ocurrido un error inesperado',
                type: 'error'
            });
        }
    });
    // Previene la acción por default del formulario para poder hacer PUT de los datos en este
    $('#form_modify_user').submit(function(e){
        e.preventDefault();
        if($('#form_modify_user').form('is valid')){
            $("#btn_modify_user").prop('disabled', true);
            var form_mod = new FormData($("#form_modify_user")[0]);
            form_mod.append('_method', 'put')
            $.ajax({
                url: '/api/users/'+ $('#id_to_update').text(),
                type: 'POST',
                dataType: 'json',
                data: form_mod,
                processData: false,
                contentType: false,
                success: function(result){
                    //$('#modal_modify_success').modal('show');
                    new PNotify({
                        title: 'Operación exitosa',
                        text: 'Usuario modificado exitosamente',
                        type: 'success'
                    });

                    $("#btn_modify_user").prop('disabled', false);
                    $('#modal_modify').modal('hide');

                    fullTable($('.current').text());
                },
                error: function(er){
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'Ha ocurrido un error inesperado',
                        type: 'error'
                    });
                    $("#btn_modify_user").prop('disabled', false);
                }
            });
        } else {
            $('#form_modify_user .ui.error.message').show();
        }
    });

    $('#form_modify_user .field input:not(input[type=file])').on('change', function (e) {
        $('.ui.error.message').hide();
        $('#form_modify_user').form('validate field', this.name)
    });

    $('#form_new_user .field input:not(input[type=file])').on('change', function (e) {
        $('.ui.error.message').hide();
        $('#form_new_user').form('validate field', this.name)
    });

    $('#form_modify_user #photo').on('change', function (e) {
        if (this.files && this.files[0] && this.files[0].type.includes('image')) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#form_modify_user .img_preview').attr('src', e.target.result);
                $('#form_modify_user .img_preview').show();
                $('#form_modify_user .placeholder').hide();
            }
            reader.readAsDataURL(this.files[0]);
        } else {
            this.value = null
            new PNotify({
                title: 'Archivo no permitido',
                text: 'El archivo seleccionado debe ser de una imagen',
                type: 'error'
            });
            //$('#form_modify_user .img_preview').attr('src', '');
            //$('#form_modify_user .img_preview').hide();
            //$('#form_modify_user .placeholder').show();
        }
    });
    // Lo mismo que en modificar, pero esta vez se hará POST
    $('#form_new_user').submit(function(e){
        e.preventDefault();
        if($('#form_new_user').form('is valid')){
            $("#btn_new_user").prop('disabled', true);
            var form = new FormData($("#form_new_user")[0]);
            $.ajax({
                url: '/api/users',
                method: "POST",
                dataType: 'json',
                data: form,
                processData: false,
                contentType: false,
                success: function(result){
                    //$('#modal_new_success').modal('show');
                    new PNotify({
                        title: 'Operación exitosa',
                        text: 'Usuario agregado exitosamente',
                        type: 'success'
                    });
                    $("#btn_new_user").prop('disabled', false);
                    $('#modal_new').modal('hide');

                    fullTable($('.current').text());
                },
                error: function(er){
                    if(er.responseJSON.error.email){
                        new PNotify({
                            title: 'Operación fallida',
                            text: 'El correo ya ha sido registrado anteriormente',
                            type: 'error'
                        });
                    }else{
                        new PNotify({
                            title: 'Operación fallida',
                            text: 'Ha ocurrido un error inesperado',
                            type: 'error'
                        });
                    }
                    $("#btn_new_user").prop('disabled', false);
                }
            });
        } else {
            $('#form_new_user .ui.error.message').show();
        }
    });

    $('#form_new_user #picture').on('change', function (e) {
        if (this.files && this.files[0] && this.files[0].type.includes('image')) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#form_new_user .img_preview').attr('src', e.target.result);
                $('#form_new_user .img_preview').show();
                $('#form_new_user .placeholder').hide();
            }
            reader.readAsDataURL(this.files[0]);
        } else {
            this.value = null
            new PNotify({
                title: 'Archivo no permitido',
                text: 'El archivo seleccionado debe ser de una imagen',
                type: 'error'
            });
            //$('#form_new_user .img_preview').attr('src', '');
            //$('#form_new_user .img_preview').hide();
            //$('#form_new_user .placeholder').show();
        }
    });

    $('.prev_all').on('click',function () {
        fullTable($('.prev_all').data('value'));
    });
    $('.prev').on('click',function () {
        fullTable($('.prev').data('value'));
    });
    $('.next').on('click',function () {
        fullTable($('.next').data('value'));
    });
    $('.next_all').on('click',function () {
        fullTable($('.next_all').data('value'));
    });
    // Petición GET para mostrar los datos en la tabla del index
    function fullTable(page) {
        jQuery.ajax({
            type: 'GET',
            url: '/api/users?page=' + page,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data, status, jqXHR) {
                $('.prev_all').data('value',1);
                $('.prev').data('value',(((data.current_page-1) > 0)?(data.current_page-1):1));
                $('.current').text(data.current_page);
                $('.next').data('value',((data.current_page+1) > data.last_page)?data.last_page:(data.current_page+1))
                $('.next_all').data('value',data.last_page);

                managePages(page, data);

                var trHTML = '';
                $(data.data).each(function(index, value){
                    trHTML += '<tr>';
                    trHTML += '<td>' + value.user + '</td>';
                    var img = 'null';
                    if(value.image){
                        img = value.image.url_image;
                    }
                    trHTML += '<td class="row-img-user"><img src="{{ URL('storage/') }}/'+ img +'" alt="No disponible" class="ui tiny rounded image">'+'</td>';
                    trHTML += '<td>'+ value.profile.name +'</td>';
                    trHTML += '<td>' + value.email + '</td>';
                    trHTML += '<td class="center aligned">';
                    trHTML += '<button id="' + value.id + '" class="ui '+ ((value.editable && value.id !== +localStorage.getItem('id')) ? '' : 'disabled') + ' negative button user_delete" '+ (value.editable ? '' : 'data-tooltip="No se puede eliminar este usuario"') + '>';
                    trHTML += 'Eliminar';
                    trHTML += '</button>';
                    trHTML += '<button id="' + value.id + '" class="ui button user_modify">';
                    trHTML += 'Editar';
                    trHTML += '</button>';
                    trHTML += '</td> </tr>';
                });
                $('#users_table').html(trHTML);
                $('.user_delete').on('click',user_delete_function);
                $('.user_modify').on('click',function(){
                    $('.ui.error.message').hide();
                    var user_to_update = this.id;
                    $('#id_to_update').text(this.id);
                    jQuery.ajax({
                        type: 'GET',
                        url: '/api/users/'+user_to_update,
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function(data_user, status_user, jqXHR_user){
                            $('#form_modify_user .img_preview').attr('src', '');
                            $('#form_modify_user .img_preview').hide();
                            $('#form_modify_user .placeholder').show();
                            $('#form_modify_user').form('reset');

                            $('.form_modify input[name=user]').val(data_user.user);
                            $('.form_modify input[name=email]').val(data_user.email);
                            $('.form_modify select[name=profile_id]').val(data_user.profile_id);

                            $('.form_modify select[name=profile_id]').prop('disabled', !data_user.editable);

                            if (data_user.image) {
                                $('#form_modify_user .img_preview').attr('src', '/storage/' + data_user.image.url_image);

                                $('#form_modify_user .img_preview').show();
                                $('#form_modify_user .placeholder').hide();
                            }

                            $('#modal_modify')
                                .modal({closable: false, observeChanges: true })
                                .modal('show');
                        },
                        error: function(){
                            new PNotify({
                                title: 'Operación fallida',
                                text: 'Ha ocurrido un error inesperado',
                                type: 'error'
                            });
                        }
                    });
                });
            },
            error: function (jqXHR, status) {
                new PNotify({
                    title: 'Operación fallida',
                    text: 'Ha ocurrido un error inesperado',
                    type: 'error'
                });
            }
        });
    }

    fullTable(1)
    // Validación formulario New User
    $('#form_new_user').form({
        revalidate: true,
        fields: {
            user: {
                identifier: 'user',
                rules: [
                    {
                        type : 'regExp[^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜñÑ0-9\\s-_]+$]',
                        prompt : 'Nombre: Entrada no valida'
                    },
                    {
                        type : 'minLength[3]',
                        prompt : 'Nombre: Al menos 3 caracteres'
                    },
                    {
                        type : 'maxLength[16]',
                        prompt : 'Nombre: Máximo 16 caracteres'
                    }
                ]
            },
            email: {
                identifier: 'email',
                rules: [
                    {
                        type : 'email',
                        prompt : 'Correo: La dirección de correo no es válida'
                    },
                    {
                        type : 'maxLength[191]',
                        prompt : 'Email: Máximo 191 caracteres'
                    }
                ]
            },
            password: {
                identifier: 'password',
                rules: [
                    {
                        type   : 'minLength[6]',
                        prompt : 'Contraseña: Al menos 6 caracteres'
                    },
                    {
                        type : 'maxLength[30]',
                        prompt : 'Nombre: Máximo 30 caracteres'
                    }
                ]
            },
            profile_id: {
                identifier: 'profile_id',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Perfil: Seleccione un perfil'
                    }
                ]
            }
        }
    });
    // Validación Modify User
    $('#form_modify_user').form({
        revalidate: true,
        fields: {
            user: {
                identifier: 'user',
                rules: [
                    {
                        type : 'regExp[^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜñÑ0-9\\s-_]+$]',
                        prompt : 'Nombre: Entrada no valida'
                    },
                    {
                        type : 'minLength[3]',
                        prompt : 'Nombre: Al menos 3 caracteres'
                    },
                    {
                        type : 'maxLength[16]',
                        prompt : 'Nombre: Máximo 16 caracteres'
                    }
                ]
            },
            email: {
                identifier: 'email',
                rules: [
                    {
                        type   : 'email',
                        prompt : 'Correo: La dirección de correo no es válida'
                    },
                    {
                        type : 'maxLength[191]',
                        prompt : 'Email: Máximo 191 caracteres'
                    }
                ]
            },
            password: {
                identifier: 'password',
                optional: true,
                rules: [
                    {
                        type   : 'minLength[6]',
                        prompt : 'Contraseña: Al menos 6 caracteres'
                    },
                    {
                        type: 'maxLength[30]',
                        prompt : 'Contraseña: Maximo 30 caracteres'
                    }
                ]
            },
            profile_id: {
                identifier: 'profile_id',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Perfil: Seleccione un perfil'
                    }
                ]
            }
        }
    });
    $('#modals').modal({allowMultiple: false});
    // Esta función se manda a llamar cuando se hace click al botón de eliminar
    function user_delete_function(){
        // Al botón de modificar se le agrega un id al momento de crearlo, este id será el que usaremos para eliminary modificar
        var user_to_delete = this.id;
        $('#modal_delete').modal({
            // En el caso de confirmar el modal (o ventana) entrará a la siguiente función
            closable: false,
            onApprove: function(){
                // Se hace una petición AJAX tipo DELETE usando la ruta que proporcionó back-end para eliminar un usuario
                jQuery.ajax({
                    type: 'DELETE',
                    url: '/api/users/'+ user_to_delete +'',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    // En caso de que la petición se haga correctamente entrará a la siguiente función
                    success: function (data, status, jqXHR) {
                        // Dentro de la función se muestra el modal de 'Registro eliminado correctamente'
                        //$('#modal_delete_success').modal('show');
                        new PNotify({
                            title: 'Operación exitosa',
                            text: 'Usuario eliminado exitosamente',
                            type: 'success'
                        });

                        // Y se da al usuario 1.5 segundos para visualizar el modal antes de recargar la página
                        fullTable($('.current').text());
                    },
                    // En el caso de que la petición regrese un error, entra a la siguiente función
                    error: function (jqXHR, status) {
                        new PNotify({
                            title: 'Operación fallida',
                            text: 'Ha ocurrido un error inesperado',
                            type: 'error'
                        });
                    }
                });
            }
        })
        .modal('show');
    }
    $('.user_new').on('click',function(){
        $('.ui.error.message').hide();
        $('#form_new_user .img_preview').attr('src', '');
        $('#form_new_user .img_preview').hide();
        $('#form_new_user .placeholder').show();
        $('#form_new_user').form('clear');
        $('#modal_new').modal({closable: false, observeChanges: true }).modal('show');
    });
    $('.close.icon').on('click',function () {
        $(this).parent().parent().modal('hide');
    });
});
</script>
@endsection
