@extends('index')
@section('content')
	<!--<div class="padding-30"><p class="ui medium header">Áreas</p></div>-->
    <div class="ui twelve wide column">
        <div class="padding-30">
			<p class="ui medium header">Áreas</p>
            <table class="ui selectable celled table padded">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <!-- <th>Ultima actualización</th> -->
                        <th width="250" class="center aligned">Acción</th>
                    </tr>
                </thead>
                <tbody id="areas_table">
                </tbody>
            </table>
            <div class="ui pagination menu">
                <a class="active item prev_all">
                    <i class="angle double left icon"></i>
                </a>
                <a class="active item prev">
                    <i class="angle left icon"></i>
                </a>
                <a class="active item current">1</a>
                <a class="active item next">
                    <i class="angle right icon"></i>
                </a>
                <a class="active item next_all">
                    <i class="angle double right icon"></i>
                </a>
            </div>
            <button id="new" class="ui right floated positive button">
                Agregar área
            </button>
        </div>
    </div>

    <div class="ui modal small areas">
        <div class="header">
            <label id="modaltittle">Áreas</label>
            <i class="close icon right" style="cursor:pointer;"></i>
        </div>
        <!-- pie chart, home, building,location arrow, map  -->
        <form class="ui form content" method="post" id="form_areas">
            <div class="ui column padded grid">
                <div class="column">
                    <div class="field">
                        <input type="hidden" name="id" id="id_area"/>
                        <label>Nombre:</label>
                        <input type="text" name="name" id="name_area" placeholder="Nombre del área">
                    </div>
                    <div class="ui actions right aligned container">
                        <button class="ui primary save_areas button">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="ui error message"></div>
        </form>
    </div>

    <div class="ui modal small message">
        <div class="header">
            <i class="cube box icon"></i>
            <label class="message_text"></label>
            <i class="close icon right" style="cursor:pointer;"></i>
        </div>
        <div class="actions">
            <div class="ui positive button">
                Aceptar
                <i class="checkmark icon"></i>
            </div>
        </div>
    </div>

    <div class="ui modal small delete">
        <div class="header">
            <i class="cube box icon"></i>
            ¿Está seguro que quiere eliminar esta área?
        </div>
        <div class="actions">
            <div class="ui negative button">No</div>
            <div class="ui approve blue button">Sí</i></div>
        </div>
    </div>

    <script type="text/javascript">
    var idBuilding = 0;
    $(function () {
        fullTable(1);
    });

    $('#form_areas .field input:not(input[type=file])').on('change', function (e) {
        $('.ui.error.message').hide();
        $('#form_areas').form('validate field', this.name)
    });

    $('#form_areas').form({
        revalidate: true,
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type : 'regExp[/^[a-zA-ZÀ-ÿ\u00f1\u00d10-9 ]+$/i]',
                        prompt : 'Nombre: No es posible el uso de esos caracteres'
                    },
                    {
                        type : 'minLength[3]',
                        prompt : 'Nombre: Al menos 3 caracteres'
                    },
                    {
                        type : 'maxLength[40]',
                        prompt : 'Nombre: Máximo 40 caracteres'
                    }
                ]
            }
        }
    });

    $('.close.icon').on('click',function () {
        $(this).parent().parent().modal('hide');
    });

    $('.prev_all').on('click',function () {
        fullTable($('.prev_all').data('value'));
    });

    $('.prev').on('click',function () {
        fullTable($('.prev').data('value'));
    });

    $('.next').on('click',function () {
        fullTable($('.next').data('value'));
    });

    $('.next_all').on('click',function () {
        fullTable($('.next_all').data('value'));
    });

    $('#new').on('click',function () {
        $('#id_area').val('0');
        $('.ui.error.message').hide();
        $('#form_areas').form('clear');
		$("#modaltittle").empty();
		$("#modaltittle").text('Agregar área');
        $('.ui.modal.areas')
            .modal({closable: false, observeChanges: true })
            .modal('show');
    });

    $('#form_areas').submit(function (e) {
        e.preventDefault();
        if ($('#form_areas').form('is valid')) {
            var id = $('#id_area').val();
            if($('#name_area').val() != ""){
                if(id == 0) {
                    save($('#form_areas'));
                } else {
                    update($('#form_areas'),id);
                }
                fullTable();
            } else {
                $('.message_text').text('¡Aviso! Existen campos vacíos.');
                $('.ui.modal.message').modal('setting', 'closable', false).modal({
                    observeChanges: true,
                    onApprove: function(){
                    $('.ui.modal.areas')
                        .modal('setting', 'closable', false)
                        .modal({ observeChanges: true })
                        .modal('show');
                }}).modal('show');
            }
            //console.log(id);
        } else {
            $('.ui.error.message').show();
        }
    });

    function fullTable (page) {
        $.ajax({
            type: 'GET',
            url: '{{ url('/') }}/api/areas?page=' + page,
            dataType: 'json',
            success: function (data) {
                var trHTML = '';
                $(data.data).each(function(index, value){
                    trHTML += "<tr style='cursor:pointer;'>";
                    trHTML += "<td class='tr_data' id = '" + value.id + "'>" + value.name + "</td>";
                    <!--trHTML += "<td class='tr_data' id = '" + value.id + "'>" + value.updated_at + "</td>";-->
                    trHTML += "<td class='center aligned'>";
                    trHTML += "<button id='" + value.id + "' class='ui right button edit_area'>";
                    trHTML += "Editar";
                    trHTML += '</button>';
                    trHTML += "<button id='" + value.id + "' class='ui negative right button delete_area'>";
                    trHTML += "Eliminar";
                    trHTML += '</button>';
                    trHTML += '</td> </tr>';
                });
                $('#areas_table').html(trHTML);
                //$('tr .tr_data').on('click',getArea);
                $('.button.edit_area').on("click", getArea);
                $('.button.delete_area').on('click',deleteArea);
                $('.prev_all').data('value',1);
                $('.prev').data('value',(((data.current_page-1) > 0)?(data.current_page-1):1));
                $('.current').text(data.current_page);
                $('.next').data('value',((data.current_page+1) > data.last_page)?data.last_page:(data.current_page+1))
                $('.next_all').data('value',data.last_page);

                managePages(page, data)
            },
            error: function () {
                $('.message_text').text('Ha ocurrido un error al cargar la información, favor de contactar con el encargado.');
                $('.ui.modal.message')
                    .modal({closable: false, observeChanges: true })
                    .modal('show');
            }
        });
    }

    function save (frm) {
        $.ajax({
            type: 'POST',
            url: '{{ url('/') }}/api/areas',
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                //$('.message_text').text('Se ha guardado el área con éxito.');
                //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                new PNotify({
                    title: 'Operación exitosa',
                    text: 'Área agregada exitosamente',
                    type: 'success'
                });

                $('#name_area').val('');
                $('.ui.modal.areas').modal('hide');
            },
            error: function (response) {
				if(response.responseJSON.message !== undefined){
                    new PNotify({
                        title: 'Operación fallida',
                        text: response.responseJSON.message+'.',
                        type: 'error'
                    });
					//$('.message_text').text(response.responseJSON.message+".");
					//$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
				}	else{
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'Ha ocurrido un error al crear el área.',
                        type: 'error'
                    });
					//$('.message_text').text('Ha ocurrido un error al crear el área.');
					//$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
				}
            }
        });
    }

    function update (frm, id) {
        $.ajax({
            type: 'PUT',
            url: '{{ url('/') }}/api/areas/' + id,
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                //$('.message_text').text('Se ha modificado el área con éxito.');
                //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                new PNotify({
                    title: 'Operación exitosa',
                    text: 'Área modificada exitosamente',
                    type: 'success'
                });

                $('#name_area').val('');
                $('.ui.modal.areas').modal('hide');
            },
            error: function (response) {
                if(response.responseJSON.message !== undefined){
                    new PNotify({
                        title: 'Operación fallida',
                        text: response.responseJSON.message+'.',
                        type: 'error'
                    });
					//$('.message_text').text(response.responseJSON.message+".");
					//$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
				}	else{
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'Ha ocurrido un error al actualizar el área.',
                        type: 'error'
                    });
					//$('.message_text').text('Ha ocurrido un error al actualizar el área.');
					//$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
				}
            }
        });
    }

    function getArea () {
        var id = this.id;
        $('#form_areas').form('clear');
        $('.ui.error.message').hide();
        $.ajax({
            type: 'GET',
            url: '{{ url('/') }}/api/areas/' + id,
            dataType: 'json',
            success: function (data) {
                $("#modaltittle").empty();
				$("#modaltittle").text('Editar área');
				$('#id_area').val(data.id);
                $('#name_area').val(data.name);
                $('.ui.modal.areas')
                    .modal({closable: false, observeChanges: true })
                    .modal('show');
            },
            error: function () {
                $('.message_text').text('Ha ocurrido un error al obtener la información del área.');
                $('.ui.modal.message')
                    .modal({closable: false, observeChanges: true })
                    .modal('show');
            }
        });
    }

    function deleteArea () {
        var id = this.id;
        $('.ui.modal.delete').modal({
            observeChanges: true,
            onApprove: function(){
                $.ajax({
                    type: 'DELETE',
                    url: '{{ url('/') }}/api/areas/' + id,
                    success: function () {
                        fullTable();
                        //$('.message_text').text('Se ha eliminado el área con éxito.');
                        //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                        new PNotify({
                            title: 'Operación exitosa',
                            text: 'Área eliminada exitosamente',
                            type: 'success'
                        });
                    },
                    error: function (error) {
                        if(error.responseJSON.errors !== undefined)
						{
							new PNotify({
								title: 'Operación fallida',
								text: error.responseJSON.errors+'.'
							});
						}
						else
						{
							new PNotify({
								title: 'Operación fallida',
								text: 'Ha ocurrido un error inesperado.',
								type: 'error'
							});
						}						
						/*$('.message_text').text('Ha ocurrido un error al momento de eliminar el área.');
                        $('.ui.modal.message')
                            .modal({closable: false, observeChanges: true })
                            .modal('show');*/
                    }
                });
            }
        }).modal('setting', 'closable', false).modal('show');
    }
    </script>
@endsection
