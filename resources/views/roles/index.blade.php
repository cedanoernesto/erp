@extends('index')
@section('content')
<div class="ui twelve wide column">
	<div class="ui container" id="profiles_container">
		<p class="ui medium header">Roles</p>
		<table class="ui selectable celled table" id="tableRole">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Descripción</th>
					<th width="250">Acciones</th>
				</tr>
			</thead>
			<tbody id="roles">
			</tbody>
		</table>
		<!--Paginacion-->
		<div id="pag" class="ui pagination menu">
		</div>
		<button id="new" class="ui right floated positive button">
			Agregar rol
		</button>

		<!--Eliminar Elemento-->
		<div id="modal_cont_del" class="ui dimmer modals page transition">
			<div id="modal_del" class="ui small first coupled modal transition visible">
			  <div class="header">¿Está seguro que quiere eliminar este registro?</div>
			  <div class="actions">
                <div id="no" class="ui negative button">No</div>
                <div id="yes" class="ui approve blue button">Sí</i></div>
			  </div>
			</div>
			<div id="modal_del_ok" class="ui small second coupled modal transition">
			  <i class="right floated remove icon" style="float: right;cursor: pointer;"></i>
			  <div class="header" style="color: #44647a;">Registro eliminado con éxito!!</div>
			  <div class="actions">
				<button id="btn_end" class="ui approve button" style="color: white;background-color: #213543;"> Aceptar </button>
			  </div>
			</div>
		</div>

		<!--Editar Elemento-->
		<div id="modal_cont_edit" class="ui dimmer modals page transition">
			<div id="modal_edit" class="ui modal first coupled modal transition visible" style="top: 50px;">
				<!--<i class="close icon"></i>-->
				<div class="header">
					<span class="texti"></span> Rol
                    <i class="close icon right" style="cursor:pointer;"></i>
				</div>
				<div class="content" style="display: inline-flex;">
				   <form class="ui form" method="post" action="" style="width: 60%;" id="form">
						<div class="ui field">
							<span>Nombre</span>
						</div>
						<div class="ui input focus field" name="a">
							<input id="nombreRole" type="text" placeholder="Nombre" name="name">
						</div>
						<div class="ui field">
							<span>Descripción</span>
						</div>
						<div class="ui focus field" name="b">
							<textarea id="descripcionRole" placeholder="Descripción del perfil" name="description"></textarea>
						</div>
						<div class="ui error message">

						</div>
					</form>
					<div style="width: 25%;margin: 0 auto;text-align: right;">
						<div class="actions" style="margin-top: 60%;">
							<button id="save" class="ui blue button btn">Guardar </button>
						</div>
					</div>
				</div>
			</div>
			<div id="modal_save_ok" class="ui small second coupled modal transition">
				<i class="close icon"></i>
				<div class="header" style="color: #44647a;"><span class="texti_2"></span> !!</div>
				<div class="actions">
					<button id="btn_end" class="ui approve button" style="color: white;background-color: #213543;"> Aceptar </button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="{{ URL::asset('js/roles.js') }}"></script>
@endsection
