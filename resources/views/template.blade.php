<!DOCTYPE html>
<html>
<head>
	<title>
		Aprendoo
	</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
    <link rel="manifest" href="/img/manifest.json">
    <link rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="{{ URL::asset('css/master.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('css/semantic.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('css/dropdown.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/calendar.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/pnotify.custom.min.css') }}">
	<script type="text/javascript" src="{{ URL::asset('js/vendor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dropdown.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/calendar.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pnotify.custom.min.js') }}"></script>
</head>
<body>
	@yield('body')
</body>
</html>
