@extends('index')
@section('content')
    <div class="ui twelve wide column">
        <div class="padding-30">
			<p class="ui medium header">Periodos</p>
            <table class="ui celled structured table">
                <thead>
                    <tr>
                        <th rowspan="2">Nombre</th>
                        <th rowspan="2">Descripción</th>
                        <th colspan="2">Duración</th>
                        <th colspan='2'>Periodo para inscribirse</th>
                        <th rowspan="2" width="250">Acciones</th>
                    </tr>
                    <tr>
                        <th>Inicio</th>
                        <th>Término</th>
                        <th>Inicio</th>
                        <th>Término</th>
                    </tr>
                </thead>
                <tbody id="periods_table">
                    <!-- <tr>
                    <td>Nombre</td>
                    <td>Desc</td>
                    <td>Inicio</td>
                    <td>term</td>
                    <td>inicio</td>
                    <td>termino</td>
                    <td>accione</td>
                    </tr> -->
                </tbody>
            </table>
        </div>
        <div class="ui pagination menu">
            <a class="active item prev_all">
                <i class="angle double left icon"></i>
            </a>
            <a class="active item prev">
                <i class="angle left icon"></i>
            </a>
            <a class="active item current">1</a>
            <a class="active item next">
                <i class="angle right icon"></i>
            </a>
            <a class="active item next_all">
                <i class="angle double right icon"></i>
            </a>
        </div>

        <button id="user_add"  class="ui right floated right positive button period_new">
            Agregar Periodo
        </button>
    </div>

    <!--
    Modals de periodos
-->
<div id="modals" class="ui dimmer modals page transition">

    <div id="modal_delete" class="ui small first coupled modal">
        <div class="header">¿Está seguro que quiere eliminar este registro?</div>
        <div class="actions">
            <div class="ui negative button">No</div>
            <div id="yes" class="ui approve blue button">Sí</i></div>
        </div>
    </div>
    <div id="modal_delete_success" class="ui small second coupled modal">
        <div class="header">¡Registro eliminado con éxito!</div>
    </div>
    <!--
    Modal para agregar periodo
    -->
    <div id="modal_new" class="ui small modal">
        <div class="header">
            Agregar periodo
            <i class="close icon right" style="cursor:pointer;"></i>
        </div>
        <form id="form_new_period" class="ui form form_new">
            <div class="ui padded grid">
                <div class="column">
                    <div class="three fields">
                        <span id="id_to_update" style="display: none"></span>
                        <div class="field">
                            <label>Nombre:</label> <input type="text"  id="name" name="name" />
                        </div>

                        <div class="field ui calendar" id="calendar_add_start">
                            <label>Fecha de incio:</label>
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input type="text" id="start_date" name="start_date" />
                            </div>
                        </div>

                        <div class="field ui calendar" id="calendar_add_end">
                            <label>Fecha de término:</label>
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input type="text" id="end_date" name="end_date" />
                            </div>
                        </div>
                    </div>

                    <div class="two fields">
                        <div class="field">
                            <label>Periodo de Inscripción:</label>
                            <select class="inscriptions"  id="inscription_periods_id1" name="inscription_period_id">
                                <option value="add_new_inscription">Agregar fechas de inscripción</option>
                            </select>
                            <!-- <span class="ui green right labeled icon button btn_new_inscription">Nuevo<i class="add icon"></i></span> -->
                        </div>

                        <div class="field">
                            <label>Tipo de Periodo:</label>
                            <select class="TypePeriods" id ="type_period_id" name="type_period_id">
                                <option value="add_new_type">Agregar tipo de periodo</option>
                            </select>
                            <!-- <span class="ui green right labeled icon button btn_new_type_period">Nuevo<i class="add icon"></i></span> -->
                        </div>
                    </div>
                    <div class="ui right aligned container field">
                        <button id="btn_new_period" class="ui blue approve button">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="ui error message"></div>
        </form>
    </div>
    <div id="modal_new_inscriptions" class="ui small modal">
        <div class="header">
            Agregar inscripción a periodo
            <i class="close icon right" style="cursor:pointer;"></i>
        </div>
        <div class="content">
            <form id="form_new_inscripcion_period" class="ui form">
                <div class="ui padded grid">
                    <div class="column">
                        <div class="two fields">
                            <div class="field ui calendar" id="calendar_add_end">
                                <label>Fecha de incio:</label>
                                <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <input type="text" id="start_date" name="start_date" />
                                </div>
                            </div>

                            <div class="field ui calendar" id="calendar_add_end">
                                <label>Fecha de término:</label>
                                <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <input type="text" id="end_date" name="end_date" />
                                </div>
                            </div>
                        </div>

                        <div class="ui right aligned container field">
                            <button id="btn_new_inscription_period"  class="ui blue button">Guardar</button>
                        </div>
                    </div>
                </div>
                <div class="ui error message"></div>
            </form>
        </div>
    </div>
    <div id="modal_new_type" class="ui small modal">
        <div class="header">
            Agregar tipo de periodo
            <i class="close icon right" style="cursor:pointer;"></i>
        </div>
        <form id="form_new_type" class="ui form">
            <div class="ui padded grid">
                <div class="column">

                    <div class="field">
                        <label>Nombre de periodo:</label> <input type="text" name="name" />
                    </div>

                    <div class="ui right aligned container field">
                        <button id="btn_new_type" class="ui blue button">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="ui error message"></div>
        </form>
    </div>

    <div id="modal_new_success" class="ui small second coupled modal">
        <div class="header">¡Registro agregado con éxito!</div>
    </div>

    <!--
    Modal para modificar periodo
    -->
    <div id="modal_modify" class="ui small modal">
        <div class="header">
            Editar Periodo
            <i class="close icon right" style="cursor:pointer;"></i>
        </div>

        <form id="form_modify_period" class="ui form form_modify">
            <div class="ui padded grid">
                <div class="column">
                    <span id="id_to_update" style="display: none"></span>
                    <div class="three fields">
                        <div class="field">
                            <label>Nombre:</label> <input type="text"  id="name" name="name" />
                        </div>
                        <div class="field ui calendar" id="calendar_mod_start">
                            <label>Fecha de incio:</label>
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input type="text" id="start_date" name="start_date" />
                            </div>
                        </div>

                        <div class="field ui calendar" id="calendar_mod_end">
                            <label>Fecha de término:</label>
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input type="text" id="end_date" name="end_date" />
                            </div>
                        </div>
                    </div>

                    <div class="two fields">
                        <div class="field">
                            <label>Periodo de Inscripción:</label>
                            <select class="inscriptions"  id="inscription_periods_id1" name="inscription_period_id">
                                <option value="add_new_inscription">Agregar fechas de inscripción</option>
                            </select>
                            <!-- <span class="ui green right labeled icon button btn_new_inscription">Nuevo<i class="add icon"></i></span> -->
                        </div>

                        <div class="field">
                            <label>Tipo de Periodo:</label>
                            <select class="TypePeriods" id ="type_period_id" name="type_period_id">
                                <option value="add_new_type">Agregar tipo de periodo</option>
                            </select>
                            <!-- <span class="ui green right labeled icon button btn_new_type_period">Nuevo<i class="add icon"></i></span> -->
                        </div>
                    </div>

                    <div class="ui right aligned container field">
                        <button id="btn_modify_period" class="ui blue approve button">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="ui error message"></div>
        </form>
    </div>

    <div id="modal_modify_success" class="ui small second coupled modal">
        <div class="header">¡Registro modificado con éxito!</div>
    </div>
</div>
<!--
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
  webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');
</script>
-->
<script type="text/javascript">
// Esta función se ejecutará en cuanto la página cargue
$(function() {
    var section_to_update = null;
    var data_save_new;
    var data_save_mod;
    var form_type;

    $('#tab_period').on('click', function (e) {
        $('.ui.error.message').hide();
    });

    $('#tab_inscriptions').on('click', function (e) {
        $('.ui.error.message').hide();
    });

    $('#tab_type').on('click', function (e) {
        $('.ui.error.message').hide();
    });

    $('select.inscriptions').change(function(){
      var data= $(this).val();
      if(data == 'add_new_inscription'){
          data_save_new = {name: $("#form_new_period input[name=name]").val(),
          start_date: $("#form_new_period input[name=start_date]").val(),
          end_date: $("#form_new_period input[name=end_date]").val(),
          inscription_period: $('#form_new_period input[name=inscription_period_id]').val(),
          type_period: $('#form_new_period input[name=type_period_id]').val()};

          data_save_mod = {name: $("#form_modify_period input[name=name]").val(),
          start_date: $("#form_modify_period input[name=start_date]").val(),
          end_date: $("#form_modify_period input[name=end_date]").val(),
          inscription_period: $('#form_modify_period input[name=inscription_period_id]').val(),
          type_period: $('#form_modify_period input[name=type_period_id]').val()};

          $('#modal_new_inscriptions')
              .modal({closable: false, observeChanges: true })
              .modal('show');

          $('.ui.calendar').calendar(calendarConf);
      }
    });
    $('select.TypePeriods').change(function(){
      var data= $(this).val();
      if(data == 'add_new_type'){
          data_save_new = {name: $("#form_new_period input[name=name]").val(),
          start_date: $("#form_new_period input[name=start_date]").val(),
          end_date: $("#form_new_period input[name=end_date]").val(),
          inscription_period: $('#form_new_period input[name=inscription_period_id]').val(),
          type_period: $('#form_new_period input[name=type_period_id]').val()};

          data_save_mod = {name: $("#form_modify_period input[name=name]").val(),
          start_date: $("#form_modify_period input[name=start_date]").val(),
          end_date: $("#form_modify_period input[name=end_date]").val(),
          inscription_period: $('#form_modify_period input[name=inscription_period_id]').val(),
          type_period: $('#form_modify_period input[name=type_period_id]').val()};

          $('#modal_new_type')
              .modal({closable: false, observeChanges: true })
              .modal('show');
          $('.ui.calendar').calendar(calendarConf);
      }
    });

    var calendarConf = {
        type: 'date',
        minDate: new Date(),
        formatter: {
            date: function (date, settings) {
                //return a formatted string representing the date of 'date'
                return moment(date).format('YYYY-MM-DD')
            },
        }
    };

    $('.menu .item').tab();
    // Petición para el index, donde nos regresará un objeto JSON como respuesta
    // $('.prev_all').on('click',function () {
    $('.prev_all').on('click',function () {
        fullTable($('.prev_all').data('value'));
    });
    $('.prev').on('click',function () {
        fullTable($('.prev').data('value'));
    });
    $('.next').on('click',function () {
        fullTable($('.next').data('value'));
    });
    $('.next_all').on('click',function () {
        fullTable($('.next_all').data('value'));
    });
    $('#new').on('click',function () {
        $('#id_building').val('0');
        $('.ui.modal.buildings')
            .modal('setting', 'closable', false)
            .modal({closable: false, observeChanges: true })
            .modal('show');
    });

    function fullTable (page) {
        jQuery.ajax({
            type: 'GET',
            url: '/api/periods?page='+page,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            // En caso de que la petición se se haga correctamente entrará a la siguiente función
            success: function (data, status, jqXHR) {
                $('.prev_all').data('value',1);
                $('.prev').data('value',(((data.current_page-1) > 0)?(data.current_page-1):1));
                $('.current').text(data.current_page);
                $('.next').data('value',((data.current_page+1) > data.last_page)?data.last_page:(data.current_page+1))
                $('.next_all').data('value',data.last_page);

                managePages(page, data);

                var trHTML = '';
                $(data.data).each(function(index, value){
                    trHTML += '<tr>';
                    trHTML += '<td>' + value.name + '</td>';
                    trHTML += '<td class="long">' + value.type_period.name + '</td>';
                    trHTML += '<td>' + value.start_date + '</td>';
                    trHTML += '<td>' + value.end_date + '</td>';
                    trHTML += '<td>' + value.inscription.start_date + '</td>';
                    trHTML += '<td>' + value.inscription.end_date + '</td>';
                    trHTML += '<td class="center aligned">';
                    trHTML += '<button id="' + value.id + '" class="ui negative button period_delete">';
                    trHTML += 'Eliminar';
                    trHTML += '</button>';
                    trHTML += '<button id="' + value.id + '" class="ui button period_modify">';
                    trHTML += 'Editar';
                    trHTML += '</button>';
                    trHTML += '</td> </tr>';
                });
                $('#periods_table').html(trHTML);
                $('.period_delete').on('click',function(){
                    var period_to_delete = this.id;
                    $('#modal_delete').modal({
                        // En el caso de confirmar el modal (o ventana) entrará a la siguiente función
                        closable: false,
                        observeChanges: true,
                        onApprove: function(){
                            jQuery.ajax({
                                type: 'DELETE',
                                url: '/api/periods/'+ period_to_delete ,
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                // En caso de que la petición se haga correctamente entrará a la siguiente función
                                success: function (data, status, jqXHR) {
                                    // Dentro de la función se muestra el modal de 'Registro eliminado correctamente'
                                    //$('#modal_delete_success').modal('show');
                                    new PNotify({
                                        title: 'Operación exitosa',
                                        text: 'Periodo eliminado exitosamente',
                                        type: 'success'
                                    });
                                    // Y se da al usuario 1.8 segundos para visualizar el modal antes de recargar la página
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1500);
                                },
                                // En el caso de que la petición regrese un error, entra a la siguiente función
                                error: function (jqXHR, status) {
                                    // alert("Error delete user");
                                    new PNotify({
                                        title: 'Operación fallida',
                                        text: 'Ha ocurrido un error inesperado',
                                        type: 'error'
                                    });
                                }
                            });
                        }
                    }).modal('show');

                });
                $('.period_modify').on('click',function(){
                    form_type = 2;
                    $('#form_modify_period .inscriptions option:eq(1)').prop('selected', true);
                    $('#form_modify_period .TypePeriods option:eq(1)').prop('selected', true);
                    section_to_update = this.id;
                    $('#id_to_update').text(this.id);

                    jQuery.ajax({
                        type: 'GET',
                        url: '/api/periods/'+section_to_update,
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function(data, status, jqXHR){
                            $('.ui.error.message').hide();
                            $('.form_modify input[name=name]').val(data.name);
                            $('.form_modify input[name=start_date]').val(data.start_date);
                            $('.form_modify input[name=end_date]').val(data.end_date);

                            $('#form_modify_period .inscriptions option:eq('+ data.inscription_period_id +')').prop('selected', true);
                            $('#form_modify_period .TypePeriods option:eq('+ data.type_period_id +')').prop('selected', true);
                            console.log(data);

                            $('#modal_modify')
                                .modal({closable: false, observeChanges: true })
                                .modal('show');

                            $('.ui.calendar').calendar(calendarConf);
                        },
                        error: function(){
                            // alert('error get 1 user');

                        }
                    });
                });
            },
            //  En caso de que la petición no se haga correctamente entrará a la siguiente
            error: function (jqXHR, status) {
                //          	alert("Error get periodos");
            }
        });
    }

    fullTable(1);
    // Las siguientes dos peticiones son para cargar las opciones en el select de inscripciones y tipo de periodos
    // inscripciones
    jQuery.ajax({
        type: 'GET',
        url: '/api/inscriptions',
        dataType: 'json',
        success: function(data) {
            var inscriptions_options = '';
            $(data).each(function(index,value){
                inscriptions_options += '<option value="' + value.id + '">';
                inscriptions_options += value.start_date +' - ';
                inscriptions_options += value.end_date;
                inscriptions_options += '</option>'
            });
            $('.inscriptions').append(inscriptions_options);
        },
        error: function(){
            // alert('error get inscripciones');
        }
    });
    // Periodos
    jQuery.ajax({
        type: 'GET',
        url: '/api/type_periods',
        dataType: 'json',
        success: function(data) {
            var TypePeriods_options = '';
            $(data).each(function(index,value){
                TypePeriods_options += '<option value="' + value.id + '">';
                TypePeriods_options += value.name;
                TypePeriods_options += '</option>'
            });
            $('.TypePeriods').append(TypePeriods_options);
        },
        error: function(){
            // alert('error get tipo periodo');
        }
    });
    // Editar periodos
    $('#form_modify_period').submit(function(e){
        e.preventDefault();
        if($('#form_modify_period').form('is valid')){
            var first_date = $("#form_modify_period input[name=start_date]").val();
            var second_date = $("#form_modify_period input[name=end_date]").val();
            var first_inscription = $('#form_modify_period select.inscriptions option:selected').text().substr(0,10);

            if( moment(first_date).isBefore(second_date, 'day')){
                if(!moment(first_date).isBefore(new Date(), 'day')){
                    if(moment(first_inscription).isBefore(first_date)){
                        var form_mod = new FormData($("#form_modify_period")[0]);
                        form_mod.append('_method', 'put');
                        $.ajax({
                            url: '/api/periods/'+ $('#id_to_update').text(),
                            type: 'POST',
                            dataType: 'json',
                            data: form_mod,
                            processData: false,
                            contentType: false,
                            success: function(result){
                                //$('#modal_modify_success').modal('show');
                                new PNotify({
                                    title: 'Operación exitosa',
                                    text: 'Periodo modificado exitosamente',
                                    type: 'success'
                                });

                                setTimeout(function () {
                                    location.reload();
                                }, 1500);
                            },
                            error: function(er){
                                // console.log(er);
                                new PNotify({
                                    title: 'Operación fallida',
                                    text: 'Ha ocurrido un error inesperado',
                                    type: 'error'
                                });
                            }
                        });
                    }else{
                        new PNotify({
                            title: 'Operación fallida',
                            text: 'La fecha de inicio no puede ser antes que la fecha de inscripción',
                            type: 'error'
                        });
                        $('#form_modify_period .ui.error.message').hide();
                    }
                }else{
                    // alert("La fecha de inicio no puede ser antes que la fecha actual");
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'La fecha de inicio no puede ser antes que la fecha actual',
                        type: 'error'
                    });
                    $('#form_modify_period .ui.error.message').hide();
                }
            }else{
                // alert("La fecha de inicio no tiene que ser mayor que la fecha de término");
                new PNotify({
                    title: 'Operación fallida',
                    text: 'La fecha de inicio no tiene que ser mayor que la fecha de término',
                    type: 'error'
                });
                $('#form_modify_period .ui.error.message').hide();
            }
        } else {
            $('#form_modify_period .ui.error.message').show();
        }
        // e.preventDefault();
        // jQuery.ajax({
        //     type: 'PUT',
        //     url: '/api/periodos/'+$('#id_to_update').text(),
        // 	data: $('#form_modify_period').serialize(),
        // 	contentType: 'application/json; charset=utf-8',
        //     dataType: 'json',
        //     success: function (data) {
        // 		$('#modal_new_success').modal('show');
        // 		setTimeout(function () {
        // 			location.reload();
        // 		}, 1800);
        //      },
        //      error: function (jqXHR, status) {
        // 		 alert('error put periodo');
        // 		 console.log(jqXHR);
        //      }
        // });
    });
    // Eliminar periodos

    // Esta función cancela la acción por default del submit del formulario para hacer un POST de los datos del form
    $('#form_new_period').submit(function(e){
        e.preventDefault();
        if($('#form_new_period').form('is valid')){
            $("#btn_new_period").prop('disabled', true);
            var first_date = $("#form_new_period input[name=start_date]").val();
            var second_date = $("#form_new_period input[name=end_date]").val();
            var first_inscription = $('#form_new_period select.inscriptions option:selected').text().substr(0,10);

            if( moment(first_date).isBefore(second_date, 'day')){
                if(!moment(first_date).isBefore(new Date(), 'day')){
                    if(moment(first_inscription).isBefore(first_date)){
                        jQuery.ajax({
                            type: 'POST',
                            url: '/api/periods',
                            data: $('#form_new_period').serialize(),
                            dataType: 'json',
                            success: function (data) {
                                //$('#modal_new_success').modal('show');
                                $("#btn_new_period").prop('disabled', false);
                                new PNotify({
                                    title: 'Operación exitosa',
                                    text: 'Periodo agregado exitosamente',
                                    type: 'success'
                                });
                                setTimeout(function () {
                                    location.reload();
                                }, 1500);
                            },
                            error: function (jqXHR, status) {
                                //  alert('error post periodo');
                                //  console.log(jqXHR, status);
                                new PNotify({
                                    title: 'Operación fallida',
                                    text: 'Ha ocurrido un error inesperado',
                                    type: 'error'
                                });
                            }
                        });
                    }else{
                        new PNotify({
                            title: 'Operación fallida',
                            text: 'La fecha de inicio no puede ser antes que la fecha de inscripción',
                            type: 'error'
                        });
                        $('#form_new_period .ui.error.message').hide();
                    }
                }else{
                    // alert("La fecha de inicio no puede ser antes que la fecha actual");
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'La fecha de inicio no puede ser antes que la fecha actual',
                        type: 'error'
                    });
                    $('#form_new_period .ui.error.message').hide();
                }
            }else{
                // alert("La fecha de inicio no tiene que ser mayor que la fecha de término");
                new PNotify({
                    title: 'Operación fallida',
                    text: 'La fecha de inicio no tiene que ser mayor que la fecha de término',
                    type: 'error'
                });
                $('#form_new_period .ui.error.message').hide();
            }
        } else {
            $('#form_new_period .ui.error.message').show();
        }
        $("#btn_new_period").prop('disabled', false);
    });
    // $('#tab_inscriptions').on('click',function(){
    //     data_save_new = {name: $("#form_new_period input[name=name]").val(), start_date: $("#form_new_period input[name=start_date]").val(), end_date: $("#form_new_period input[name=end_date]").val()};
    //     data_save_mod = {name: $("#form_modify_period input[name=name]").val(), start_date: $("#form_modify_period input[name=start_date]").val(), end_date: $("#form_modify_period input[name=end_date]").val()};
    // });
    // $('#tab_type').on('click',function(){
    //     data_save_new = {name: $("#form_new_period input[name=name]").val(), start_date: $("#form_new_period input[name=start_date]").val(), end_date: $("#form_new_period input[name=end_date]").val()};
    //     data_save_mod = {name: $("#form_modify_period input[name=name]").val(), start_date: $("#form_modify_period input[name=start_date]").val(), end_date: $("#form_modify_period input[name=end_date]").val()};
    // });
    // Nuevo tipo de periodo
    $('#form_new_type').submit(function(e){
        e.preventDefault();
        if($('#form_new_type').form('is valid')){
            $("#btn_new_type").prop('disabled', true);
            jQuery.ajax({
                type: 'POST',
                url: '/api/type_periods',
                data: $('#form_new_type').serialize(),
                dataType: 'json',
                success: function (data) {
                    var TypePeriods_options = '';
                    TypePeriods_options += '<option value="' + data.id + '">';
                    TypePeriods_options += data.name;
                    TypePeriods_options += '</option>'
                    $('.TypePeriods').append(TypePeriods_options);
                    $('.form_new input[name=name]').val(data_save_new.name);
                    $('.form_new input[name=start_date]').val(data_save_new.start_date);
                    $('.form_new input[name=end_date]').val(data_save_new.end_date);
                    $('.form_modify input[name=name]').val(data_save_mod.name);
                    $('.form_modify input[name=start_date]').val(data_save_mod.start_date);
                    $('.form_modify input[name=end_date]').val(data_save_mod.end_date);
                    $('#form_modify_period .inscriptions option:eq(1)').prop('selected', true);
                    $('#form_modify_period .TypePeriods option:eq(1)').prop('selected', true);
                    $('#form_new_period .inscriptions option:eq(1)').prop('selected', true);
                    $('#form_new_period .TypePeriods option:eq(1)').prop('selected', true);
                    $("#form_new_type").form('clear');
                    $("#btn_new_type").prop('disabled', false);
                    new PNotify({
                        title: 'Operación exitosa',
                        text: 'Periodo agregado exitosamente',
                        type: 'success'
                    });
                    if(form_type == 1){
                        $('#modal_new')
                            .modal({closable: false, observeChanges: true })
                            .modal('show');
                    }else if(form_type == 2){
                        $('#modal_modify')
                            .modal({closable: false, observeChanges: true })
                            .modal('show');
                    }
                    // setTimeout(function () {
                    //     $("#tab_period").addClass("active");
                    //     $("#tab_period_content").addClass("active");
                    //     $("#tab_type").removeClass("active");
                    //     $("#tab_type_content").removeClass("active");
                    //     $("#modal_new").modal('show');
                    // }, 1500);
                },
                error: function (jqXHR, status) {
                    //  alert('error post periodo');
                    //  console.log(jqXHR, status);
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'Ha ocurrido un error inesperado',
                        type: 'error'
                    });
                }
            });
        } else {
            $('#form_new_type .ui.error.message').show();
        }
        $("#btn_new_type").prop('disabled', false);
    });
    // Nuevo periodo de inscripciones
    $('#form_new_inscripcion_period').submit(function(e){
        e.preventDefault();
        if($('#form_new_inscripcion_period').form('is valid')){
            $("#btn_new_inscription_period").prop('disabled', true);
            var first_date = $("#form_new_inscripcion_period input[name=start_date]").val();
            var second_date = $("#form_new_inscripcion_period input[name=end_date]").val();
            if( moment(first_date).isBefore(second_date, 'day')){
                if(!moment(first_date).isBefore(new Date(), 'day')){
                    jQuery.ajax({
                        type: 'POST',
                        url: '/api/inscriptions',
                        data: $('#form_new_inscripcion_period').serialize(),
                        dataType: 'json',
                        success: function (data) {
                            var inscriptions_options = '';
                            inscriptions_options += '<option value="' + data.id + '">';
                            inscriptions_options += data.start_date +' - ';
                            inscriptions_options += data.end_date;
                            inscriptions_options += '</option>'
                            $('.inscriptions').append(inscriptions_options);
                            $('.form_new input[name=name]').val(data_save_new.name);
                            $('.form_new input[name=start_date]').val(data_save_new.start_date);
                            $('.form_new input[name=end_date]').val(data_save_new.end_date);
                            $('.form_modify input[name=name]').val(data_save_mod.name);
                            $('.form_modify input[name=start_date]').val(data_save_mod.start_date);
                            $('.form_modify input[name=end_date]').val(data_save_mod.end_date);
                            $('#form_modify_period .inscriptions option:eq(1)').prop('selected', true);
                            $('#form_modify_period .TypePeriods option:eq(1)').prop('selected', true);
                            $('#form_new_period .inscriptions option:eq(1)').prop('selected', true);
                            $('#form_new_period .TypePeriods option:eq(1)').prop('selected', true);
                            $("#form_new_inscripcion_period").form('clear');
                            $("#btn_new_inscription_period").prop('disabled', false);
                            new PNotify({
                                title: 'Operación exitosa',
                                text: 'Periodo agregado exitosamente',
                                type: 'success'
                            });
                            if(form_type == 1){
                                $('#modal_new')
                                    .modal({closable: false, observeChanges: true })
                                    .modal('show');
                            }else if(form_type == 2){
                                $('#modal_modify')
                                    .modal({closable: false, observeChanges: true })
                                    .modal('show');
                            }
                            // setTimeout(function () {
                            //     $("#tab_period").addClass("active");
                            //     $("#tab_period_content").addClass("active");
                            //     $("#tab_inscriptions").removeClass("active");
                            //     $("#tab_inscriptions_content").removeClass("active");
                            //     $("#modal_new").modal('show');
                            // }, 1500);
                        },
                        error: function (jqXHR, status) {
                            //  alert('error post periodo');
                            //  console.log(jqXHR, status);
                            new PNotify({
                                title: 'Operación fallida',
                                text: 'Ha ocurrido un error inesperado',
                                type: 'error'
                            });
                        }
                    });
                }else{
                    // alert("La fecha de inicio no puede ser antes que la fecha actual");
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'La fecha de inicio no puede ser antes que la fecha actual',
                        type: 'error'
                    });
                    $('#form_new_inscripcion_period .ui.error.message').hide();
                }
            }else{
                // alert("La fecha de inicio no tiene que ser mayor que la fecha de término");
                new PNotify({
                    title: 'Operación fallida',
                    text: 'La fecha de inicio no tiene que ser mayor que la fecha de término',
                    type: 'error'
                });
                $('#form_new_inscripcion_period .ui.error.message').hide();
            }
        } else {
            $('#form_new_inscripcion_period .ui.error.message').show();
        }
        $("#btn_new_inscription_period").prop('disabled', false);
    });

    $('.period_new').on('click',function(){
        form_type = 1;
        $('form').form('clear');
        $('#form_new_period .inscriptions option:eq(1)').prop('selected', true);
        $('#form_new_period .TypePeriods option:eq(1)').prop('selected', true);
        $('#modal_new')
            .modal({closable: false, observeChanges: true })
            .modal('show');
        $('.ui.calendar').calendar(calendarConf);
        $('.ui.error.message').hide();
    });
    $('.period_delete').on('click',function(){
        $('#modal_delete')
            .modal({closable: false, observeChanges: true })
            .modal('show');
    });
    // $('.period_modify').on('click',function(){
    //     form_type = 2;
    //     $('#modal_modify')
    //         .modal({ observeChanges: true })
    //         .modal('show');
    // });

    //validaciones para agregar periodo
    $('#form_new_type').form({
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type : 'minLength[3]',
                        prompt : 'Nombre de periodo: Al menos 3 caracteres'
                    },
                    {
                        type : 'maxLength[25]',
                        prompt : 'Nombre de periodo: Máximo 25 caracteres'
                    },
                    {
                        type : 'regExp[/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜñÑ/\\s-]+$/i]',
                        prompt : 'Nombre de periodo: No es posible el uso de esos caracteres'
                    }
                ]
            }
        }
    });
    $("#form_new_inscripcion_period").form({
        fields: {
            start_date: {
                identifier: 'start_date',
                rules: [
                    {
                        type: "regExp[^\\d{4}[\\/\\-](0?[1-9]|1[012])[\\/\\-](0?[1-9]|[12][0-9]|3[01])$]",
                        prompt: "Fecha de inicio: Fecha no válida"
                    }
                ]
            },
            end_date: {
                identifier: 'end_date',
                rules: [
                    {
                        type: "regExp[^\\d{4}[\\/\\-](0?[1-9]|1[012])[\\/\\-](0?[1-9]|[12][0-9]|3[01])$]",
                        prompt: "Fecha de término: Fecha no válida"
                    }
                ]
            }
        }
    });
    $('#form_new_period').form({
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type : 'regExp[/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜñÑ/\\s-]+$/i]',
                        prompt : 'Nombre: No es posible el uso de esos caracteres'
                    },
                    {
                        type : 'maxLength[25]',
                        prompt : 'Nombre de periodo: Máximo 25 caracteres'
                    },
                    {
                        type : 'minLength[3]',
                        prompt : 'Nombre: Al menos 3 caracteres'
                    }
                ]
            },
            start_date:{
                identifier:'start_date',
                rules: [
                    {
                        type: "regExp[^\\d{4}[\\/\\-](0?[1-9]|1[012])[\\/\\-](0?[1-9]|[12][0-9]|3[01])$]",
                        prompt: "Fecha de inicio: Fecha no válida"
                    }
                ]

            },

            end_date:{
                identifier:'end_date',
                rules: [
                    {
                        type:"regExp[^\\d{4}[\\/\\-](0?[1-9]|1[012])[\\/\\-](0?[1-9]|[12][0-9]|3[01])$]",
                        prompt:"Fecha de término: Fecha no válida"
                    }
                ]
            },

            inscription_period_id:{
                identifier:'inscription_period_id',
                rules: [
                    {
                        type:"empty",
                        prompt:"Periodo de inscripción: Ingrese Periodo de inscripción"
                    }
                ]
            },

            type_period_id: {
                identifier:'type_period_id',
                rules: [
                    {
                        type:"empty",
                        prompt: "Tipo de periodo: Ingrese tipo periodo"
                    }
                ]
            },
        }
    });

    //validacion Modify Perioso
    $('#form_modify_period').form({
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type : 'regExp[/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜñÑ/\\s-]+$/i]',
                        prompt : 'Nombre: No es posible el uso de esos caracteres'
                    },
                    {
                        type : 'maxLength[25]',
                        prompt : 'Nombre de periodo: Máximo 25 caracteres'
                    },
                    {
                        type : 'minLength[3]',
                        prompt : 'Nombre: Al menos 3 caracteres'
                    },
                ]
            },
            start_date:{
                identifier:'start_date',
                rules: [
                    {
                        type: "regExp[^\\d{4}[\\/\\-](0?[1-9]|1[012])[\\/\\-](0?[1-9]|[12][0-9]|3[01])$]",
                        prompt: "Fecha de inicio: Fecha no válida"
                    }
                ]
            },

            end_date:{
                identifier:'end_date',
                rules: [
                    {
                        type:"regExp[^\\d{4}[\\/\\-](0?[1-9]|1[012])[\\/\\-](0?[1-9]|[12][0-9]|3[01])$]",
                        prompt:"Fecha de término: Fecha no válida"
                    }
                ]
            },

            inscription_period_id:{
                identifier:'inscription_period_id',
                rules: [
                    {
                        type:"empty",
                        prompt:"Periodo de inscripción: Ingrese periodo de inscripción"
                    }
                ]
            },

            type_period_id: {
                identifier:'type_period_id',
                rules: [
                    {
                        type:"empty",
                        prompt: "Tipo de periodo: Ingrese tipo periodo"
                    }
                ]
            },
        }


    });

    $('.close.icon').on('click',function () {
        $(this).parent().parent().modal('hide');
    });
    // $('#modal_new').modal({
    //onShow: function(){}
    //})
    //;
});


</script>
@endsection
