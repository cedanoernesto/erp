@extends('index')
@section('content')
<div class="ui container center aligned">
	<h1>Ingresar nuevo perfil.</h1>
</div>
<div class="ui container">
	<form class="ui form">
	  <div class="field">
	    <label>Nombre</label>
	    <input type="text" name="Nombre" placeholder="Ejemplo nombre">
			<label for ="txt_file ">Agregar Foto</label>
			<input type="file" id="file" name="file">
	  </div>
	  <div class="field">
	    <label>Descripcion</label>
	    <textarea>Ejemplo descripcion</textarea>
	  </div>
	  <div class="field">
	  	<select name="skills" multiple="" class="ui fluid dropdown">
	  		<option value="">Roles</option>
	  	<option value="modulo1">Rol 1</option>
	  	<option value="modulo2">Rol 2</option>
	  	</select>
	  </div>
	  <button class="ui button" type="submit">Submit</button>
	</form>
</div>
<script type="text/javascript">
	$('.ui.dropdown')
  .dropdown();
</script>
@endsection
