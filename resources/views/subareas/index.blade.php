@extends('index')
@section('content')
    <div class="ui twelve wide column">
        <div class="padding-30">
            <p class="ui medium header">Subáreas</p>
            <table class="ui selectable celled table padded">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Área</th>
                        <th width="250" class="center aligned">Acciones</th>
                    </tr>
                </thead>
                <tbody id="areas_table">
                </tbody>
            </table>
            <div class="ui pagination menu">
                <a class="active item prev_all">
                    <i class="angle double left icon"></i>
                </a>
                <a class="active item prev">
                    <i class="angle left icon"></i>
                </a>
                <a class="active item current">1</a>
                <a class="active item next">
                    <i class="angle right icon"></i>
                </a>
                <a class="active item next_all">
                    <i class="angle double right icon"></i>
                </a>
            </div>
            <button id="new" class="ui right floated positive button">
                Agregar subárea
            </button>
        </div>
    </div>

    <div class="ui modal small areas">
        <div class="header">
            <label id="modaltittle">Subáreas</label>
            <i class="close icon right" style="cursor:pointer;"></i>
        </div>
        <!-- pie chart, home, building,location arrow, map  -->
        <form class="ui form content" method="post" id="form_areas">
            <div class="ui column padded grid">
                <div class="column">
                    <div class="field">
                        <input type="hidden" name="id" id="id_area"/>
                        <label>Nombre:</label>
                        <input type="text" name="name" id="name_area" placeholder="Nombre de la subárea">
                    </div>
                    <div class="field">
                        <label>Área :</label>
                        <select class="ui search dropdown" id="cmb_areas" name="area_id"></select>
                    </div>
                    <div class="ui actions right aligned container">
                        <button class="ui primary save_areas button">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="ui error message"></div>
        </form>
    </div>

    <div class="ui modal small message">
        <div class="header">
            <i class="cube box icon"></i>
            <label class="message_text"></label>
            <i class="close icon right" style="cursor:pointer;"></i>
        </div>
        <div class="actions">
            <div class="ui positive button">
                Aceptar
                <i class="checkmark icon"></i>
            </div>
        </div>
    </div>

    <div class="ui modal small delete">
        <div class="header">
            <i class="cube box icon"></i>
            ¿Está seguro que quiere eliminar esta subárea?
        </div>
        <div class="actions">
            <div class="ui negative button">No</div>
            <div class="ui approve blue button">Sí</i></div>
        </div>
    </div>

    <script type="text/javascript">
    var idBuilding = 0;
    $(function () {
        fullTable(1);
    });

    $('#form_areas .field input:not(input[type=file])').on('change', function (e) {
        $('.ui.error.message').hide();
        $('#form_areas').form('validate field', this.name)
    });

    $('#form_areas').form({
        revalidate: true,
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type : 'regExp[/^[a-zA-ZÀ-ÿ\u00f1\u00d10-9 ]+$/i]', //Nueva expresion regular para los acentos
                        prompt : 'Nombre: No es posible el uso de esos caracteres'
                    },
                    {
                        type : 'minLength[3]',
                        prompt : 'Nombre: Al menos 3 caracteres'
                    },
                    {
                        type : 'maxLength[40]',
                        prompt : 'Nombre: Máximo 40 caracteres'
                    }
                ]
            },
            area_id: {
                identifier: 'area_id',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Asigne un área'
                    }
                ]
            }
        }
    });

    $('.close.icon').on('click',function () {
        $(this).parent().parent().modal('hide');
    });

    $('.prev_all').on('click',function () {
        fullTable($('.prev_all').data('value'));
    });

    $('.prev').on('click',function () {
        fullTable($('.prev').data('value'));
    });

    $('.next').on('click',function () {
        fullTable($('.next').data('value'));
    });

    $('.next_all').on('click',function () {
        fullTable($('.next_all').data('value'));
    });

    $('#new').on('click',function () {
        $('.ui.error.message').hide();
        fullAreas(0);
		$("#modaltittle").empty();
		$("#modaltittle").text('Agregar subárea');
        $('#cmb_areas').val('0');
        $('#name_area').val('');
        $('#form_areas').form('clear');
        $('.ui.modal.areas')
        .modal({closable: false, observeChanges: true })
        .modal('show');
    });

    $('#form_areas').submit(function (e) {
        e.preventDefault();
        if ($('#form_areas').form('is valid')) {
            var id = $('#id_area').val();
            if($('#name_area').val() != ""){
                if(id == 0) {
                    save($('#form_areas'));
                    $('#name_area').val('');
                } else {
                    update($('#form_areas'),id);
                    $('#name_area').val('');
                }
                fullTable();
            } else {
                $('.message_text').text('¡Aviso! Existen campos vacíos.');
                $('.ui.modal.message')
                .modal('setting', 'closable', false)
                .modal({
                    observeChanges: true,
                    onApprove: function(){
                        $('.ui.modal.areas')
                            .modal({closable: false, observeChanges: true })
                            .modal('show');
                    }
                }).modal('show');
            }
        } else {
            $('.ui.error.message').show();
        }
    });

    function fullTable (page) {
        $.ajax({
            type: 'GET',
            url: '{{ url('/') }}/api/subareas?page=' + page,
            dataType: 'json',
            success: function (data) {
                var trHTML = '';
                $(data.data).each(function(index, value){
                    trHTML += "<tr style='cursor:pointer;'>";
                    trHTML += "<td class='tr_data' id = '" + value.id + "'>" + value.name + "</td>";
                    trHTML += "<td class='tr_data' id = '" + value.id + "'>" + value.area.name + "</td>";
                    trHTML += "<td class='center aligned'>";
                    trHTML += "<button id='" + value.id + "' class='ui right button edit_area'>";
                    trHTML += "Editar";
                    trHTML += '</button>';
                    trHTML += "<button id='" + value.id + "' class='ui negative right button delete_area'>";
                    trHTML += "Eliminar";
                    trHTML += '</button>';
                    trHTML += '</td> </tr>';
                });
                $('#areas_table').html(trHTML);
                //$('tr .tr_data').on('click',getSubarea);
                $('.button.edit_area').on('click',getSubarea);
                $('.button.delete_area').on('click',deleteArea);
                $('.prev_all').data('value',1);
                $('.prev').data('value',(((data.current_page-1) > 0)?(data.current_page-1):1));
                $('.current').text(data.current_page);
                $('.next').data('value',((data.current_page+1) > data.last_page)?data.last_page:(data.current_page+1))
                $('.next_all').data('value',data.last_page);

                managePages(page, data)
            },
            error: function () {
                $('.message_text').text('Ha ocurrido un error al cargar la información, favor de contactar con el encargado.');
                $('.ui.modal.message')
                    .modal({closable: false, observeChanges: true })
                    .modal('show');
            }
        });
    }

    function save (frm) {
        $.ajax({
            type: 'POST',
            url: '{{ url('/') }}/api/subareas',
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                //$('.message_text').text('Se ha guardado la subarea con éxito.');
                //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                new PNotify({
                    title: 'Operación exitosa',
                    text: 'Subárea agregada exitosamente',
                    type: 'success'
                });

                $('.ui.modal.areas').modal('hide');
            },
            error: function (response) {
                if(response.responseJSON.message !== undefined){
                    new PNotify({
                        title: 'Operación fallida',
                        text: response.responseJSON.message+'.',
                        type: 'error'
                    });
                    //$('.message_text').text(response.responseJSON.message+".");
                    //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                }	else{
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'Ha ocurrido un error al crear la subárea.',
                        type: 'error'
                    });
                    //$('.message_text').text('Ha ocurrido un error al crear el subárea.');
                    //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                }
            }
        });
    }

    function update (frm, id) {
        $.ajax({
            type: 'PUT',
            url: '{{ url('/') }}/api/subareas/' + id,
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                //$('.message_text').text('Se ha modificado la subarea con éxito.');
                //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                new PNotify({
                    title: 'Operación exitosa',
                    text: 'Subárea modificada exitosamente',
                    type: 'success'
                });

                $('.ui.modal.areas').modal('hide');
            },
            error: function (response) {
                if(response.responseJSON.message !== undefined){
                    new PNotify({
                        title: 'Operación fallida',
                        text: response.responseJSON.message+'.',
                        type: 'error'
                    });
                    //$('.message_text').text(response.responseJSON.message+".");
                    //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                }	else{
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'Ha ocurrido un error al actualizar la subárea.',
                        type: 'error'
                    });
                    //$('.message_text').text('Ha ocurrido un error al actualizar el subárea.');
                    //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                }
            }
        });
    }

    function getSubarea () {
        var id = this.id;
        $('#form_areas').form('clear');

        $('.ui.error.message').hide();
        $.ajax({
            type: 'GET',
            url: '{{ url('/') }}/api/subareas/' + id,
            dataType: 'json',
            success: function (data) {
                fullAreas(data.area_id, function () {
                    $("#modaltittle").empty();
					$("#modaltittle").text('Editar subárea');
					$('#id_area').val(data.id);
                    $('#name_area').val(data.name);
                    $('.ui.modal.areas')
                        .modal({closable: false, observeChanges: true })
                        .modal('show');

                    setTimeout(function () {
                        $('#cmb_areas').dropdown('set selected', data.area_id);
                    }, 100);
                });
            },
            error: function () {
                $('.message_text').text('Ha ocurrido un error al obtener la información del subarea.');
                $('.ui.modal.message')
                    .modal({closable: false, observeChanges: true })
                    .modal('show');
            }
        });
    }

    function deleteArea () {
        var id = this.id;
        $('.ui.modal.delete').modal({
            onApprove: function(){
                $.ajax({
                    type: 'DELETE',
                    url: '{{ url('/') }}/api/subareas/' + id,
                    success: function () {
                        fullTable();
                        //$('.message_text').text('Se ha eliminado la subarea con éxito.');
                        //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                        new PNotify({
                            title: 'Operación exitosa',
                            text: 'Subárea eliminada exitosamente',
                            type: 'success'
                        });
                    },
                    error: function () {
                        $('.message_text').text('Ha ocurrido un error al momento de eliminar la subarea.');
                        $('.ui.modal.message')
                            .modal({closable: false, observeChanges: true })
                            .modal('show');
                    }
                });
            }
        })
        .modal({closable: false, observeChanges: true })
        .modal('show');
    }

    function fullAreas (area, callback) {
        $.ajax({
            type: 'GET',
            url: '{{ url('/') }}/api/all/areas',
            dataType: 'json',
            success: function (data) {
                var trHTML = '';
                $(data).each(function(index, value){
                    if(area == value.id) trHTML += "<option value='" + value.id + "' selected>" + value.name + '</option>';
                    else trHTML += "<option value='" + value.id + "'>" + value.name + '</option>';
                });
                $('#cmb_areas').html(trHTML);
                $('#cmb_areas').val(-1);
                if (callback) {
                    callback();
                }
            },
            error: function () {
                $('.message_text').text('Error al cargar la información de las áreas');
                $('.ui.modal.message')
                    .modal({closable: false, observeChanges: true })
                    .modal('show');
            }
        });
    }
    </script>
@endsection
