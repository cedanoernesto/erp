@extends('index')
@section('content')
<div class="ui twelve wide column">
	<div class="padding-30">
		<p class="ui medium header">Infraestructuras</p>
		<table class="ui selectable celled table padded">
		  	<thead>
			    <tr>
			      	<th>Nombre</th>
                    <th>Ubicación</th>
			      	<th width="380" class="center aligned">Acciones</th>
			    </tr>
		  	</thead>
			<tbody id="buildings_table">
			</tbody>
		</table>
        <div class="ui pagination menu">
            <a class="active item prev_all">
                <i class="angle double left icon"></i>
            </a>
            <a class="active item prev">
                <i class="angle left icon"></i>
            </a>
            <a class="active item current">1</a>
            <a class="active item next">
                <i class="angle right icon"></i>
            </a>
            <a class="active item next_all">
                <i class="angle double right icon"></i>
            </a>
        </div>
       <button id="new" class="ui right floated positive button">
            Agregar infraestructura
        </button>
	</div>
</div>
<div class="ui modal small buildings">
	<div class="header">
        <label id="modaltittle">Infraestructura</label>
        <i class="close icon right" style="cursor:pointer;"></i>
    </div>
    <div class="content">
        <form class="ui form" method="post" id="form_buildings">
            <div class="ui one column padded grid">
                <div class="column">
                    <div class="field">
                        <input type="hidden" name="id" id="id_building"/>
                        <label>Nombre:</label>
                        <input type="text" name="name" id="name_building" placeholder="Nombre del Edificio">
                    </div>
                    <div class="field">
                        <label>Ubicación:</label>
                        <input type="text" name="ubication" id="ubication_building" placeholder="Ubicación del Edificio">
                    </div>
                    <div class="fields">
                        <div class="five wide field">
                          <label>Pisos</label>
                          <input type="number" min="1" name="floors" id="floors_building" maxlength="2" placeholder="#">
                        </div>
                        <div class="five wide field">
                          <label>Habitaciones</label>
                          <input type="number" min="1" name="rooms" id="rooms_building" maxlength="2" placeholder="#">
                        </div>
                    </div>
                </div>
                <div class="ui actions right aligned container">
                    <button class="ui primary save_building button">Aceptar</button>
                </div>
            </div>
            <div class="ui error message"></div>
        </form>
    </div>
</div>
<div class="ui modal small rooms">
	<div class="header">
        <label id = "modaltittleroom">Habitación</label>
        <i class="close icon right" style="cursor:pointer;"></i>
    </div>
    <div class="content">
        <form class="ui form" method="post" id="form_rooms">
            <div class="ui one column padded grid">
                <div class="column">
                    <div class="field">
                        <input type="hidden" id="building_id" name="infrastructure_id" placeholder="Id Edificio">
                        <input type="hidden" id="id_room" name="id" placeholder="Id Habitacion">
                    </div>
                    <div class="field">
                        <label>Nombre:</label>
                        <input type="text" name="name" id="name_room" placeholder="Nombre de la Habitación">
                    </div>
                    <div class="fields">
                        <div class="five wide field">
                          <label>Capacidad:</label>
                          <input type="number" min="1" name="capacity" id="capacity_room" maxlength="2" placeholder="#">
                        </div>
                        <div class="five wide field">
                          <label>Ubicación:</label>
                          <input type="text" name="ubication" id="ubication_room" maxlength="45" placeholder="Ubicación">
                        </div>
                    </div>
                    <div class="field">
                        <div class="field">
                          <label>Subárea:</label>
                          <select class="ui search dropdown" id="cmb_subareas" name="subarea_id"></select>
                          <!--<button class="ui button add_subareas"><i class="add icon"></i></button>-->
                        </div>
                    </div>
                    <div class="ui actions right aligned container">
                        <button class="ui primary button save_room">Aceptar</button>
                    </div>
                </div>
            </div>
            <div class="ui error message"></div>
        </form>
    </div>
</div>
<div class="ui modal small areas">
	<div class="header">
        <i class="pie chart icon"></i>
            Áreas
        <i class="close icon right" style="cursor:pointer;"></i>
    </div>
    <div class="content">
        <form class="ui form" method="post" id="form_areas">
            <div class="field">
                <input type="hidden" id="id_area" name="id" placeholder="Id area">
                <label>Nombre:</label>
                <input type="text" name="name" id="name_area" placeholder="Nombre del área">
            </div>
        </form>
    </div>
    <div class="ui actions right aligned container">
        <button class="ui primary button save_areas">Aceptar</button>
    </div>
</div>
<div class="ui modal small subareas">
	<div class="header">
        <i class="location arrow icon"></i>
            Subáreas
        <i class="close icon right" style="cursor:pointer;"></i>
    </div>
    <div class="content">
        <form class="ui form" method="post" id="form_subareas">
            <div class="field">
                <input type="hidden" id="id_subarea" name="id" placeholder="Id Subarea">
                <label>Nombre:</label>
                <input type="text" name="name" id="name_subarea" placeholder="Nombre del subarea">
            </div>
            <div class="field">
                <label>Área:</label>
                <select class="ui search dropdown" id="cmb_areas" name="area_id"></select>
                <button class="ui button add_areas"><i class="add icon"></i></button>
            </div>
        </form>
    </div>
    <div class="ui actions right aligned container">
        <button class="ui primary button save_subareas">Aceptar</button>
    </div>
</div>
<div class="ui modal small list_rooms">
	<div class="header">
            Lista de Habitaciones
        <i class="close icon right" style="cursor:pointer;"></i>
    </div>
    <div class="content" style="overflow-y: auto; max-height: 400px;">
        <div class="ui list scroll" id = "list_rooms">
        </div>
    </div>
    <div class="ui actions right aligned container">
        <button class="ui button add_rooms">Agregar Habitaciones</button>
        <button class="ui primary cancel button">Aceptar</button>
    </div>
</div>
<div class="ui modal small delete">
	<div class="header">
        <!--<i class="cube box icon"></i>-->
        ¿Está seguro que quiere eliminar este registro?
    </div>
    <div class="actions">
        <div class="ui negative button">No</div>
        <div class="ui approve blue button">Sí</i></div>
    </div>
</div>
<div class="ui modal small message">
	<div class="header">
        <i class="cube box icon"></i>
        <label class="message_text"></label>
        <i class="close icon right" style="cursor:pointer;"></i>
    </div>
    <div class="actions">
        <div class="ui positive button">
            Aceptar
            <i class="checkmark icon"></i>
        </div>
    </div>
</div>
<script type="text/javascript">
var idBuilding = 0, roomsCount=0, maxRooms=0;
$(function () {
    fullTable(1);
});

$('#form_buildings').form({
    revalidate: true,
    fields: {
        name: {
            identifier: 'name',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Nombre: Ingrese un nombre para el edificio'
                },
                {
                    type : 'maxLength[45]',
                    prompt : 'Nombre: Máximo 45 caracteres'
                },
				{
                    type : 'regExp[/^[a-zA-ZÀ-ÿ\u00f1\u00d10-9 ]+$/i]', //Nueva expresion regular para los acentos
                    prompt : 'Nombre: No es posible el uso de esos caracteres'
                }
            ]
        },
        ubication: {
            identifier: 'ubication',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Ubicación: Ingrese la ubicación'
                },
                {
                    type : 'maxLength[45]',
                    prompt : 'Ubicación: Máximo 45 caracteres'
                },
				{
                    type : 'regExp[/^[a-zA-ZÀ-ÿ\u00f1\u00d10-9 ]+$/i]', //Nueva expresion regular para los acentos
                    prompt : 'Ubicación: No es posible el uso de esos caracteres'
                }
            ]
        },
        floors: {
            identifier: 'floors',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Pisos: Ingrese el número de pisos'
                }
            ]
        },
        rooms: {
            identifier: 'rooms',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Habitaciones: Ingrese el número de habitaciones'
                }
            ]
        }
    }
});

$('#form_rooms').form({
    revalidate: true,
    fields: {
        name: {
            identifier: 'name',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Nombre: Ingrese un nombre para la habitacion'
                },
				{
                    type : 'maxLength[45]',
                    prompt : 'Nombre: Máximo 45 caracteres'
                },
				{
                    type : 'regExp[/^([a-zA-ZÀ-ÿ\u00f1\u00d10-9 ]+)?$/i]', //Nueva expresion regular para los acentos
                    prompt : 'Nombre: No es posible el uso de esos caracteres'
                }
            ]
        },
        ubication: {
            identifier: 'ubication',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Ubicación: Ingrese la ubicación'
                },
				{
                    type : 'maxLength[45]',
                    prompt : 'Ubicación: Máximo 45 caracteres'
                },
				{
                    type : 'regExp[/^([a-zA-ZÀ-ÿ\u00f1\u00d10-9 ]+)?$/i]', //Nueva expresion regular para los acentos
                    prompt : 'Ubicación: No es posible el uso de esos caracteres'
                }
            ]
        },
        capacity: {
            identifier: 'capacity',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Capacidad: Ingrese la capacidad'
                }
            ]
        },
        cmb_subareas: {
            identifier: 'cmb_subareas',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Seleccione una subarea'
                }
            ]
        }
    }
});

$('.close.icon').on('click',function () {
	/*console.log($(".ui.modal.small.rooms"));
	console.log($(this).parent().parent());*/
	if($(".ui.modal.small.rooms").html() == $(this).parent().parent().html()) {
		fullListRooms(idBuilding);
	}
	$(this).parent().parent().modal('hide');
});
$('.prev_all').on('click',function () {
    fullTable($('.prev_all').data('value'));
});
$('.prev').on('click',function () {
    fullTable($('.prev').data('value'));
});
$('.next').on('click',function () {
    fullTable($('.next').data('value'));
});
$('.next_all').on('click',function () {
    fullTable($('.next_all').data('value'));
});
$('#new').on('click',function () {
    $('#form_buildings').form('clear');
    $('.ui.error.message').hide();
    $("#modaltittle").empty();
	$("#modaltittle").text('Agregar infraestructura');
	$('#id_building').val('0');
    $('.ui.modal.buildings')
        .modal({closable: false, observeChanges: true })
        .modal('show');
});
$('.ui.button.add_rooms').on('click', function () {
	if(maxRooms > roomsCount){
		$('#form_rooms').form('clear');
		$('.ui.error.message').hide();
		$('#building_id').val(idBuilding);
		$('#id_room').val('0');
		$("#modaltittleroom").empty();
		$("#modaltittleroom").text("Agregar habitación");
		$('.ui.modal.rooms')
            .modal({closable: false, observeChanges: true })
            .modal('show');
		fullSubAreas();
	}
	else{
		new PNotify({
				title: 'Error',
				text: 'Ya no se pueden agregar cuartos. Limite alcanzado'
			});
	}
});
$('.ui.button.add_subareas').on('click', function (e) {
    e.preventDefault();
    $('#id_subarea').val('0');
    $('.ui.modal.subareas')
        .modal('setting', 'closable', false)
        .modal({ observeChanges: true })
        .modal('show');
    fullAreas();
});
$('.ui.button.add_areas').on('click', function (e) {
    e.preventDefault();
    $('#id_area').val('0');
    $('.ui.modal.areas')
        .modal('setting', 'closable', false)
        .modal({ observeChanges: true })
        .modal('show');
});
$('.ui.button.save_areas').on('click', function (e) {
    e.preventDefault();
    var id = $('#id_subarea').val();
    if($('#name_area').val() != ""){
        if(id == 0) {
            save($('#form_areas'),'areas');
            $('#name_area').val('');
            $('.ui.modal.subareas')
                .modal('setting', 'closable', false)
                .modal({ observeChanges: true })
                .modal('show');
            fullAreas();
        } else {
            update($('#form_areas'),'areas',id);
            $('#name_area').val('');
            $('.ui.modal.subareas')
                .modal('setting', 'closable', false)
                .modal({ observeChanges: true })
                .modal('show');
            fullAreas();
        }
    }else {
        $('.message_text').text('¡Aviso! Existen campos vacíos.');
        $('.ui.modal.message')
            .modal('setting', 'closable', false)
            .modal({
                observeChanges: true,
                onApprove: function(){
                    $('.ui.modal.areas')
                        .modal('setting', 'closable', false)
                        .modal({ observeChanges: true })
                        .modal('show');
                }
            }).modal('show');
    }
});
$('.ui.button.save_subareas').on('click', function (e) {
    e.preventDefault();
    var id = $('#id_subarea').val();
    if($('#name_subarea').val() != ""){
        if(id == 0) {
            save($('#form_subareas'),'subareas');
            $('#name_subarea').val('');
            $('.ui.modal.rooms')
                .modal('setting', 'closable', false)
                .modal({ observeChanges: true })
                .modal('show');
            fullSubAreas();
        } else {
            update($('#form_subareas'),'subareas',id);
            $('#name_subarea').val('');
            $('.ui.modal.rooms')
                .modal('setting', 'closable', false)
                .modal({ observeChanges: true })
                .modal('show');
            fullSubAreas();
        }
    }else {
        $('.message_text').text('¡Aviso! Existen campos vacíos.');
        $('.ui.modal.message').modal('setting', 'closable', false).modal({
            observeChanges: true,
            onApprove: function(){
            $('.ui.modal.subareas')
                .modal('setting', 'closable', false)
                .modal({ observeChanges: true })
                .modal('show');
        }}).modal('show');
    }
});
$('#form_buildings').submit(function (e) {
    e.preventDefault();
    if ($(this).form('is valid')) {
        var id = $('#id_building').val();
        if (id == 0) {
            save($('#form_buildings'),'infrastructures');
            $('#name_building').val('');
            $('#ubication_building').val('');
            $('#floors_building').val('');
            $('#rooms_building').val('');
        } else {
            update($('#form_buildings'),'infrastructures',id);
            $('#name_building').val('');
            $('#ubication_building').val('');
            $('#floors_building').val('');
            $('#rooms_building').val('');
        }
        $('.ui.modal.buildings').modal('hide');
        fullTable();
    } else {
        $('.ui.error.message').show();
    }
});
$('#form_rooms').submit(function (e) {
    e.preventDefault();
    if ($(this).form('is valid')) {
        var id = $('#id_room').val();
        if (id == 0) {
            save($('#form_rooms'),'rooms');
        } else {
            update($('#form_rooms'),'rooms',id);
        }
        var id = idBuilding;
        fullListRooms(id);
    } else {
        $('.ui.error.message').show();
    }
});

function fullListRooms (id) {
    $.ajax({
        type: 'GET',
        url: '{{ url('/') }}/api/infrastructures/' + id + '/room',
        success: function (data) {
            var itemHTML = '';
			roomsCount=0;
            itemHTML += "<div class='ui three cards'>";
			/*console.log(data);*/
            $(data.room).each(function(index, value){
                itemHTML += "<div class='ui card room' id='" + value.id + "'>";
                itemHTML += "<div class='content'>";
                itemHTML += "<div class='header room'>" + value.name + "<i class='close icon right' id='" + value.id + "' style='cursor:pointer;'></i></div>";
                itemHTML += "<div class='meta'><i class='users icon'></i><a>" + value.capacity + " capacidad</a></div>";
                itemHTML += "<div class='meta'><i class='location arrow icon'></i><a>" + value.ubication + " </a></div>";
                itemHTML += "</div></div>'";
				roomsCount++;
            });
			maxRooms = data.rooms;
            itemHTML += '</div>';
            $('#list_rooms').html(itemHTML);
            $('.header.room .close.icon').on('click',deleteRoom);
            $('.ui.card.room').on('click',getRoom);
            $('.ui.modal.list_rooms')
                .modal({closable: false, observeChanges: true })
                .modal('show');
        },
        error: function () {
            $('.message_text').text('¡Ha ocurrido un error!');
            $('.ui.modal.message')
                .modal({closable: false, observeChanges: true })
                .modal('show');
        }
    });
}
function listRooms () {
    var id = this.id;
    idBuilding = this.id;
    fullListRooms(id);
}
function fullTable (page) {
    $.ajax({
        type: 'GET',
        url: '{{ url('/') }}/api/infrastructures?page=' + page,
        dataType: 'json',
        success: function (data) {
            var trHTML = '';
            $(data.data).each(function(index, value){
                trHTML += "<tr style='cursor:pointer;'>";
                trHTML += "<td class='tr_data' id = '" + value.id + "'>" + value.name + "</td>";
                trHTML += "<td class='tr_data' id = '" + value.id + "'>" + value.ubication + "</td>";
                trHTML += "<td class='center aligned'>";
                trHTML += "<button id='" + value.id + "' class='ui right button modify_building'>";
                trHTML += "Editar";
                trHTML += '</button>';
                trHTML += "<button id='" + value.id + "' class='ui negative right button delete_building'>";
                trHTML += "Eliminar";
                trHTML += '</button>';
                trHTML += "<button id='" + value.id + "' class='ui right button list_rooms'>";
                trHTML += "Ver Habitaciones";
                trHTML += '</button>';
                trHTML += '</td> </tr>';
            });
            $('#buildings_table').html(trHTML);
            $('.button.list_rooms').on('click',listRooms);
            $('.modify_building').on('click',getBuilding);
            $('.button.delete_building').on('click',deleteBuilding);
            $('.prev_all').data('value',1);
            $('.prev').data('value',(((data.current_page-1) > 0)?(data.current_page-1):1));
            $('.current').text(data.current_page);
            $('.next').data('value',((data.current_page+1) > data.last_page)?data.last_page:(data.current_page+1))
            $('.next_all').data('value',data.last_page);

            managePages(page, data);
        },
        error: function () {
            $('.message_text').text('¡Ha ocurrido un error!');
            $('.ui.modal.message')
                .modal('setting', 'closable', false)
                .modal({ observeChanges: true })
                .modal('show');
        }
    });
}
function fullAreas () {
    $.ajax({
        type: 'GET',
        url: '{{ url('/') }}/api/areas',
        dataType: 'json',
        success: function (data) {
            var trHTML = '';
            $(data.data).each(function(index, value){
                trHTML += "<option value='" + value.id + "'>" + value.name + '</option>';
            });
            $('#cmb_areas').html(trHTML);
        },
        error: function () {
            $('.message_text').text('¡Ha ocurrido un error!');
            $('.ui.modal.message')
                .modal('setting', 'closable', false)
                .modal({ observeChanges: true })
                .modal('show');
        }
    });
}
function fullSubAreas () {
    $.ajax({
        type: 'GET',
        url: '{{ url('/') }}/api/subareas/all',
        dataType: 'json',
        success: function (data) {
            var trHTML = '';
            $(data).each(function(index, value){
                trHTML += "<option value='" + value.id + "'>" + value.name + '</option>';
            });
            $('#cmb_subareas').html(trHTML);
			$('#cmb_subareas').val(-1);
        },
        error: function () {
            $('.message_text').text('¡Ha ocurrido un error!');
            $('.ui.modal.message')
                .modal('setting', 'closable', false)
                .modal({ observeChanges: true })
                .modal('show');
        }
    });
}
function save (frm, apiText) {
    $.ajax({
        type: 'POST',
        url: '{{ url('/') }}/api/' + apiText,
        data: frm.serialize(),
        dataType: 'json',
        success: function (data) {
            //$('.message_text').text('¡Se ha guardado con éxito!');
            //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
            new PNotify({
                title: 'Operación exitosa',
                text: 'Registro agregado exitosamente',
                type: 'success'
            });

            $('.ui.modal.buildings').modal('hide');
            $('.ui.modal.rooms').modal('hide');
        },
		error: function (response) {
            if(response.responseJSON.message !== undefined){
                new PNotify({
                    title: 'Operación fallida',
                    text: response.responseJSON.message+'.',
                    type: 'error'
                });
                //$('.message_text').text(response.responseJSON.message+".");
				//$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
            }	else{
                new PNotify({
                    title: 'Operación fallida',
                    text: 'Ha ocurrido un error al agregar el registro.',
                    type: 'error'
                });
                //$('.message_text').text('Ha ocurrido un error al agregar el registro.');
				//$('.ui.modal.message').modal('setting', 'closable', false).modal('show')
            }
		}
    });
}
function update (frm, apiText, id) {
    $.ajax({
        type: 'PUT',
        url: '{{ url('/') }}/api/' + apiText + '/' + id,
        data: frm.serialize(),
        dataType: 'json',
        success: function (data) {
            //$('.message_text').text('¡Se ha guardado con éxito!');
            //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
            new PNotify({
                title: 'Operación exitosa',
                text: 'Registro modificado exitosamente',
                type: 'success'
            });

            $('.ui.modal.buildings').modal('hide');
        },
        error: function (response) {
            if(response.responseJSON.message !== undefined){
                new PNotify({
                    title: 'Operación fallida',
                    text: response.responseJSON.message+'.',
                    type: 'error'
                });
                //$('.message_text').text(response.responseJSON.message+".");
				//$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
            }	else{
                new PNotify({
                    title: 'Operación fallida',
                    text: 'Ha ocurrido un error al actualizar el registro.',
                    type: 'error'
                });
                //$('.message_text').text('Ha ocurrido un error al agregar el registro.');
				//$('.ui.modal.message').modal('setting', 'closable', false).modal('show')
            }
		}
    });
}
function getBuilding () {
    var id = this.id;
    $.ajax({
        type: 'GET',
        url: '{{ url('/') }}/api/infrastructures/' + id,
        dataType: 'json',
        success: function (data) {
            $('#form_buildings').form('clear');

            $('#id_building').val(data.id);
            $('#name_building').val(data.name);
            $('#ubication_building').val(data.ubication);
            $('#floors_building').val(data.floors);
            $('#rooms_building').val(data.rooms);
			$("#modaltittle").empty();
			$("#modaltittle").text('Editar infraestructura');
            $('.ui.modal.buildings')
                .modal({closable: false, observeChanges: true })
                .modal('show');

            $('.ui.error.message').hide();
        },
        error: function () {
            $('.message_text').text('¡Ha ocurrido un error!');
            $('.ui.modal.message')
                .modal({closable: false, observeChanges: true })
                .modal('show');
        }
    });
}
function deleteBuilding () {
    var id = this.id;
    $('.ui.modal.delete').modal({
        observeChanges: true,
        onApprove: function(){
            $.ajax({
                type: 'DELETE',
                url: '{{ url('/') }}/api/infrastructures/' + id,
                success: function () {
                    fullTable();
                    //$('.message_text').text('¡Se ha eliminado con éxito!');
                    //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                    new PNotify({
                        title: 'Operación exitosa',
                        text: 'Edificio eliminado exitosamente',
                        type: 'success'
                    });
                 },
                 error: function () {
                    $('.message_text').text('¡Ha ocurrido un error!');
                    $('.ui.modal.message')
                        .modal({closable: false, observeChanges: true })
                        .modal('show');
                 }
            });
        }
    }).modal('setting', 'closable', false).modal('show');
}
function getRoom () {
    var id = this.id;
    fullSubAreas();
    $.ajax({
        type: 'GET',
        url: '{{ url('/') }}/api/rooms/' + id,
        dataType: 'json',
        success: function (data) {
            $('#form_rooms').form('clear');
            $('.ui.error.message').hide();

            $('#id_room').val(data.id);
            $('#name_room').val(data.name);
            $('#ubication_room').val(data.ubication);
            $('#capacity_room').val(data.capacity);
			$("#modaltittleroom").empty();
			$("#modaltittleroom").text("Editar habitacion");
            $('#building_id').val(data.infrastructure_id);
            $('#cmb_subareas').dropdown('set selected', data.subarea_id);
            $('.ui.modal.rooms')
                .modal({closable: false, observeChanges: true })
                .modal('show');
        },
        error: function () {
            $('.message_text').text('¡Ha ocurrido un error!');
            $('.ui.modal.message')
                .modal({closable: false, observeChanges: true })
                .modal('show');
        }
    });
}
function deleteRoom (e) {
    e.stopPropagation();
    var id = this.id;
    $('.ui.modal.delete').modal({
        observeChanges: true,
        onApprove: function(){
            $.ajax({
                type: 'DELETE',
                url: '{{ url('/') }}/api/rooms/' + id,
                success: function () {
                    var id = idBuilding;
                    fullListRooms(id);
                    //$('.message_text').text('¡Se ha eliminado con éxito!');
                    //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                    new PNotify({
                        title: 'Operación exitosa',
                        text: 'Habitacion eliminada exitosamente',
                        type: 'success'
                    });
                 },
                 error: function () {
                    $('.message_text').text('¡Ha ocurrido un error!');
                    $('.ui.modal.message')
                        .modal({closable: false, observeChanges: true})
                        .modal('show');
                 }
            });
        }
    }).modal('setting', 'closable', false).modal('show');
}
</script>
@endsection
