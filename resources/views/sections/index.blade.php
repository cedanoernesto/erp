@extends('index')
@section('content')

    <div class="ui twelve wide column">
        <div class="padding-30">
			<p class="ui medium header">Secciones</p>
            <table class="ui selectable celled table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Ícono</th>
                        <th width="250">Acciones</th>
                    </tr>
                </thead>
                <tbody id="sections_table">
                </tbody>
            </table>
            <div class="ui pagination menu">
                <a class="active item prev_all">
                    <i class="angle double left icon"></i>
                </a>
                <a class="active item prev">
                    <i class="angle left icon"></i>
                </a>
                <a class="active item current">1</a>
                <a class="active item next">
                    <i class="angle right icon"></i>
                </a>
                <a class="active item next_all">
                    <i class="angle double right icon"></i>
                </a>
            </div>

            <button class="ui right floated blue right positive button section_new">
                Agregar sección
            </button>
        </div>
        <!--
        Modals de secciones
    -->

    <div id="modals" class="ui dimmer modals page transition">

        <!--
        Modal para eliminar sección
    -->
    <div id="modal_delete" class="ui small first coupled modal">
        <div class="header">¿Está seguro que quiere eliminar este registro?</div>
        <div class="actions">
            <div class="ui negative button">No</div>
            <div id="yes" class="ui approve blue button">Sí</i></div>
        </div>
    </div>
    <div id="modal_delete_success" class="ui small second coupled modal">
        <div class="header">¡Registro eliminado con éxito!</div>
    </div>

    <!--
    Modal para agregar sección
-->
<div id="modal_new" class="ui small modal">
    <div class="header">
        Agregar Sección
        <i class="close icon right" style="cursor:pointer;"></i>
    </div>
    <div class="content">
        <form id="form_new_section" class="ui form form_new">
            <div class="ui one column padded grid">
                <div class="column">
                    <div class="ui center aligned segment">
                        <label for="icon">
                            <i class="ui bordered inverted black massive photo icon placeholder"></i>
                            <img src="" alt="" class="img_preview" style="display: none;">
                        </label>
                        <div class="field">
                            <input type="file" id="icon" accept="image/*"  name="icon" value="">
                        </div>
                    </div>
                    <div class="field">
                        <label>Nombre:</label> <input type="text" name="name" />
                    </div>
                    <div class="ui right aligned container field">
                        <button id="btn_new_section" class="ui blue right button">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="ui segment error message"></div>
        </form>
    </div>
</div>
<div id="modal_new_success" class="ui small second coupled modal">
    <div class="header">¡Registro agregado con éxito!</div>
</div>

<!--
Modal para modificar sección
-->
<div id="modal_modify" class="ui small modal">
    <div class="header">
        Editar Sección
        <i class="close icon right" style="cursor:pointer;"></i>
    </div>
    <div class="content">
        <form id="form_modify_section" class="ui form form_modify">
            <span id="id_to_update" style="display: none"></span>
            <div class="ui one column padded grid">
                <div class="column">
                    <div class="ui center aligned segment">
                        <label for="icon_m">
                            <i class="ui bordered inverted black massive photo icon placeholder"></i>
                            <img src="" alt="" class="img_preview" style="display: none;">
                        </label>
                        <input type="file" id="icon_m" accept="image/*" name="icon" value="">
                    </div>
                    <div class="field">
                        <label for="name">Nombre:</label>
                        <input type="text" name="name" placeholder="Nombre">
                    </div>
                    <div class="ui right aligned container field">
                        <button id="btn_modify_section" class="ui blue button">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="ui error message"></div>
        </form>
    </div>
</div>
<div id="modal_modify_success" class="ui small second coupled modal">
    <div class="header">¡Registro modificado con éxito!</div>
</div>
</div>
</div>

<script type="text/javascript">
$(function() {
    $('#form_modify_section').submit(function(e){
        e.preventDefault();
        if($('#form_modify_section').form('is valid')){
            $("#btn_modify_section").prop('disabled', true);
            var form_mod = new FormData($("#form_modify_section")[0]);
            form_mod.append('_method', 'put');
            $.ajax({
                url: '/api/sections/'+ $('#id_to_update').text(),
                type: 'POST',
                dataType: 'json',
                data: form_mod,
                processData: false,
                contentType: false,
                success: function(result){
                    new PNotify({
                        title: 'Operación exitosa',
                        text: 'Sección modificada exitosamente',
                        type: 'success'
                    });
                    $("#btn_modify_section").prop('disabled', false);

                    $('#modal_modify').modal('hide');
                    //$('#modal_modify_success').modal('show');
                    fullTable($('.current').text());
                },
                error: function(er){
                    console.log(er);
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'Ha ocurrido un error inesperado',
                        type: 'error'
                    });
                    $("#btn_modify_section").prop('disabled', false);
                    // console.log(er);
                    // alert('error put section');
                }
            });
        } else {
            $('.ui.error.message').show();
        }
    });
    $('#form_new_section').submit(function(e){
        e.preventDefault();
        if($('#form_new_section').form('is valid')){
            $("#btn_new_section").prop('disabled', true);
            var form = new FormData($("#form_new_section")[0]);
            // console.log(form);
            $.ajax({
                url: '/api/sections',
                method: "POST",
                dataType: 'json',
                data: form,
                processData: false,
                contentType: false,
                success: function(result){
                    //$('#modal_new_success').modal('show');
                    new PNotify({
                        title: 'Operación exitosa',
                        text: 'Sección agregada exitosamente',
                        type: 'success'
                    });
                    $("#btn_new_section").prop('disabled', false);
                    $('#modal_new').modal('hide');
                    fullTable($('.current').text());
                },
                error: function(er){
                    new PNotify({
                        title: 'Operación fallida',
                        text: 'Ha ocurrido un error inesperado',
                        type: 'error'
                    });
                    $("#btn_new_section").prop('disabled', false);
                    // console.log(er);
                    // alert('error post sections');
                }
            });
        } else {
            $('.ui.error.message').show();
        }
    });

    $('.prev_all').on('click',function () {
        fullTable($('.prev_all').data('value'));
    });
    $('.prev').on('click',function () {
        fullTable($('.prev').data('value'));
    });
    $('.next').on('click',function () {
        fullTable($('.next').data('value'));
    });
    $('.next_all').on('click',function () {
        fullTable($('.next_all').data('value'));
    });

    function fullTable(page) {
        jQuery.ajax({
            type: 'GET',
            url: '/api/sections?page=' + page,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data, status, jqXHR) {
                $('.prev_all').data('value',1);
                $('.prev').data('value',(((data.current_page-1) > 0)?(data.current_page-1):1));
                $('.current').text(data.current_page);
                $('.next').data('value',((data.current_page+1) > data.last_page)?data.last_page:(data.current_page+1))
                $('.next_all').data('value',data.last_page);

                managePages(page, data);

                var trHTML = '';
                $(data.data).each(function(index, value){
                    // console.log(value);
                    trHTML += '<tr>';
                    trHTML += '<td>' + value.name + '</td>';
                    var img = 'null';
                    if(value.icon){
                        img = value.icon;
                    }
                    trHTML += '<td class=""><img src="'+ img +'" alt="No disponible" class="ui tiny rounded image">'+'</td>';
                    trHTML += '<td class="center aligned">';
                    trHTML += '<button id="' + value.id + '" class="ui '+ (value.editable ? '' : 'disabled') + ' negative button section_delete" '+ (value.editable ? '' : 'data-tooltip="No se puede eliminar esta sección"') + '>';
                    trHTML += 'Eliminar';
                    trHTML += '</button>';
                    trHTML += '<button id="' + value.id + '" class="ui button section_modify">';
                    trHTML += 'Editar';
                    trHTML += '</button>';
                    trHTML += '</td> </tr>';
                });
                $('#sections_table').html(trHTML);
                $('.section_delete').on('click',section_delete_function);
                $('.section_modify').on('click',function(){
                    $('.ui.error.message').hide();
                    var section_to_update = this.id;
                    $('#id_to_update').text(this.id);
                    jQuery.ajax({
                        type: 'GET',
                        url: '/api/sections/'+section_to_update,
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function(data, status, jqXHR){
                            if (data.icon) {
                                $('#form_modify_section .img_preview').attr('src', data.icon);

                                $('#form_modify_section .img_preview').show();
                                $('#form_modify_section .placeholder').hide();
                            }

                            $('.form_modify input[name=name]').val(data.name);
                            $('#modal_modify')
                                .modal({closable: false, observeChanges: true })
                                .modal('show');
                        },
                        error: function(){
                            // alert('error get 1 user');
                        }
                    });
                });
            },
            error: function (jqXHR, status) {
                new PNotify({
                    title: 'Operación fallida',
                    text: 'Ha ocurrido un error inesperado',
                    type: 'error'
                });
                //  console.log(jqXHR);
                //  alert("Error get sections");
            }
        });
    }

    fullTable(1);
    // Validación formulario New Section
    $('#form_new_section').form({
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type : 'regExp[/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜñÑ\\s]+$/i]',
                        prompt : 'Nombre: No es posible el uso de esos caracteres'
                    },
                    {
                        type : 'minLength[3]',
                        prompt : 'Nombre: Al menos 3 caracteres'
                    },
                    {
                        type : 'maxLength[25]',
                        prompt : 'Nombre: Máximo 25 caracteres'
                    }
                ]
            }
            // ,icon:{
            // 	identifier: 'icon',
            // 	optional: true,
            // 	rules: [
            // 		{
            // 			type : 'regExp[%[gif|jpe?g|png]$%i]',
            // 			prompt: 'Solo se aceptan formato .gif | .jpg | .png'
            // 		}
            // 	]
            // }
        }
    });

    $('#form_new_section #icon').on('change', function (e) {
        if (this.files && this.files[0] && this.files[0].type.includes('image')) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#form_new_section .img_preview').attr('src', e.target.result);
                $('#form_new_section .img_preview').show();
                $('#form_new_section .placeholder').hide();
            }
            reader.readAsDataURL(this.files[0]);
        } else {
            this.value = null
            $('#form_new_section .img_preview').attr('src', '');
            $('#form_new_section .img_preview').hide();
            $('#form_new_section .placeholder').show();
        }
    });
    // Validación Modify Section
    $('#form_modify_section').form({
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type : 'regExp[/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜñÑ\\s]+$/i]',
                        prompt : 'Nombre: No es posible el uso de esos caracteres'
                    },
                    {
                        type : 'minLength[3]',
                        prompt : 'Nombre: Al menos 3 caracteres'
                    },
                    {
                        type : 'maxLength[25]',
                        prompt : 'Nombre: Máximo 25 caracteres'
                    }
                ]
            }
            // ,icon: {
            // 	identifier: 'icon',
            // 	optional: true,
            // 	rules: [
            // 		{
            // 			type : 'regExp[%\.(gif|jpe?g|png)$%i]',
            // 			prompt: 'Solo se aceptan formato .gif | .jpg | .png'
            // 		}
            // 	]
            // }
        }
    });

    $('#form_modify_section #icon_m').on('change', function (e) {
        if (this.files && this.files[0] && this.files[0].type.includes('image')) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#form_modify_section .img_preview').attr('src', e.target.result);
                $('#form_modify_section .img_preview').show();
                $('#form_modify_section .placeholder').hide();
            }
            reader.readAsDataURL(this.files[0]);
        } else {
            this.value = null
            // $('#form_modify_section .img_preview').attr('src', '');
            // $('#form_modify_section .img_preview').hide();
            // $('#form_modify_section .placeholder').show();
        }
    });

    $('#form_modify_section .field input:not(input[type=file])').on('change', function (e) {
        $('.ui.error.message').hide();
        $('#form_modify_section').form('validate field', this.name)
    });

    $('#form_new_section .field input:not(input[type=file])').on('change', function (e) {
        $('.ui.error.message').hide();
        $('#form_new_section').form('validate field', this.name)
    });

    $('#modals').modal({allowMultiple: false});
    function section_delete_function(){
        var section_to_delete = this.id;
        $('#modal_delete').modal({
            closable: false,
            observeChanges: true,
            onApprove: function(){
                jQuery.ajax({
                    type: 'DELETE',
                    url: '/api/sections/'+ section_to_delete +'',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data, status, jqXHR) {
                        //$('#modal_delete_success').modal('show');
                        new PNotify({
                            title: 'Operación exitosa',
                            text: 'Sección eliminada exitosamente',
                            type: 'success'
                        });

                        fullTable($('.current').text());
                    },
                    error: function (error) {
                        if(error.responseJSON.errors !== undefined)
						{
							new PNotify({
								title: 'Operación fallida',
								text: error.responseJSON.errors+'.'
							});
						}
						else
						{
							new PNotify({
								title: 'Operación fallida',
								text: 'Ha ocurrido un error inesperado',
								type: 'error'
							});
						}
                        // console.log(jqXHR);
                        // alert("Error delete sections");
                    }
                });
            }
        })
        .modal('show');
    }
    $('.section_new').on('click',function(){
        $('.ui.error.message').hide();
        $('#form_new_section .img_preview').attr('src', '');
        $('#form_new_section .img_preview').hide();
        $('#form_new_section .placeholder').show();
        $('#form_new_section').form('clear');
        $('#modal_new')
            .modal({closable: false, observeChanges: true })
            .modal('show');
    });
    $('.close.icon').on('click',function () {
        $(this).parent().parent().modal('hide');
    });
});
</script>
@endsection
