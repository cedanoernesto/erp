<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Aprendoo</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
        <link rel="manifest" href="/img/manifest.json">
        <link rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">

        <link rel="stylesheet" href="{{ URL::asset('css/master.css') }}">
    	<link rel="stylesheet" href="{{ URL::asset('css/semantic.min.css') }}">
    </head>
    <style media="screen">
        /*body {
            background-color: #198CCF;
        }*/

        .logo {
            width: 30%;
            float: left;
        }

        .container {
            max-width: 800px;
            overflow: hidden;
        }

        .big-text {
            font-size: 5em;
        }

        .medium-text {
            font-size: 3em;
        }

        .center {
            text-align: center;
        }

        .dark-blue {
            color: #394a56
        }

        .aligner {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .item {
            width: 100%;
        }

    </style>
    <body class="aligner">
        <div class="container aligner">
            <img class="logo" src="/img/logo-small-text.png" alt="">
            <div class="center dark-blue">
                <!--<h1 class="big-text item">404</h1>-->
                <h2 class="medium-text item">La URL a la que intenta acceder no esta disponible</h2>
            </div>
        </div>
    </body>
</html>
