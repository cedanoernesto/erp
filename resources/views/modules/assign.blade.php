@extends('index')
@section('content')}
<style media="screen">
    .img-module {
        padding: 1em !important;
        max-height: 150px;
    }
</style>
<div class="ui twelve wide column">
	<div class="padding-30">
		<p class="ui medium header">Asignar módulos</p>
		<table class="ui selectable table celled padded">
		  	<thead>
			    <tr>
			      	<th>Nombre</th>
                    <th>Descripción</th>
			      	<th width="200" class="center aligned">Acciones</th>
			    </tr>
		  	</thead>
			<tbody id="assign_table">
			</tbody>
		</table>
        <div class="ui pagination menu">
            <a class="active item prev_all">
                <i class="angle double left icon"></i>
            </a>
            <a class="active item prev">
                <i class="angle left icon"></i>
            </a>
            <a class="active item current">1</a>
            <a class="active item next">
                <i class="angle right icon"></i>
            </a>
            <a class="active item next_all">
                <i class="angle double right icon"></i>
            </a>
        </div>
	</div>
</div>
<div id="modals">
    <div class="ui modal assign">
    	<div class="header">
            <!--<i class="cubes icon"></i>-->
                Asignar Módulos
            <i class="close icon right" style="cursor:pointer;"></i>
        </div>
        <div class="content" style="overflow-y: auto; max-height: 400px;">
            <div class="ui list scroll" id = "list_modules">
            </div>
        </div>
        <div class="ui actions right aligned container">
            <button class="ui primary cancel button">Aceptar</button>
        </div>
    </div>
    <div class="ui modal assign_modules">
    	<div class="header">
                Módulos
        </div>
        <div class="content" style="overflow-y: auto; max-height: 400px;min-height: 390px;">
            <div class="ui list scroll" id = "list_modules_all">

            </div>
        </div>
        <div class="ui actions right aligned container">
            <button class="ui primary approve button">Aceptar</button>
            <button class="ui cancel button">Cancelar</button>
        </div>
    </div>
</div>

<script type="text/javascript">
var idRol = '';
$(function () {
    fullTable(1);
    $('.ui.modal').modal({allowMultiple: true});
    $('#modals').modal({allowMultiple: true});

    $('.close.icon').on('click',function () {
        $(this).parent().parent().modal('hide');
    });
    $('.prev_all').on('click',function () {
        fullTable($('.prev_all').data('value'));
    });
    $('.prev').on('click',function () {
        fullTable($('.prev').data('value'));
    });
    $('.next').on('click',function () {
        fullTable($('.next').data('value'));
    });
    $('.next_all').on('click',function () {
        fullTable($('.next_all').data('value'));
    });

    function sections_modules () {
        var idSection = this.id;
        $.ajax({
            type: 'GET',
            url: '{{ url('/') }}/api/roles/' + idRol + '/modules',
            dataType: 'json',
            success: function (datas) {
                $.ajax({
                    type: 'GET',
                    url: '{{ url('/') }}/api/sections/menuAll',
                    dataType: 'json',
                    success: function (data) {
                        var itemHTML = '';
                        $(data).each(function(index, value){
                            if(value.id == idSection){
                                itemHTML += "<div class='ui horizontal divider'>" + value.name + "</div>";
                                itemHTML += "<div class='ui three cards'>";
                                $(value.modules).each(function(i, v){
                                    itemHTML += "<div class='card'><img id='" + v.id + "' class='ui image module_img img-module' src='" + v.icon + "'>";
                                    itemHTML += "<div class='content'><div class='ui checkbox'>";
                                    itemHTML += "<input type='checkbox' data-id = '" + v.id + "' id='ckm_" + v.id + "' />";
                                    itemHTML += "<label  name='section' for='ckm_" + v.id + "'>" + v.name + "</label>";
                                    itemHTML += '</div></div></div>';
                                });
                                itemHTML += '</div>';
                            }
                        });
                        $('#list_modules_all').html(itemHTML);
                        $('.module_img').on('click',function () {
                           $('#ckm_' + this.id ).prop('checked', true);
                        });
                        var database = {role_id:idRol, module_id:[]};
                        $(datas).each(function(index, value){
                            if(value.section_id == idSection){
                                $('input[type=checkbox]').each(function (i, v) {
                                    if(('ckm_' + value.id) == $(this).prop('id')) {
                                        $(this).prop('checked',true);
                                    }
                                });
                            } else{
                                database.module_id.push(value.id);
                            }
                        });
                        $('.assign_modules')
                            .modal('setting', 'closable', false)
                            .modal('attach events', '.modal.assign .button.more')
                            .modal({
                                allowMultiple: true,
                                onApprove: function () {
                                    $('input[type=checkbox]').each(function (i, v) {
                                        if($(this).prop('checked')) {
                                            database.module_id.push($(this).data('id'));
                                        }
                                    });
                                    $.ajax({
                                        type: 'POST',
                                        url: '{{ url('/') }}/api/roles/' + idRol + '/assign',
                                        data: database,
                                        dataType: 'json',
                                        success: function (data) {
                                            //$('.ui.modal.assign').modal('setting', 'closable', false).modal('show');
                                            new PNotify({
                                                title: 'Operación exitosa',
                                                text: 'Modulos guardados exitosamente',
                                                type: 'success'
                                            });
                                            fullAssignedModules();
                                        }
                                    });
                                },
                                onCancel: function () {
                                    //$('.ui.modal.assign').modal('setting', 'closable', false).modal('show');
                                }
                            })
                            .modal('show');
                    },
                    error: function () {
                        $('.message_text').text('¡Ha ocurrido un error!');
                        $('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                    }
                });
            },
            error: function () {
                $('.message_text').text('¡Ha ocurrido un error!');
                $('.ui.modal.message').modal('setting', 'closable', false).modal('show');
            }
        });
    }

    function assignModule() {
        idRol = this.id;
        var dataSection = '';
        fullAssignedModules(function () {
            $('.ui.modal.assign').modal('setting', 'closable', false).modal('show');
        });

    }

    function fullAssignedModules(callback) {
        $.ajax({
            type: 'GET',
            url: '{{ url('/') }}/api/sections/all',
            dataType: 'json',
            success: function (data) {
                dataSection = data.data;
                $.ajax({
                    type: 'GET',
                    url: '{{ url('/') }}/api/roles/' + idRol + '/modules',
                    dataType: 'json',
                    success: function (datas) {
                        var itemHTML = '';
                        $(data).each(function(index, value){
                            itemHTML += "<div class='content' style='display: inline-block;'><div class='ui horizontal divider'>" + value.name + "</div>";
                            itemHTML += "<button id = '" + value.id + "' class='ui right button more'>Más</button>";
                            itemHTML += "<div class='ui four cards'>";
                            $(datas).each(function(i, v){
                                if(v.section_id == value.id){
                                    itemHTML += "<div class='card'><img class='ui image img-module' src='" + v.icon + "'>";
                                    itemHTML += "<div class='content'><div class='header'>" + v.name + "</div></div>";
                                    itemHTML += '</div>';
                                }
                            });
                            itemHTML += '</div></div>';
                        });
                        $('#list_modules').html(itemHTML);
                        $('.more').on('click',sections_modules);

                        if (callback) {
                            callback();
                        }
                    },
                    error: function () {
                        $('.message_text').text('¡Ha ocurrido un error!');
                        $('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                    }
                });
            },
            error: function () {
                $('.message_text').text('¡Ha ocurrido un error!');
                $('.ui.modal.message').modal('setting', 'closable', false).modal('show');
            }
        });
    }

    function fullTable (page) {
        $.ajax({
            type: 'GET',
            url: '{{ url('/') }}/api/roles/paginate?page=' + page,
            dataType: 'json',
            success: function (data) {
                var trHTML = '';
                $(data.data).each(function(index, value){
                    trHTML += '<tr>';
                    trHTML += '<td>' + value.name + '</td>';
                    trHTML += '<td>' + value.description + '</td>';
                    trHTML += "<td class='center aligned'>";
                    trHTML += "<button id='" + value.id + "' class='ui icon right button assign_module'>";
                    //trHTML += "<i class='cubes icon'></i> Ver Módulos";
                    trHTML += "Ver Módulos";
                    trHTML += '</button>';
                    trHTML += '</td> </tr>';
                });
                $('#assign_table').html(trHTML);
                $('.assign_module').on('click',assignModule);
                $('.prev_all').data('value',1);
                $('.prev').data('value',(((data.current_page-1) > 0)?(data.current_page-1):1));
                $('.current').text(data.current_page);
                $('.next').data('value',((data.current_page+1) > data.last_page)?data.last_page:(data.current_page+1))
                $('.next_all').data('value',data.last_page);

                managePages(page, data);
            },
            error: function () {
                $('.message_text').text('¡Ha ocurrido un error!');
                $('.ui.modal.message').modal('setting', 'closable', false).modal('show');
            }
        });
    }
});
</script>

@endsection
