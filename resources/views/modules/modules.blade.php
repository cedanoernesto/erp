@extends('index')
@section('content')
    <div class="ui twelve wide column">
        <div class="padding-30">
			<p class="ui medium header">Módulos</p>
            <table class="ui selectable celled table padded">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th width="250">Acciones</th>
                    </tr>
                </thead>
                <tbody id="modules_table">
                </tbody>
            </table>
            <div class="ui pagination menu">
                <a class="active item prev_all">
                    <i class="angle double left icon"></i>
                </a>
                <a class="active item prev">
                    <i class="angle left icon"></i>
                </a>
                <a class="active item current">1</a>
                <a class="active item next">
                    <i class="angle right icon"></i>
                </a>
                <a class="active item next_all">
                    <i class="angle double right icon"></i>
                </a>
            </div>
            <button id="new" class="ui right floated positive button new module">
                Agregar módulo
            </button>
        </div>
    </div>
    <div class="ui modal new">
        <div class="header">
            <!--<i class="cube box icon"></i>-->
            <label class="message_text">Módulo</label>
            <i class="close icon right"  style="cursor:pointer;"></i>
        </div>
        <form id="frmModule" class="ui form">
            <div class="ui column padded grid">
                <div class="column">
                    <div class="two fields">
                        <input type="hidden" name="id" id="id"/>
                        <div class="twelve wide field">
                            <label>Nombre del Módulo:</label> <input type="text" name="name" id="name"/>
                        </div>
                        <div class="four wide field">
                            <label>Sección:</label>
                            <div name="section_id" class="ui selection dropdown section_id"  id="section_id">
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label>Descripción del Módulo:</label> <textarea rows="5" name="description" id="description"></textarea>
                    </div>
                    <div class="two fields">
                        <div class="field">
                            <label>URL del Módulo:</label>
                            <div class="ui labeled input">
                                <div class="ui label">
                                    {{URL::to('/')}}/
                                </div>
                                <input type="text" placeholder="modulo" name="url" id="url"/>
                            </div>
                        </div>
                        <div class="field">
                            <label for="icon" class="aline">
                                <i class="ui bordered inverted black large photo icon placeholder"></i>
                                <img src="" alt="" class="img_preview mini" style="display: none;">
                            </label>
                            <input type="file" id="icon" name="icon" accept="image/*" class="aline" style="width: calc(100% - 64px)">
                        </div>
                    </div>
                    <div class="ui column middle aligned content">
                        <div class="ui center aligned container field">
                            <button id="saveModule" class="ui blue approve right button">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ui error message"></div>
        </form>
    </div>
    <div class="ui modal small delete">
        <div class="header">
            <!--<i class="cube box icon"></i>-->
            ¿Está seguro que quiere eliminar este registro?
        </div>
        <div class="actions">
            <div class="ui negative button">No</div>
            <div class="ui approve blue button">Sí</i></div>
        </div>
    </div>
    <div class="ui modal small message">
        <div class="header">
            <!--<i class="cube box icon"></i>-->
            <label class="message_text"></label>
            <i class="close icon right" style="cursor:pointer;"></i>
        </div>
        <div class="actions">
            <div class="ui positive button">
                Aceptar
                <i class="checkmark icon"></i>
            </div>
        </div>
    </div>
    <script type="text/javascript" >
    $(function () {
        fullTable(1);
        var next = null, prev = null;
        $('.prev_all').on('click',function () {
            fullTable($('.prev_all').data('value'));
        });
        $('.prev').on('click',function () {
            if (prev) {
                fullTable($('.prev').data('value'));
            }
        });
        $('.next').on('click',function () {
            if (next) {
                fullTable($('.next').data('value'));
            }
        });
        $('.next_all').on('click',function () {
            fullTable($('.next_all').data('value'));
        });
        $('#new').on('click',function () {
            $('.ui.error.message').hide();
            $('.message_text').text('Agregar Módulo');
            fullComboSections();
            $('.ui.modal.new').modal({
				closable: false,
				observeChanges: true
			}).modal('show');

            $('#frmModule').form('clear');
            $('#frmModule .img_preview').attr('src', '');
            $('#frmModule .img_preview').hide();
            $('#frmModule .placeholder').show();

            setTimeout(function () {
                $('#section_id').dropdown('set selected', $('#section_id').find('.menu .item:first-child').data('value'));
            }, 100);
        });

        $('#frmModule .field textarea').on('change', function (e) {
            $('.ui.error.message').hide();
            $('#frmModule').form('validate field', this.name)
        });

        $('#frmModule .field select').on('change', function (e) {
            $('.ui.error.message').hide();
            $('#frmModule').form('validate field', this.name)
        });

        $('#frmModule .field input:not(input[type=file])').on('change', function (e) {
            $('.ui.error.message').hide();
            $('#frmModule').form('validate field', this.name)
        });

        $('#frmModule').form({
            revalidate: true,
            fields: {
                name: {
                    identifier: 'name',
                    rules: [
                        {
                            type : 'regExp[/^([a-zA-ZÀ-ÿ\u00f1\u00d1 ]+)?$/i]',
                            prompt : 'Nombre: No es posible el uso de esos caracteres'
                        },
                        {
                            type : 'minLength[3]',
                            prompt : 'Nombre: Al menos 3 caracteres'
                        },
                        {
                            type : 'maxLength[191]',
                            prompt : 'Nombre: Máximo 191 caracteres'
                        }
                    ]
                },
                section_id: {
                    identifier: 'section_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Sección: Seleccione una sección'
                        }
                    ]
                },
                description: {
                    identifier: 'description',
                    rules: [
                        {
                            type   : 'minLength[6]',
                            prompt : 'Descripcion: Al menos 6 caracteres'
                        }
                    ]
                },
                url: {
                    identifier: 'url',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Url: Debe proporcionar una url'
                        },
                        {
                            type : 'regExp[^([a-zA-Zñ0-9._\/]{3,16})?$]',
                            prompt : 'Url: No es posible el uso de esos caracteres'
                        }
                    ]
                }
            }
        });

        $('#frmModule').submit(function (e) {
            $("#saveModule").prop('disabled', true);
			e.preventDefault();
            if ($(this).form('is valid')) {
                var frm = $('#frmModule');
                var idModule = $('#id').val();
                if(idModule != 0){
                    var frmData = new FormData($('#frmModule')[0]);
                    frmData.append('section_id', $('#section_id').dropdown('get value'));
                    if (frmData.get('section_id')){
						var file_name = frmData.get('icon').name;
                        frmData.append('_method', 'put');
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('/') }}/api/modules/' + idModule,
                            data: frmData,
                            processData: false,
                            contentType: false,
                            dataType: 'json',
                            success: function (data) {
                                fullTable();
                                new PNotify({
                                    title: 'Operación exitosa',
                                    text: 'Módulo modificado exitosamente',
                                    type: 'success'
                                });

								/*if(file_name != 'jpg' || file_name != 'jpeg' || file_name != 'bmp' || file_name != 'png')
								{
									new PNotify({
										title: 'Archivo incorrecto',
										text: 'El archivo que intenta subir no es una imagen, la actualización de la información se realizo sin el nuevo archivo.'
									});
								}*/

                                $('.ui.modal.new').modal('hide');
								$("#saveModule").prop('disabled', false);

                                //$('.message_text').text('¡Se ha modificado con éxito!');
                                //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                            },
                            error: function (response) {
                                if(response.responseJSON.errors !== undefined){
									var longitud = response.responseJSON.errors.length;
									for(var i = 0; i<longitud; i++)
									{
										new PNotify({
											title: 'Operación fallida',
											text: response.responseJSON.errors[i]+'.',
											type: 'error'
										});
									}

								}	else{
									new PNotify({
										title: 'Operación fallida',
										text: 'Ha ocurrido un error al actualizar el módulo.',
										type: 'error'
									});
								}

								/*$('.message_text').text('¡Ha ocurrido un error!');
                                $('.ui.modal.message')
                                    .modal('setting', 'closable', false)
                                    .modal({ observeChanges: true })
                                    .modal('show');*/
								$("#saveModule").prop('disabled', false);
                            }
                        });
                    } else {

                    }
                }else{
                    var frmData = new FormData($('#frmModule')[0]);
                    frmData.append('section_id', $('#section_id').dropdown('get value'));
                    if (frmData.get('section_id')){
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('/') }}/api/modules',
                            data: frmData,
                            processData: false,
                            contentType: false,
                            dataType: 'json',
                            success: function (data) {
                                fullTable();
                                new PNotify({
                                    title: 'Operación exitosa',
                                    text: 'Módulo agregado exitosamente',
                                    type: 'success'
                                });

                                $('.ui.modal.new').modal('hide');
								$("#saveModule").prop('disabled', false);
                                //$('.message_text').text('¡Se ha guardado con éxito!');
                                //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                            },
                            error: function (response) {
                                //console.log(response);
								if(response.responseJSON.errors !== undefined){
									var longitud = response.responseJSON.errors.length;
									for(var i = 0; i<longitud; i++)
									{
										new PNotify({
											title: 'Operación fallida',
											text: response.responseJSON.errors[i]+'.',
											type: 'error'
										});
									}

								}	else{
									new PNotify({
										title: 'Operación fallida',
										text: 'Ha ocurrido un error al agregar el módulo.',
										type: 'error'
									});
								}
								$("#saveModule").prop('disabled', false);
								/*$('.message_text').text('¡Ha ocurrido un error!');
                                $('.ui.modal.message')
                                    .modal('setting', 'closable', false)
                                    .modal({ observeChanges: true })
                                    .modal('show');
								$("#saveModule").prop('disabled', false);*/
                            }
                        });
                    } else {
						$("#saveModule").prop('disabled', false);
                    }
                }
            } else {
                $('.ui.error.message').show();
				$("#saveModule").prop('disabled', false);
            }
        });

        $('#frmModule #icon').on('change', function (e) {
            if (this.files && this.files[0] && this.files[0].type.includes('image')) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#frmModule .img_preview').attr('src', e.target.result);
                    $('#frmModule .img_preview').show();
                    $('#frmModule .placeholder').hide();
                }
                reader.readAsDataURL(this.files[0]);
            } else {
                this.value = null
                //$('#frmModule .img_preview').attr('src', '');
                //$('#frmModule .img_preview').hide();
                //$('#frmModule .placeholder').show();

                new PNotify({
                    title: 'Archivo no permitido',
                    text: 'El archivo seleccionado debe ser de una imagen',
                    type: 'error'
                });
            }
        });

        $('.close.icon').on('click',function () {
            $(this).parent().parent().modal('hide');
        });
        function deleteModule () {
            var idModule = this.id;
            $('.ui.modal.delete').modal({
                observeChanges: true,
                onApprove: function(){
                    $.ajax({
                        type: 'DELETE',
                        url: '{{ url('/') }}/api/modules/' + idModule,
                        success: function () {
                            fullTable();
                            new PNotify({
                                title: 'Operación exitosa',
                                text: 'Modulo eliminado exitosamente',
                                type: 'success'
                            });

                            //$('.message_text').text('¡Se ha eliminado con éxito!');
                            //$('.ui.modal.message').modal('setting', 'closable', false).modal('show');
                        },
                        error: function () {
                            $('.message_text').text('¡Ha ocurrido un error!');
                            $('.ui.modal.message')
                                .modal('setting', 'closable', false)
                                .modal({ observeChanges: true })
                                .modal('show');
                        }
                    });
                }
            })
            .modal('setting', 'closable', false)
            .modal('show');
        }
        function editModule () {
            $('.ui.error.message').hide();
            $('.message_text').text('Editar Módulo');
            fullComboSections();
            var idModule = this.id;
            $.ajax({
                type: 'GET',
                url: '{{ url('/') }}/api/modules/' + idModule,
                dataType: 'json',
                success: function (data) {
                    $('#frmModule').form('clear');

                    if (data.icon) {
                        $('#frmModule .img_preview').attr('src', data.icon);
                        $('#frmModule .img_preview').show();
                        $('#frmModule .placeholder').hide();
                    } else {
                        $('#frmModule .img_preview').attr('src', '');
                        $('#frmModule .img_preview').hide();
                        $('#frmModule .placeholder').show();
                    }

                    $('#id').val(data.id);
                    $('#name').val(data.name);
                    $('#description').val(data.description);
                    $('#url').val(data.url);
                    $('#section_id').dropdown('set selected', data.section_id);
                    $('.ui.modal.new')
                        .modal({closable: false, observeChanges: true })
                        .modal('show');

                    $('#icon').val('');
                },
                error: function () {
                    $('.message_text').text('¡Ha ocurrido un error!');
                    $('.ui.modal.message')
                        .modal({closable: false, observeChanges: true })
                        .modal('show');
                }
            });
        }
        function fullTable (page) {
            $.ajax({
                type: 'GET',
                url: '{{ url('/') }}/api/modules?page=' + page,
                dataType: 'json',
                success: function (data) {
                    next = data.next_page_url;
                    prev = data.prev_page_url;
                    var trHTML = '';
                    $(data.data).each(function(index, value){
                        trHTML += '<tr>';
                        trHTML += '<td>' + value.name + '</td>';
                        trHTML += '<td>' + value.description + '</td>';
                        trHTML += '<td class="center aligned" style="text-align: right;">';
                        trHTML += '<button id="' + value.id + '" class="ui '+ (value.editable ? '' : 'disabled') + ' negative button module_delete" '+ (value.editable ? '' : 'data-tooltip="No se puede eliminar este usuario"') + '>';
                        trHTML += 'Eliminar';
                        trHTML += '</button>';
                        trHTML += '<button id="' + value.id + '" class="ui button module_edit">';
                        trHTML += 'Editar';
                        trHTML += '</button>';
                        trHTML += '</td> </tr>';
                    });
                    $('#modules_table').html(trHTML);
                    $('.module_delete').on('click',deleteModule);
                    $('.module_edit').on('click',editModule);

                    $('.prev_all').data('value',1);
                    $('.prev').data('value',(((data.current_page-1) > 0)?(data.current_page-1):1));
                    $('.current').text(data.current_page);
                    $('.next').data('value',((data.current_page+1) > data.last_page)?data.last_page:(data.current_page+1))
                    $('.next_all').data('value',data.last_page);

                    managePages(page, data);
                },
                error: function () {
                    $('.message_text').text('¡Ha ocurrido un error!');
                    $('.ui.modal.message')
                        .modal('setting', 'closable', false)
                        .modal({ observeChanges: true })
                        .modal('show');
                }
            });
        }
        function fullComboSections () {
            $.ajax({
                type: 'GET',
                url: '{{ url('/') }}/api/sections/all',
                dataType: 'json',
                success: function (data) {
                    var optionHTML = "<input type='hidden' name='section_id'>";
                    optionHTML += "<i class='dropdown icon'></i>";
                    optionHTML += "<div class='default text'>Sección</div>";
                    optionHTML += "<div class='menu'>";
                    $(data).each(function(index, value){
                        optionHTML += "<div class='item' data-value = '" + value.id + "'>" + value.name + "</div>";
                    });
                    optionHTML += "</div>";
                    $('.section_id').html(optionHTML);
                    $('.ui.dropdown').dropdown();
                },
                error: function () {
                    $('.message_text').text('¡Ha ocurrido un error!');
                    $('.ui.modal.message')
                        .modal('setting', 'closable', false)
                        .modal({ observeChanges: true })
                        .modal('show');
                }
            });
        }

		/*$.fn.form.settings.rules.containsInArray = function(text,csv){
			var array = csv.split(',');
			var isContains = false;

			$.each(array,function(index,elem){
				if($.fn.form.settings.rules.contains(text,$.trim(elem))){
					isContains = true;
					return false;
				}
			});
			console.log(text + " " + isContains);
			return isContains;
		};*/
    })
    </script>
@endsection
