@extends('index')
@section('content')
<style media="screen">
    .body {
        display: flex;
    }
</style>
<div class="ui twelve wide column" style="height: 100%;">
	<div class="ui container" id="module_container">
	<!--All cards here-->
		<div class="ui three cards" id="pictures">

			<!--<div class="ui card">
				<a class="image" href="/profiles">
					<img src="{{ URL::asset('img/placeholder.gif') }}">
				</a>
				<div class="ui center aligned content">
					<a class="header">Perfiles</a>
				</div>
			</div>-->
		</div>

	</div>
	<!--//All cards here-->
</div>

<script type="text/javascript">
	$('.ui.accordion')
  		.accordion()
	;
</script>
<script type="text/javascript" src="{{ URL::asset('js/index.js') }}"></script>

@endsection
