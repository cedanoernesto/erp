@extends('template')

@section('body')
    <link rel="stylesheet" href="/css/login.css">
    <div style="max-width: 500px; padding: 5% 0 0 5%;">
        <div class="ui one cards" style="width:100%;">
            <div class="">
                <img src="/img/logo-white.png" alt="" style="width: 100%; margin: 2.5em 0;">
            </div>
            <div class="ui  card" style="padding: 1em; border-radius: 0">
                <div class="content">
                    <form class="ui form">
                        <div class="fluid ui left icon input field" style="margin-bottom: 1em;">
                            <input type="text" name="email" placeholder="Email">
                            <i class="user icon"></i>
                        </div>
                        <div class="fluid ui left icon input field" style="margin-bottom: 1em;">
                            <input type="password" name="password" placeholder="Contraseña">
                            <i class="lock icon"></i>
                        </div>
                        <div class="ui error message"></div>
                        <button class="fluid ui teal button" type="submit">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="ui small second coupled modal transition incorrect">
        <i class="close icon"></i>
        <div class="header">
            Login fallido
        </div>
        <div class="content">
            <div class="description">
                Usuario y/o contraseña incorrectos
            </div>
        </div>
    </div>
    <div class="ui small second coupled modal transition failed">
        <i class="close icon"></i>
        <div class="header">
            Login fallido
        </div>
        <div class="content">
            <div class="description">
                Ocurrio un error con el servidor
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/js/modules/login.js"></script>
@endsection
