@extends('template')

@section('body')
    <script type="text/javascript">
    $(function () {
        PNotify.prototype.options.delay ? (function() {
            PNotify.prototype.options.delay -= 6000;
        }()) : (console.log());

        $('#button_logout').on('click', function () {
            localStorage.removeItem('token');
            window.location = '/login';
        })

        $.ajax({
            url: '/api/me',
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem('token')
            },
            success: function (response) {
                localStorage.setItem('id', response.id);
                $('#container_menu').css('display', '');
                $('#main_loading').css('display', 'none');
                $('#user_name').text(response.user);

                if (response.image) {
                    $('#user_photo').attr('src', '/storage/' + response.image.url_image)
                    $('#user_photo').show();
                    $('#user_icon_menu').hide();
                }
            },
            error: function (response) {
                window.location = '/login';
            }
        });

        $.ajax({
            url: '/api/sections/menu',
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem('token')
            },
            success: function (response) {
                response.forEach(function (section) {
                    var modules = '';

                    section.modules.forEach(function (module) {
                        modules += '<a class="item" href="/' + module.url + '">' + module.name + '</a>';
                    });

                    var section = '<div class="ui dropdown item">' +
                    '    ' + section.name + ' <i class="dropdown icon"></i>' +
                    '   <div class="menu">' +
                            modules +
                    '    </div>' +
                    '</div>';

                    $('#menu_sections').append(section);
                    $('.ui.dropdown').dropdown();
                });
            },
            error: function (response) {

            }
        });

        window.managePages = function (page, data) {
            if (data.prev_page_url) {
                $('.prev').show();
            } else {
                $('.prev').hide();
            }

            if (page > 2) {
                $('.prev_all').show();
            } else {
                $('.prev_all').hide();
            }

            if (data.next_page_url) {
                $('.next').show();
            } else {
                $('.next').hide();
            }

            if (data.last_page && page < data.last_page && data.last_page !== (page + 1)) {
                $('.next_all').show();
            } else {
                $('.next_all').hide();
            }

            if (data.last_page === 1 || data.last_page === 0) {
                $('.ui.pagination.menu').hide();
            } else {
                $('.ui.pagination.menu').show();
            }
        }
    });
    </script>
    <div class="ui" id="main_loading">
        <div class="ui active inverted dimmer">
            <div class="ui text loader">Cargando</div>
        </div>
    </div>
    <div style="display: none" id="container_menu">
        <div class="column top-bar">
            <img src="/img/logo-dark.png" alt="" style="width: 200px; float: left;" ondragstart="return false">
            <div class="ui menu" style="margin-top: 16px;">
                <div id="menu_sections" class="menu">
                    <a href="/" class="item">Inicio</a>
                </div>
                <div class="right menu">
                    <div class="item">
                        <div>
                            <i class="inverted circular user icon" id="user_icon_menu"></i>
                            <img id="user_photo" src="" alt="" style="display: none;" class="img_profile">
                        </div>
                        <div id="user_name" style="margin-left: 8px;"></div>
                    </div>
                    <a id="button_logout" class="item">Salir</a>
                </div>
            </div>
        </div>
        <div class="ui grid" id="main_grid">
            @yield('content')
        </div>
@endsection
