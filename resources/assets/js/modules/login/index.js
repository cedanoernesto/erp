$(function () {
    $('.ui.modal')
        .modal();

    $('.ui.form')
        .form({
            fields: {
                email    : 'empty',
                password : 'empty'
            }
        })
        .on('submit', function (ev) {
            ev.preventDefault();
            var values = $(this).form('get values');
            if (!!values.email && !!values.password) {
                $.ajax({
                    url: '/api/authenticate',
                    method: 'POST',
                    dataType: 'json',
                    data: values,
                    success: function (response) {
                        window.localStorage.setItem('token', response.token);
                        window.location = '/';
                    },
                    error: function (response) {
                        if (response.statusCode().status == 401) {
                            $('.ui.modal.incorrect').modal('show');
                        } else {
                            $('.ui.modal.failed').modal('show');
                        }
                    }
                })
            }
        });

});
