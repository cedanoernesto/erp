var fourd_cards = document.getElementById("pictures");
var flag = 0;

function showing(){

    $.ajax({
        url: '/api/sections/menu',
        method: 'get',
        headers: {
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        },
        success: function (response) {
            response.forEach(function (section) {
                section.modules.filter(function (module) {
                    return [1, 4, 5, 7, 10, 9].includes(module.id);
                }).forEach(function (module) {
                    var module = '<div class="ui card blue-card">' +
                    '    <a class="image" href="/' + module.url + '">' +
                    '        <img src="' + module.icon + '">' +
                    '    </a>' +
                    '    <div class="ui center aligned content">' +
                    '        <a class="header">' + module.name + '</a>' +
                    '    </div>' +
                    '</div>';

                    $('#pictures').append(module);
                });
            });
        },
        error: function (response) {

        }
    });
}

$(window).on('load',function(){
    showing();
});
