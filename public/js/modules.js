var table = document.getElementById('modules');
var txtName = document.getElementById('name');
var txtDescription = document.getElementById('description');
var txtUrl = document.getElementById('url');
var fileIcon = document.getElementById('icon');
var selSection = document.getElementById('section_id');

var flag = 0;

//Prueba paginacion
var pagination = document.getElementById('pag');
var pagIndex = 0

getModules();

function cleanTable () {
	while(table.childElementCount > 0) {
		table.removeChild(table.firstChild);
	}
	while(pagination.childElementCount > 0) {
		pagination.removeChild(pagination.firstChild);
	}
};

function show (data) {
	//Paginacion
	var pagination_size = 8;
	var startPag = pagIndex*pagination_size;
	var endPag = startPag+pagination_size;

	var array_size = data.length;
	var cont_paginator = array_size/pagination_size;
	var cont_paginator_h = array_size%pagination_size;

	if (cont_paginator_h == 1){
		cont_paginator = cont_paginator+.5;
	}

	for ( var c = startPag; c < endPag; c++ ){
		if ( c == array_size){
			break;
		}

		var tr = document.createElement('tr');

	//Nombre
		var td = document.createElement('td');
		td.textContent = data[c].name;
		td.data = data[c];
	//Seccion
		var td_2 = document.createElement('td');
		td_2.setAttribute('class', 'loadSection'+data[c].section_id)
		td_2.textContent = data[c].section_id;
		td_2.data = data[c];
	//Descripcion
		var td_3 = document.createElement('td');
		td_3.textContent = data[c].description;
		td_3.data = data[c];
	//URL
		var td_4 = document.createElement('td');
		td_4.textContent = "../"+data[c].url;
		td_4.data = data[c];
	//Botones
		var td_5=document.createElement('td');
		td_5.setAttribute('class', 'center aligned');
		//Eliminar
			var btn_del=document.createElement('button');
			btn_del.setAttribute('id', 'del');
			btn_del.setAttribute('class', 'compact ui negative button');
			btn_del.textContent=('Eliminar');
			btn_del.data = data[c];
			btn_del.addEventListener('click', del);
			//var i = document.createElement('i');
			//i.setAttribute('class', 'ban icon');
			//btn_del.prepend(i);
		//Modificar
			var btn_update=document.createElement('button');
			btn_update.setAttribute('id', 'upd');
			btn_update.setAttribute('class', 'compact ui icon button');
			btn_update.textContent=('Modificar');
			btn_update.data = data[c];
			btn_update.addEventListener('click', edit);
			var i = document.createElement('i');
			i.setAttribute('class', 'edit icon');
			btn_update.prepend(i);

		td_5.appendChild(btn_del);
		td_5.appendChild(btn_update);

		tr.appendChild(td);
		tr.appendChild(td_2);
		tr.appendChild(td_3);
		tr.appendChild(td_4);
		tr.appendChild(td_5);

		table.appendChild(tr);
	}

	getSections();

	//Prueba Paginador
	var i = document.createElement('i');
	i.setAttribute('class','angle left icon');
	var a = document.createElement('a');
	a.setAttribute('class','active item');
	a.setAttribute('id',0);
	a.appendChild(i);
	a.addEventListener('click', movePag);
	pagination.appendChild(a);
	var cAux = 0;
	for ( c = 0; c < cont_paginator; c++){
		cAux = c + 1;
		var a = document.createElement('a');
		if ( c == pagIndex){
			a.setAttribute("style", "background-color: lightblue;");
		}
		a.setAttribute('class','active item');
		a.setAttribute('id',c);
		a.textContent = cAux;
		a.addEventListener('click', movePag);
		pagination.appendChild(a);
	}
	var i = document.createElement('i');
	i.setAttribute('class','angle right icon');
	var a = document.createElement('a');
	a.setAttribute('class','active item');
	a.setAttribute('id',cAux-1);
	a.appendChild(i);
	a.addEventListener('click', movePag);
	pagination.appendChild(a);
};

//Prueba Pagination
function movePag(){
	pagIndex = this.id;
	cleanTable();
	getModules();
}

$('.ui.dimmer.modals.page.transition').modal({allowMultiple: false});

function del () {
	$('.second.modal.del').modal('attach events', '.first.modal.del #yes');
	$('.first.modal.del').modal('show');
	flag=this.data.id;
	//console.log(this.data.id);
};

function edit () {
	$('.second.modal#modal_save_ok').modal('attach events', '.first.modal.edit #save');
	$('.first.modal.edit').modal('show');
	$('.alert').text('Modulo Modificado');
	$('.modaltitle').text('Modificar');
	//console.log(this.data);
	txtName.value = this.data.name;
	txtDescription.value = this.data.description;
	txtUrl.value = this.data.url;
    fileIcon.value = null;
	flag = this.data.id;
	//console.log(flag);
};


$('#new').on('click',function(){
	$('.second.modal#modal_save_ok').modal('attach events', '.first.modal.edit #save');
	$('.first.modal.edit').modal('show');
	$('.alert').text('Modulo Agregado');
	$('.modaltitle').text('Agregar');
	txtName.value = "";
	txtDescription.value = "";
	txtUrl.value = "";
    fileIcon.value = null;
});

//GET - Index
function getModules () {
	 jQuery.ajax({
		 type: "GET",
		 url: "/api/modules",
		 contentType: 'application/json; charset=utf-8',
		 dataType: "json",
		 success: function (data) {
			 show(data);
		 },
		 error:	 function (jqXHR, status) {
				 // error handler
		 }
	})
};

function getSections () {
	 jQuery.ajax({
		 type: "GET",
		 url: "/api/sections/menu",
		 contentType: 'application/json; charset=utf-8',
		 dataType: "json",
		 success: function (data) {
		 	$("#section_id option").remove();
		 	var c = 0;
		 	$.each(data, function(id,name){
		 		console.log('cargado de sections');
				$("#section_id").append('<option value="'+data[c].id+'">'+data[c].name+'</option>');
				$('.loadSection'+data[c].id).text(data[c].name);
				c++;
		    });
		 },
		 error:	 function (jqXHR, status) {
				 // error handler
		 }
	})
};


$('#save').click(function addModule (ev) {
	if (flag === 0){
        //POST - Store
        jQuery.ajax({
            type: 'POST',
            url: '/api/modules',
            data: $('#form').serialize(),
            dataType: 'json',
            success: function (data) {
				cleanTable();
				getModules();
            },
            error: function (jqXHR, status) {
                // error handler
            }
        });
    }else if (flag > 0){
        //PUT - Update
        function updateModule (ev) {
			jQuery.ajax({
                type: 'PUT',
				url: '/api/modules/'+flag,
				data: $('#form').serialize(),
				dataType: 'json',
				success: function (data) {
                    cleanTable();
					getModules();
					flag = 0;
				},
				error: function (jqXHR, status) {
				    // error handler
                }
            });
        };
        updateModule();
    }
});

$('#yes').click(function deleteModule (ev) {
    jQuery.ajax({
        type: 'DELETE',
        url: '/api/modules/'+flag,
        data: $('#form').serialize(),
        dataType: 'json',
        success: function (data) {
            cleanTable();
            getModules();
            flag = 0;
        },
        error: function (jqXHR, status) {
            // error handler
        }
    });
});
