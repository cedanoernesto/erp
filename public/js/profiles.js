$('.ui.dropdown').dropdown();
var las_page_set = 0;
var tablaProfiles = document.getElementById('profiles');
var txtNombreProfile = document.getElementById('nombreProfile');
var txtDescripcionProfile = document.getElementById('descripcionProfile');
//  var txtImagen = document.getElementById("imagen");
var btnGuardar = document.getElementById('save');
var flag = 0;

$('.close.icon').on('click',function () {
    $(this).parent().parent().modal('hide');
});

//validaciones
    var letras = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s ]+$/;
    var numeros = /^[0-9]+$/;
    var revolver = /^[a-zA-Z0-9._-]+$/;
    var caracteres_esp = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g;
//finvalidaciones

getProfiles('/api/profiles/paginate');

function show (data) {
        //Vaciado de tabla y paginacion
    $('#profiles').empty();
    $('#pag').empty();
    let d = data.data;
    //Creacion de fila, columnas y llenado de columnas.
    for (let i = 0; i < d.length; i++) {
        $('#profiles').append('<tr>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td></td>' +
                            '</tr>');
        var child = i + 1;
        function getTd(x) {
            return $('#profiles tr:nth-child('+ child +') td:nth-child('+ x +')');
        }
        getTd(1).append(d[i].name);
        getTd(1).css('cursor', 'pointer');
        //getTd(1).css('text-decoration', 'underline');
        getTd(2).append(d[i].description);
        getTd(3).append("<button id='mod' class='ui right button'>Editar</button>");
        getTd(3).append("<button id='del' class='ui negative right button'>Eliminar</button>");

        getTd(3).find('#mod').on("click", function() {
            $('#form').form('clear');
            $('#save').attr("disabled", false);
            $('#messages').empty();
            $('.ui.modal#modal_edit')
                .modal({closable: false, observeChanges: true })
                .modal('show');
            $('.texti_2').text('Rol Editado');
            $('.texti').text('Editar');
            $('#nombreRole').val(d[i].name);
            $('#descripcionRole').val(d[i].description);
            edit(d[i]);
            $('#save').off().on("click", function(e) {
                e.preventDefault();
                $('#form').form('validate form');
                if ($('#form').form('is valid')) {
                    updateProfile(d[i].id, data.current_page);
                }
                //$('#modal_edit').modal('hide');
            });
        });

        getTd(3).find('#del').on("click", function() {
            //$('.second.modal#modal_del_ok').modal('attach events', '.first.modal .button#yes');
            $('#yes').attr("disabled", false);
            $('.first.modal#modal_del').modal({
                observeChanges: true,
                closable: false,
                onApprove: function () {
                    $('#yes').attr("disabled", true);
                    deleteProfile(d[i].id, data.current_page, data.last_page);
                }
            }).modal('show');
        });
    }
    if(data.current_page == 1 && data.current_page != data.last_page) {
        $('#pag').append("<a class='active item'>"+ data.current_page +"</a>" +
                         "<div class='disabled item'>...</div>" +
                         "<a class='item' >"+ data.last_page +"</a>" +
                         "<a class='item'><i class='angle right icon'></i></a>");
    }else if (data.last_page > 1 && data.current_page != data.last_page) {
        $('#pag').append("<a class='item'><i class='angle left icon'></i></a>" +
                         "<a class='active item'>"+ data.current_page +"</a>" +
                         "<div class='disabled item'>...</div>" +
                         "<a class='item' >"+ data.last_page +"</a>" +
                         "<a class='item'><i class='angle right icon'></i></a>");
    }else if(data.last_page > 1 && data.current_page == data.last_page){
        $('#pag').append("<a class='item'><i class='angle left icon'></i></a>" +
                         "<a class='active item'>"+ data.current_page +"</a>");
    }
    $('#pag a:nth-last-child(2)').on("click", function() {
        if(data.current_page != data.last_page) {
            getProfiles("/api/profiles/paginate?page="+ data.last_page);
        }
    });
    $('#pag a:first-child').on("click", function() {
        if (data.current_page > 1) {
            var prevIndexSlice = data.prev_page_url.indexOf('/', 8);
            getProfiles(data.prev_page_url.slice(prevIndexSlice));
        }
    });
    $('#pag a:last-child').on("click", function() {
        if (data.current_page != data.last_page) {
            var nextIndexSlice = data.next_page_url.indexOf('/', 8);
            getProfiles(data.next_page_url.slice(nextIndexSlice));
        }
    });
    last_page_set = data.last_page;
};

$('#modal_cont_del').modal({allowMultiple: false});
$('#modal_cont_edit').modal({allowMultiple: false});

function del () {
    $('.second.modal#modal_del_ok').modal('attach events', '.first.modal .button#yes');
    $('.first.modal#modal_del')
        .modal({closable: false, observeChanges: true })
        .modal('show');
    flag=this.data.id;
    //console.log(this.data.id);
};

function edit(data) {
    $('.second.modal#modal_save_ok').modal('attach events', '.first.modal .button#save');
    $('.ui.modal#modal_edit')
        .modal({closable: false, observeChanges: true })
        .modal('show');
    $('.texti_2').text('Perfil Editado');
    $('.texti').text('Editar');
    $('#nombreProfile').val(data.name);
    $('#descripcionProfile').val(data.description);
    $('#select_roles').dropdown("clear");
    data.roles.forEach(function (role) {
        $('#select_roles').dropdown("set selected", role.id);
    });
};

$('#new').on('click',function(){
    $('#save').attr("disabled", false);
    $('.second.modal#modal_save_ok').modal('attach events', '.first.modal .button#save');
    $('.ui.modal#modal_edit')
        .modal({closable: false, observeChanges: true })
        .modal('show');
    $('.texti_2').text('Perfil Agregado');
    $('.texti').text('Agregar');
    $('#nombreProfile').val("");
    $('#descripcionProfile').val("");
    $('#select_roles').dropdown("clear");
    $('#save').off().on("click", function(e) {
        e.preventDefault();
        if ($('#form').form('is valid')) {
            addProfile(last_page_set);
        }else {
            $('#form').form('validate form');
        }
    });
    //$('#form').form('reset');
});

//GET - Index
function getProfiles (u) {
     jQuery.ajax({
         type: "GET",
         url: u,
         contentType: 'application/json; charset=utf-8',
         dataType: "json",
         success: function (data) {
             show(data);
         },
         error:  function (jqXHR, status) {
                 // error handler
         }
    })
};

function addProfile(last_page) {
        $('#save').attr("disabled", true);
        //$('.second.modal#modal_save_ok').modal('show');
        jQuery.ajax({
            type: 'POST',
            url: '/api/profiles',
            data: {
                name: $('#nombreProfile').val(),
                roles: $('#select_roles').val(),
                description: $('#descripcionProfile').val(),
            },
            dataType: 'json',
            success: function (data) {
                new PNotify({
                    title: 'Operación exitosa',
                    text: 'Perfil agregado exitosamente',
                    type: 'success'
                });

                $('.ui.modal#modal_edit').modal('hide');

                getProfiles('/api/profiles/paginate?page='+ last_page);
            },
            error: function (jqXHR, status) {
                // error handler
            }
        });
       $('#form').form('clear');
       $('.ui.error.message').empty();
};
        //PUT - Update
function updateProfile(id, current_page) {
        $('#save').attr("disabled", true);
        //$('.second.modal#modal_save_ok').modal('show');
        jQuery.ajax({
            type: 'PUT',
            url: '/api/profiles/'+id,
            data: {
                name: $('#nombreProfile').val(),
                roles: $('#select_roles').val(),
                description: $('#descripcionProfile').val(),
            },
            dataType: 'json',
            success: function (data) {
                new PNotify({
                    title: 'Operación exitosa',
                    text: 'Perfil modificado exitosamente',
                    type: 'success'
                });

                $('.ui.modal#modal_edit').modal('hide');

                getProfiles('/api/profiles/paginate?page='+ current_page);
            },
            error: function (jqXHR, status) {
                // error handler
            }
        });
        $('.ui.error.message').empty();

};

$('#form').form({
    revalidate: true,
    fields: {
        name: {
            identifier: 'name',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Ingrese el nombre del perfil'
                },
                {
                    type   : 'maxLength[50]',
                    prompt : 'Sólo se permiten máximo 50 carácteres en Nombre'
                },
                {
                    type: 'regExp',
                    value: /^[a-zA-ZñÑ,. ÁÉÍÓÚáéíóúäëïöüÄËÏÖÜ\#&-\s]*$/,
                    prompt: 'El nombre no puede tener caracteres especiales ni números.'
                }
            ]
        },
        roles: {
            identifier: 'roles',
            rules: [
                {
                    type: 'minCount[1]',
                    prompt: 'Seleccione al menos un rol'
                }
            ]
        },
        description: {
            identifier: 'description',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Ingrese la descripción del perfil'
                },
                {
                    type: 'maxLength[190]',
                    prompt: 'Solo se permiten máximo 190 caracteres en Descripción.'
                },
                {
                    type: 'regExp',
                    value: /^[a-zA-ZñÑ,. ÁÉÍÓÚáéíóúäëïöüÄËÏÖÜ\#&-\s]*$/,
                    prompt: 'La descripción no puede tener caracteres especiales ni números.'
                }
            ]
        }
    }
});

function deleteProfile (id, current_page, last_page) {
    jQuery.ajax({
        type: 'DELETE',
        url: '/api/profiles/'+id,
        data: $('#form').serialize(),
        dataType: 'json',
        success: function (data) {
            if (current_page == last_page && current_page > 1 && $('#profiles tr').length == 1) {
                var prevPage = current_page - 1;
                getProfiles("/api/profiles/paginate?page=" + prevPage);
            }else {
                getProfiles("/api/profiles/paginate?page=" + current_page);
            }

            new PNotify({
                title: 'Operación exitosa',
                text: 'Perfil eliminado exitosamente',
                type: 'success'
            });
        },
        error: function (jqXHR, status) {
            // error handler
        }
    });
}

$('#no').click(function close () {
    $('#modal_del').modal('hide');
});

//validaciones
  validar = function(id_elemento,expresion){
        var elemento = getID(id_elemento);
        var valor = elemento.val();

       if(expresion.test(valor) ){
       $(elemento).css("border","0.2px solid rgba(34,36,38,.15)");
            return true;
        }
        else {
            $(elemento).css("border","0.2px solid rgba(34,36,38,.15)");
            return false;
        }
    }

    function getID( elemento ){
        return $(elemento);
    }
