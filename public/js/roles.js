var last_page_set = 0;
//validaciones
    var letras = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s ]+$/;
    var numeros = /^[0-9]+$/;
    var revolver = /^[a-zA-Z0-9._-]+$/;

getRoles("/api/roles/paginate");

$('.close.icon').on('click',function () {
    $(this).parent().parent().modal('hide');
});

function show (data) {
	//Vaciado de tabla y paginacion
	$('#roles').empty();
	$('#pag').empty();
	let d = data.data;
	//Creacion de fila, columnas y llenado de columnas.
	for (let i = 0; i < d.length; i++) {
		$('#roles').append('<tr>' +
								'<td></td>' +
								'<td></td>' +
								'<td></td>' +
							'</tr>');
		var child = i + 1;
		function getTd(x) {
			return $('#roles tr:nth-child('+ child +') td:nth-child('+ x +')');
		}
		getTd(1).append(d[i].name);
		getTd(1).css('cursor', 'pointer');
		//getTd(1).css('text-decoration', 'underline');
		getTd(2).append(d[i].description);
        getTd(3).append("<button id='mod' class='ui right button'>Editar</button>");
		getTd(3).append("<button id='del' class='ui negative right button'>Eliminar</button>");

        getTd(3).find('#mod').on("click", function() {
            $('#form').form('clear');
            $('#save').attr("disabled", false);
            $('#messages').empty();
			$('.ui.modal#modal_edit')
                .modal({closable: false, observeChanges: true })
                .modal('show');
			$('.texti_2').text('Rol Editado');
			$('.texti').text('Editar');
			$('#nombreRole').val(d[i].name);
			$('#descripcionRole').val(d[i].description);
            $('.message').empty();
			$('#save').off().on("click", function(e) {
                e.preventDefault();
                //$('#form').form('validate form');
				if ($('#form').form('is valid')) {
                    updateRole(d[i].id, data.current_page);   
                }else {
                    $('#form').form('validate form');
                }	
			});
		});

		getTd(3).find('#del').on("click", function() {
			//$('.second.modal#modal_del_ok').modal('attach events', '.first.modal .button#yes');
            $('#yes').attr("disabled", false);
            $('.first.modal#modal_del').modal({
                closable: false,
				observeChanges: true,
                onApprove: function () {
                    deleteRole(d[i].id, data.current_page, data.last_page);
                }
            }).modal('show');
		});
	}
	if(data.current_page == 1 && data.current_page != data.last_page) {
        $('#pag').append("<a class='active item'>"+ data.current_page +"</a>" +
                         "<div class='disabled item'>...</div>" +
                         "<a class='item' >"+ data.last_page +"</a>" +
                         "<a class='item'><i class='angle right icon'></i></a>");
	}else if (data.last_page > 1 && data.current_page != data.last_page) {
		$('#pag').append("<a class='item'><i class='angle left icon'></i></a>" +
					 	 "<a class='active item'>"+ data.current_page +"</a>" +
					 	 "<div class='disabled item'>...</div>" +
					 	 "<a class='item' >"+ data.last_page +"</a>" +
					 	 "<a class='item'><i class='angle right icon'></i></a>");
	}else if(data.last_page > 1 && data.current_page == data.last_page){
		$('#pag').append("<a class='item'><i class='angle left icon'></i></a>" +
					 	 "<a class='active item'>"+ data.current_page +"</a>");
	}
	$('#pag a:nth-last-child(2)').on("click", function() {
		if(data.current_page != data.last_page) {
			getRoles("/api/roles/paginate?page="+ data.last_page);
		}
	});
	$('#pag a:first-child').on("click", function() {
		if (data.current_page > 1) {
			var prevIndexSlice = data.prev_page_url.indexOf('/', 8);
			getRoles(data.prev_page_url.slice(prevIndexSlice));
		}
	});
	$('#pag a:last-child').on("click", function() {
		if (data.current_page != data.last_page) {
			var nextIndexSlice = data.next_page_url.indexOf('/', 8);
			getRoles(data.next_page_url.slice(nextIndexSlice));
		}
	});
	last_page_set = data.last_page;
};

$('#modal_cont_del').modal({allowMultiple: false});
$('#modal_cont_edit').modal({allowMultiple: false});

$('#form').on('submit', function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#save').trigger('click');
});

$('#new').on('click',function(){
	//$('.second.modal#modal_save_ok').modal('attach events', '.first.modal .button#save');
    $('#form').form('clear');
    $('#save').attr("disabled", false);
	$('#messages').empty();
	$('.ui.modal#modal_edit')
        .modal({closable: false, observeChanges: true })
        .modal('show');
	$('.texti_2').text('Rol Agregado');
	$('.texti').text('Agregar');
	$('#nombreRole').val("");
	$('#descripcionRole').val("");
     $('.message').empty();
	$('#save').off().on("click", function(e) {
        e.preventDefault();
		if ($('#form').form('is valid')) {
            addRole(last_page_set);    
        }else {
            $('#form').form('validate form');
        }
		
	});
});

//GET - Index
function getRoles (u) {
	 jQuery.ajax({
		 type: "GET",
		 url: u,
		 contentType: 'application/json; charset=utf-8',
		 dataType: "json",
		 success: function (data) {
			show(data);
		 },
		 error:	 function (jqXHR, status) {
				 // error handler
		 }
	})
};

function addRole (last_page) {
        $('#save').attr("disabled", true);
   		//$('.second.modal#modal_save_ok').modal('show');
	    //POST - Store
	    $.ajax({
	    	type: 'POST',
	        url: '/api/roles/',
	        data: $('#form').serialize(),
	        dataType: 'json',
	        success: function (data) {
                new PNotify({
                    title: 'Operación exitosa',
                    text: 'Rol agregado exitosamente',
                    type: 'success'
                });

                $('.ui.modal#modal_edit').modal('hide');

	        	getRoles('/api/roles/paginate?page='+ last_page);
	        },
	        error: function (jqXHR, status) {
	            // error handler
	        }
	    });
        $('#form').form('clear');
        $('.message').empty();
 }

 //PUT - Update
 function updateRole(id, current_page) {
        $('#save').attr("disabled", true);
    	//$('.second.modal#modal_save_ok').modal('show');
		$.ajax({
        	type: 'PUT',
			url: '/api/roles/'+id,
			data: $('#form').serialize(),
			dataType: 'json',
			success: function (data) {
                new PNotify({
                    title: 'Operación exitosa',
                    text: 'Rol modificado exitosamente',
                    type: 'success'
                });

                $('.ui.modal#modal_edit').modal('hide');

				getRoles('/api/roles/paginate?page='+ current_page);
			},
			error: function (jqXHR, status) {
				// error handler
            }
        });
        $('.message').empty();
}

$('#form').form({
    revalidate: true,
    fields: {
        name: {
            identifier: 'name',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Ingrese el nombre del rol'
                },
                {
                    type   : 'maxLength[50]',
                    prompt : 'Sólo se permiten máximo 50 carácteres en Nombre'
                },
                {
                    type: 'regExp',
                    value: /^[a-zA-ZñÑ,. ÁÉÍÓÚáéíóúäëïöüÄËÏÖÜ\#&-\s]*$/,
                    prompt: 'El nombre no puede tener caracteres especiales ni números.'
                }
            ]
        },
        description: {
            identifier: 'description',
            rules: [
                {
                    type   : 'empty',
                    prompt : 'Ingrese la descripción del rol'
                },
                {
                    type: 'maxLength[190]',
                    prompt: 'Solo se permiten máximo 190 caracteres en Descripción.'
                },
                {
                    type: 'regExp',
                    value: /^[a-zA-ZñÑ,. ÁÉÍÓÚáéíóúäëïöüÄËÏÖÜ\#&-\s]*$/,
                    prompt: 'La descripción no puede tener caracteres especiales ni números.'
                }
            ]
        }
    }
});

function deleteRole(id, current_page, last_page) {
    jQuery.ajax({
        type: 'DELETE',
        url: '/api/roles/'+id,
        data: $('#form').serialize(),
        dataType: 'json',
        success: function (data) {
        	if (current_page == last_page && current_page > 1 && $('#roles tr').length == 1) {
        		var prevPage = current_page - 1;
        		getRoles("/api/roles/paginate?page=" + prevPage);
        	}else {
        		getRoles("/api/roles/paginate?page=" + current_page);
        	}

            new PNotify({
                title: 'Operación exitosa',
                text: 'Rol eliminado exitosamente',
                type: 'success'
            });

        },
        error: function (jqXHR, status) {
            // error handler
        }
    });
}

$('#no').click(function close () {
    $('#modal_del').modal('hide');
    flag = 0;
});

//validaciones
  validar = function(id_elemento,expresion){
        var elemento = getID(id_elemento);
        var valor = elemento.val();

       if(expresion.test(valor) ){
       $(elemento).css("border","0.2px solid rgba(34,36,38,.15)");
            return true;
        }
        else {
            $(elemento).css("border","0.2px solid rgba(34,36,38,.15)");
            return false;
        }
    }

    function getID( elemento ){
        return $(elemento);
    }
