var pending = false;
var idInstitution = 0;
var idCurrency = 0;
var idPayType = 0;
var idRegistrationType;
var savedInitialCharacter;

var direccion = document.getElementById('direccion');
var contacto = document.getElementById('contacto');
var moneda = document.getElementById('moneda');
var matriculacion = document.getElementById('matriculacion');
var turno = document.getElementById('turno');

function cleanTable () { //Limpiar datos de institución
	while(direccion.childElementCount > 0) {direccion.removeChild(direccion.firstChild);}
	while(contacto.childElementCount > 0) {contacto.removeChild(contacto.firstChild);}
	while(moneda.childElementCount > 0) {moneda.removeChild(moneda.firstChild);}
	while(matriculacion.childElementCount > 0) {matriculacion.removeChild(matriculacion.firstChild);}
	while(turno.childElementCount > 0) {turno.removeChild(turno.firstChild);}
};

$('#picture').on('change', function (e) {
    if (this.files && this.files[0] && this.files[0].type.includes('image')) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#institution_img').attr('src', e.target.result);
            reader.readAsDataURL(this.files[0]);
        }
    } else {
        this.value = null
        new PNotify({
            title: 'Archivo no permitido',
            text: 'El archivo seleccionado debe ser de una imagen',
            type: 'error'
        });
        //$('#institution_img').attr('src', '/img/user.png');
    }
});

function show () {
    cleanTable();
    $.ajax({
        type: "GET",
        url: "/api/institutions",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (data) {

            if (data.length > 0){
                if (!!data[0].image_url) {
                    $('#instImg').attr('src', '/storage/' + data[0].image_url);
                } else {
                    $('#instImg').attr('src', '/img/image.png');
                }

                idInstitution = data[0].id;
                $('#instName span').text(data[0].name);
                var span = document.createElement('span');
                span.textContent = data[0].address + ', ' + data[0].neighborhood;
                direccion.appendChild(span);
                direccion.appendChild(document.createElement("br"));

                var span = document.createElement('span');
                span.textContent = "Codigo Postal: " + data[0].zip_code;
                direccion.appendChild(span);
                direccion.appendChild(document.createElement("br"));

                var city = data[0].city.name;
                var span = document.createElement('span');
                span.textContent = city + ", " + data[0].state.name + ", " + data[0].country.name;
                direccion.appendChild(span);

                var span = document.createElement('span');
                span.textContent = "Télefono: ";
                contacto.appendChild(span);

                var span = document.createElement('span');
                span.textContent = data[0].phone;
                contacto.appendChild(span);
                contacto.appendChild(document.createElement("br"));
                $.ajax({
                    type: "GET",
                    url: "/api/payments",
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function (data) {
                        for( i = 0; i < data.length; i++ ){
                            if(data[i].institution.id == idInstitution){
                                idPayments = data[i].id;
                                var span = document.createElement('span');
                                span.textContent = "Moneda: " + data[i].currency.name;
                                moneda.appendChild(span);
                                moneda.appendChild(document.createElement("br"));
                                var span = document.createElement('span');
                                span.textContent = "Tipo de pago: " +   data[i].types_payment.name;
                                moneda.appendChild(span);
                                moneda.appendChild(document.createElement("br"));
                                idCurrency = data[i].currency.id;
                                idPayType = data[i].types_payment.id;
                            }
                        }
                    },
                    error:	 function (jqXHR, status) {
                        $('.texti_2').text('¡Ha ocurrido un error!');
                    }
                });//Payments
                var span = document.createElement('span');
				span.textContent = "E-mail: " + data[0].email;
				contacto.appendChild(span);
				var span = document.createElement('span');
				span.textContent = "Estilo de matriculación: " + data[0].registration.type;
				matriculacion.appendChild(span);
				matriculacion.appendChild(document.createElement("br"));
				var span = document.createElement('span');
				span.textContent = "Caracter inicial: " + data[0].initial_character;
				matriculacion.appendChild(span);
				idTurno = data[0].shift_id;
				var span = document.createElement('span');
				span.textContent = "Turno: ";
				turno.appendChild(span);
				var span = document.createElement('span');
                span.textContent = data[0].shift.type;
				turno.appendChild(span);
            }else{
                var span = document.createElement('span');
                span.textContent = "Dirección, Calle";
                direccion.appendChild(span);
                direccion.appendChild(document.createElement("br"));
                var span = document.createElement('span');
                span.textContent = "Colonia, Codigo Postal (CP):0000";
                direccion.appendChild(span);
                direccion.appendChild(document.createElement("br"));
                var span = document.createElement('span');
                span.textContent = "Ciudad, Estado, País";
                direccion.appendChild(span);
                var span = document.createElement('span');
                span.textContent = "Télefono: (000) 000-0000";
                contacto.appendChild(span);
                contacto.appendChild(document.createElement("br"));
                var span = document.createElement('span');
                span.textContent = "E-mail: email@institución.extención";
                contacto.appendChild(span);
                var span = document.createElement('span');
                span.textContent = "Moneda: MXN";
                moneda.appendChild(span);
                moneda.appendChild(document.createElement("br"));
                var span = document.createElement('span');
                span.textContent = "Tipo de pago: Efectivo";
                moneda.appendChild(span);
                var span = document.createElement('span');
                span.textContent = "Estilo de matriculación: Numérica";
                matriculacion.appendChild(span);
                matriculacion.appendChild(document.createElement("br"));
                var span = document.createElement('span');
                span.textContent = "Caracter inicial: 123";
                matriculacion.appendChild(span);
                var span = document.createElement('span');
                span.textContent = "Turno: Vespertino / Matutino / Mixto";
                turno.appendChild(span);
            }
        },
        error:	 function (jqXHR, status) {
            // error handler
        }
    })
};

function getInfoModal () { //Llenar información del formulario
    $.ajax({
        type: "GET",
        url: "/api/institution_info",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (data) {
            var institution = data[0];
            var currencies = data[1];
            var registrations = data[2];
            var cities = data[3];
            var states = data[4];
            var countries = data[5];
            var types_payments = data[6];
            var shifts = data[7];


            if (institution[0] && !!institution[0].image_url) {
                $('#institution_img').attr('src', '/storage/' + institution[0].image_url);
            } else {
                $('#institution_img').attr('src', 'img/user.png');
            }

            for (var i = 0; i < currencies.length; i++ ){
                var option = document.createElement('option');
                option.setAttribute('value', currencies[i].id);
                option.textContent = currencies[i].name;
                option.id = currencies[i].id;
                $('#cmbCurrency').append(option);
            }
            for (var i = 0; i < registrations.length; i++) {
                var option = document.createElement('option');
                option.setAttribute('value', registrations[i].id);
                option.textContent = registrations[i].type;
                option.id = registrations[i].id;
                $('#cmbRegistration').append(option);
            }
            for (var i = 0; i < cities.length; i++) {
                var option = document.createElement('option');
                option.setAttribute('value', cities[i].id);
                option.textContent = cities[i].name;
                option.id = cities[i].id;
                $('#cmbCity').append(option);
            }
            for (var i = 0; i < states.length; i++) {
                var option = document.createElement('option');
                option.setAttribute('value', states[i].id);
                option.textContent = states[i].name;
                option.id = states[i].id;
                $('#cmbState').append(option);
            }
            for (var i = 0; i < types_payments.length; i++) {
                var option = document.createElement('option');
                option.setAttribute('value', types_payments[i].id);
                option.textContent = types_payments[i].name;
                option.id = types_payments[i].id;
                $('#cmbPayType').append(option);
            }
            for (var i = 0; i < shifts.length; i++) {
                var option = document.createElement('option');
                option.setAttribute('value', shifts[i].id);
                option.textContent = shifts[i].type;
                option.id = shifts[i].id;
                $('#cmbShift').append(option);
            }
            if(data[0].length > 0){
                $('#txtName').val(institution[0].name);
                $('#txtAddress').val(institution[0].address);
                $('#txtStreet').val(institution[0].neighborhood);
                $('#txtZipCode').val(institution[0].zip_code);
                $('#txtEmail').val(institution[0].email);
                $('#txtPhone').val(institution[0].phone);
                $('#txtInitialCharacter').val(institution[0].initial_character);
                savedInitialCharacter = institution[0].initial_character;
                idRegistrationType = institution[0].registration_id;
            }

            setTimeout(function () {
                if(data[0].length > 0){
                    $('#cmbState').dropdown('set selected', institution[0].state.id);
                    $('#cmbCity').dropdown('set selected', institution[0].city_id);
                    $('#cmbRegistration').dropdown('set selected', institution[0].registration_id);
                    $('#cmbCurrency').dropdown('set selected', idCurrency);
                    $('#cmbShift').dropdown('set selected', institution[0].shift_id);
                    $('#cmbPayType').dropdown('set selected', idPayType);

                    $('#txtInitialCharacter').val(institution[0].initial_character);
                    $('#frmInstitution').form('validate form');
                }
            }, 100);
        },
        error:	 function (jqXHR, status) {
            // error handler
        }
    });
};

$('#cmbState').change(function () {
    var city_id = 0;
    $.ajax({
         type: "GET",
         url: "/api/states/"+this.value+"/cities",
         contentType: 'application/json; charset=utf-8',
         dataType: "json",
         success: function (data) {
             $('#cmbCity').empty();
             var size = data.length;
             if (size > 0) {
                 for(var i = 0; i < size; i++){
                     if(i == 0) city_id = data[i].id;
                     var option = document.createElement('option');
                     option.setAttribute('value', data[i].id);
                     option.textContent = data[i].name;
                     option.id = data[i].id;
                     $('#cmbCity').append(option);
                 }
                 setTimeout(function() {
                     $('#cmbCity').dropdown('set selected', city_id);
                 }, 100);
             }
         },
         error:	 function (jqXHR, status) {
             $('.texti_2').text('¡Ha ocurrido un error!');
         }
    });
});

$('#cmbRegistration').change(function (e) {
	var val = $('#cmbRegistration').val();
    if (val == 1) {
        $('#txtInitialCharacter').val("ABC");
    } else if (val == 2) {
        $('#txtInitialCharacter').val("123");
    } else if (val == 3) {
        $('#txtInitialCharacter').val("A1B2");
    }

    if (val == idRegistrationType && savedInitialCharacter) {
        $('#txtInitialCharacter').val(savedInitialCharacter);
    }
});

$("#btnEdit").on("click", function() {
    getInfoModal();
    $('#edit_institution').modal({
        closable: false,
        blurring: false,
        onHide: function(){
            clearData();
            $('.ui.error.message').empty();
            $('.form .message').hide();
            $('form').form('clear');
        }
    }).modal('show');
});

$('.top .item').tab();

$('.ui.form')
  .form({
    fields: {
        name: {
            identifier: 'name',
            rules: [
              {
                type   : 'empty',
                prompt : 'Ingrese el nombre de la institución'
              },
                {
                    type   : 'maxLength[50]',
                    prompt : 'Sólo se permiten máximo 50 carácteres'
                },
                {
                    type: 'regExp',
                    value: /^[a-zA-Z0-9ñÑ,. ÁÉÍÓÚáéíóúäëïöüÄËÏÖÜ\#&-\s]*$/,
                    prompt: 'El nombre no puede tener caracteres especiales'
                }
            ]
        },
        neighborhood: {
            identifier: 'neighborhood',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Ingrese el nombre de la calle'
                },
                {
                type   : 'maxLength[50]',
                prompt : 'Sólo se permiten máximo 50 carácteres'
            },
            {
                type: 'regExp',
                value: /^[a-zA-Z0-9ñÑ,. ÁÉÍÓÚáéíóúäëïöüÄËÏÖÜ\#&-\s]*$/,
                prompt: 'La colonia no puede tener caracteres especiales'
            },
            {
                type: 'regExp',
                value: /(?!^\d+$)^.+$/,
                prompt: 'La colonia debe contener al menos un caracter'
            }
            ]
        },
        phone: {
            identifier: 'phone',
            rules: [
              {
                type   : 'empty',
                prompt : 'Ingrese el número de teléfono'
              },
              {
                type   : 'number',
                prompt : 'Sólo se permiten números'
              },
              {
                type   : 'maxLength[10]',
                prompt : 'Sólo se permiten máximo 10 números'
              },
              {
                type   : 'minLength[7]',
                prompt : 'Ingrese mínimo 7 dígitos para el campo teléfono'
              }
            ]
        },
        address: {
            identifier: 'address',
            rules: [
              {
                type   : 'empty',
                prompt : 'Ingrese el nombre de la colonia'
              },
                {
                type   : 'maxLength[50]',
                prompt : 'Sólo se permiten máximo 50 carácteres'
            },
            {
                type: 'regExp',
                value: /^[a-zA-Z0-9ñÑ,. ÁÉÍÓÚáéíóúäëïöüÄËÏÖÜ\#&-\s]*$/,
                prompt: 'La calle no puede tener caracteres especiales'
            },
            {
                type: 'regExp',
                value: /(?!^\d+$)^.+$/,
                prompt: 'La colonia debe contener al menos un caracter'
            }
            ]
        },
        city: {
            identifier: 'city',
            rules: [
              {
                type   : 'empty',
                prompt : 'Seleccione una ciudad'
              }
            ]
        },
        state: {
            identifier: 'state',
            rules: [
              {
                type   : 'empty',
                prompt : 'Seleccione un estado'
              }
            ]
        },
        zip_code:{
            identifier: 'zip_code',
            rules:[
                {
                    type: 'empty',
                    prompt: 'Ingrese el código postal'
                },
                {
                type   : 'exactLength[5]',
                prompt : 'Código Postal: Sólo se permiten 5 números'
              },
                {
                    type: 'regExp',
                    value: /^([0-9]+)?$/ ,
                    prompt: 'Código postal: Sólo se permiten números'
                }
            ]
        },
        email:{
            identifier: 'email',
            rules:[
                {
                    type: 'regExp',
                    value: /^((([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})))?$/,
                    prompt: 'El correo ingresado no es válido'
                },
                {
                    type   : 'empty',
                    prompt : 'Ingrese un correo electronico'
                },
                {
                    type   : 'maxLength[70]',
                    prompt : 'Sólo se permiten máximo 70 carácteres'
                }
            ]
        },
        currency:{
            identifier: 'currency',
            rules:[
                {
                    type: 'empty',
                    prompt: 'Seleccione un tipo de moneda'
                }
            ]
        },
        registration:{
            identifier: 'registration',
            rules:[
                {
                    type: 'empty',
                    prompt: 'Seleccione un tipo de matriculación'
                }
            ]
        },
        pay_type:{
            identifier: 'pay_type',
            rules:[
                {
                    type: 'empty',
                    prompt: 'Seleccione un tipo de pago'
                }
            ]
        },
        initial_character:{
            identifier: 'initial_character',
            rules:[
                {
                    type: 'empty',
                    prompt: 'Ingrese el carácter inicial para la matriculación'
                },
                {
                type   : 'maxLength[4]',
                prompt : 'Sólo se permiten máximo 4 carácteres'
              },
                {
                type   : 'minLength[3]',
                prompt : 'Sólo se permiten mínimo 3 carácteres'
            }
            ]
        },
        shift:{
            identifier: 'shift',
            rules:[
                {
                    type: 'empty',
                    prompt: 'Seleccione el turno de la institución'
                }
            ]
        }
    },
    onFailure: function() {
        $('.form .message').show();
    }
});

validar = function(id_elemento, expresion){
    var elemento = $(id_elemento);
    var valor = elemento.val();
    return expresion.test(valor);
}

$('#frmInstitution').on("submit", function(e){
    e.preventDefault();
    if (pending) return;
    pending = true;
    $('#btnSaveInst').addClass('disabled');

    var success = false;
    if ($('#cmbRegistration').val() == 1) {
        var letters = /^[a-zA-Z\s]+$/;
        if (!validar('#txtInitialCharacter', letters)) {
            $('.form').form('add prompt', 'initial_character');
            $('.form .error.message').html('Sólo se permiten letras').show();
        } else
            success = true;
    } else if ($('#cmbRegistration').val() == 2) {
        var numbers = /^[0-9]+$/;
        if (!validar('#txtInitialCharacter', numbers)) {
            $('.form').form('add prompt', 'initial_character');
            $('.form .error.message').html('Sólo se permiten números').show();
        } else
            success = true;
    } else if ($('#cmbRegistration').val() == 3) {
        var alpha_numeric = /^[a-zA-z0-9\s]+$/;
        var least_one_number_letter = /^(?!\d*$|[a-z]*$)[a-z\d]+$/i;
        if (!validar('#txtInitialCharacter', alpha_numeric)) {
            $('.form').form('add prompt', 'initial_character');
            $('.form .error.message').html('Sólo se permiten letras y números').show();
        }  else if (!validar('#txtInitialCharacter', least_one_number_letter)) {
            $('.form').form('add prompt', 'initial_character');
            $('.form .error.message').html('Matriculación alfanumérica debe contener al menos una letra y un número').show();
        } else
            success = true;
    }
    if($('#frmInstitution').form('is valid') && success){
        var frm = new FormData();
        frm.append('id', idInstitution);
        frm.append('name', $('#txtName').val());
        frm.append('status', 1);
        frm.append('address', $('#txtAddress').val());
        frm.append('neighborhood',$('#txtStreet').val());
        frm.append('registration_id', $("#cmbRegistration").val());
        frm.append('zip_code', $('#txtZipCode').val());
        frm.append('city_id', $('#cmbCity').val());
        frm.append('shift_id', $('#cmbShift').val());
        frm.append('phone',$('#txtPhone').val());
        frm.append('image_url', $('#picture')[0].files[0]);
        frm.append('email', $("#txtEmail").val());
        frm.append('initial_character', $("#txtInitialCharacter").val());

        if (idInstitution == 0) {
            $.ajax({
                type: 'POST',
                url : '/api/institutions',
                dataType: 'json',
                data: frm,
                processData: false,
                contentType: false,
                success : function (data) {
                    var frm2 = new FormData();
                    frm2.append('_method', 'post');
                    frm2.append('institution_id', data.id);
                    frm2.append('types_payment_id', $("#cmbPayType").val());
                    frm2.append('currency_id', $("#cmbCurrency").val());
                    $.ajax({
                        type: 'POST',
                        url : '/api/payments',
                        dataType: 'json',
                        data: frm2,
                        processData: false,
                        contentType: false,
                        success : function (data) {
                            new PNotify({
                                title: 'Operación exitosa',
                                text: 'Datos guardados exitosamente',
                                type: 'success'
                            });

                            $('#edit_institution').modal('hide');
                            clearData();
                            show();
                            pending = false;
                            $('#btnSaveInst').removeClass('disabled');
                        },
                        error: function(jqXHR, status){
                            $('.texti_2').text('¡Ha ocurrido un error!');
                            pending = false;
                            $('#btnSaveInst').removeClass('disabled');
                        }
                    });
                },
                error: function(jqXHR, status){
                    $('.texti_2').text('¡Ha ocurrido un error!');
                    pending = false;
                    $('#btnSaveInst').removeClass('disabled');
                }
            });
        } else if (idInstitution > 0) { //Editar institución
            frm.append('_method', 'put');
            $.ajax({
                type: 'POST',
                url : '/api/institutions/' + idInstitution,
                data : frm,
                processData: false,
                contentType: false,
                dataType : 'json',
                success : function (data) {
                    var frm2 = new FormData();
                    frm2.append('_method', 'put');
                    frm2.append('institution_id', idInstitution);
                    frm2.append('types_payment_id', $("#cmbPayType").val());
                    frm2.append('currency_id', $("#cmbCurrency").val());
                    $.ajax({
                        type: 'POST',
                        url : '/api/payments/'+idInstitution,
                        dataType: 'json',
                        data: frm2,
                        processData: false,
                        contentType: false,
                        success : function (data) {
                            new PNotify({
                                title: 'Operación exitosa',
                                text: 'Datos guardados exitosamente',
                                type: 'success'
                            });

                            $('#edit_institution').modal('hide');
                            clearData();
                            show();
                            pending = false;
                            $('#btnSaveInst').removeClass('disabled');
                        },
                        error: function(jqXHR, status){
                            $('.texti_2').text('¡Ha ocurrido un error!');
                            pending = false;
                            $('#btnSaveInst').removeClass('disabled');
                        }
                    });
                },
                error: function(jqXHR, status){
                    $('.texti_2').text('¡Ha ocurrido un error!');
                    pending = false;
                    $('#btnSaveInst').removeClass('disabled');
                }
            });
        }
    } else {
        pending = false;
        $('#btnSaveInst').removeClass('disabled');
    }
});

function clearData(){
    $('#txtName').val("");
    $('#txtAddress').val("");
    $('#txtStreet').val("");
    $('#txtZipCode').val("");
    $('#txtEmail').val("");
    $('#txtPhone').val("");
    $('#txtInitialCharacter').val("");
    $('#cmbState').empty();
    $('#cmbCity').empty();
    $('#cmbRegistration').empty();
    $('#cmbCurrency').empty();
    $('#cmbShift').empty();
    $('#cmbPayType').empty();
}

show();

$('.close.icon').on('click',function () {
    $(this).parent().parent().modal('hide');
});
