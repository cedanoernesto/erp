<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $modules = [
            [
                'name' => 'Usuarios',
                'description' => 'Usuarios',
                'url' => 'users',
                'icon' => '/img/icons/businesswoman.svg',
                'section_id' => 1,
                'editable' => 0,
            ],
            [
                'name' => 'Roles',
                'description' => 'Roles',
                'url' => 'roles',
                'icon' => '/img/icons/question.svg',
                'section_id' => 1
            ],
            [
                'name' => 'Perfiles',
                'description' => 'Perfiles',
                'url' => 'profiles',
                'icon' => '/img/icons/question.svg',
                'section_id' => 1
            ],
            [
                'name' => 'Módulos',
                'description' => 'Modulos',
                'url' => 'modules',
                'icon' => '/img/icons/puzzle.svg',
                'section_id' => 1,
                'editable' => 0,
            ],
            [
                'name' => 'Secciones',
                'description' => 'Secciones',
                'url' => 'sections',
                'icon' => '/img/icons/timeline.svg',
                'section_id' => 1,
                'editable' => 0,
            ],
            [
                'name' => 'Asignar módulos',
                'description' => 'Asignar modulos',
                'url' => 'assign',
                'icon' => '/img/icons/cursor.svg',
                'section_id' => 1
            ],
            [
                'name' => 'Institución',
                'description' => 'Instituciones',
                'url' => 'institutions',
                'icon' => '/img/icons/library.svg',
                'section_id' => 2
            ],
            [
                'name' => 'Infraestructuras',
                'description' => 'Infraestructura',
                'url' => 'infrastructures',
                'icon' => '/img/icons/building.svg',
                'section_id' => 2
            ],
            [
                'name' => 'Periodos',
                'description' => 'Periodos',
                'url' => 'periods',
                'icon' => '/img/icons/planner.svg',
                'section_id' => 2
            ],
            [
                'name' => 'Áreas',
                'description' => 'Áreas',
                'url' => 'areas',
                'icon' => '/img/icons/org_unit.svg',
                'section_id' => 2
            ],
            [
                'name' => 'Subáreas',
                'description' => 'Subáreas',
                'url' => 'subareas',
                'icon' => '/img/icons/subnodes.svg',
                'section_id' => 2
            ]
        ];

        foreach ($modules as $index => $module) {
            $modules[$index]['id'] = $index + 1;
            if (!isset($module['editable'])) {
                $modules[$index]['editable'] = 1;
            }
        }

        DB::table('modules')->insert($modules);

        $modules_roles = [];
        foreach ($modules as $index => $module) {
            $modules_roles[] = [
                'module_id' => $module['id'],
                'role_id' => 1
            ];
        }

        DB::table('module_role')->insert($modules_roles);
    }
}
