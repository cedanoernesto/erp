<?php

use Illuminate\Database\Seeder;

class UserDemoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'user' => 'admin',
            'email' => 'admin@aprendoo.com',
            'password' => \Hash::make('123456'),
            'profile_id' => 1,
            'editable' => 0
        ]);
    }
}
