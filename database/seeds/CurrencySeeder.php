<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies_path = storage_path() . "/seeds/currencies.json";
        $currencies = json_decode(file_get_contents($currencies_path), true);
        $currencies_array = [];

        foreach ($currencies as $code => $name) {
            $currencies_array[] = [
                'name' => "$code - $name"
            ];
        }

        DB::table('currencies')->insert($currencies_array);
    }
}
