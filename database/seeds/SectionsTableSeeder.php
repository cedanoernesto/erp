<?php

use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->insert([
            'name' => 'Sistema',
            'icon' => '',
            'editable' => 0,
            'level' => 0
        ]);

        DB::table('sections')->insert([
            'name' => 'Configuración',
            'icon' => '',
            'level' => 0
        ]);
    }
}
