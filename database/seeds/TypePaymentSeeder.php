<?php

use Illuminate\Database\Seeder;

class TypePaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types_payments = [
            [
                'name' => 'Efectivo',
                'description' => 'Pagos realizados en efectivo'
            ],
            [
                'name' => 'Deposito',
                'description' => 'Pagos realizados por depositos o transferencias bancarias'
            ],
            [
                'name' => 'Cheques',
                'description' => 'Pagos realizados por medio de cheques'
            ]
        ];

        DB::table('types_payments')->insert($types_payments);
    }
}
