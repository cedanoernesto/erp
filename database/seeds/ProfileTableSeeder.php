<?php

use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'id' => 1,
            'name' => 'Administrador',
            'description' => 'Administrador'
        ]);

        DB::table('profile_role')->insert([
            'profile_id' => 1,
            'role_id' => 1
        ]);
    }
}
