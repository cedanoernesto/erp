<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(ProfileTableSeeder::class);
        $this->call(UserDemoTableSeeder::class);
        $this->call(SectionsTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(MexicoStatesCitiesSeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(TypePaymentSeeder::class);
        $this->call(ShiftsSeeder::class);
        $this->call(RegistrationSeeder::class);

    }
}
