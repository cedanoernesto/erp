<?php

use Illuminate\Database\Seeder;

class ShiftsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shifts = [
            [ 'id' => '1', 'type' => 'Matutino' ],
            [ 'id' => '2', 'type' => 'Vespertino' ],
            [ 'id' => '3', 'type' => 'Mixto' ],
        ];

        DB::table('shifts')->insert($shifts);
    }
}
