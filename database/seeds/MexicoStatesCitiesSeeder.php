<?php

use Illuminate\Database\Seeder;

class MexicoStatesCitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'id' => 1,
            'name' => 'México'
        ]);

        $states_path = storage_path() . "/seeds/states.json";
        $states = json_decode(file_get_contents($states_path), true);

        foreach ($states as $index => $state) {
            $states[$index]['country_id'] = 1;
        }

        DB::table('states')->insert($states);

        $cities_path = storage_path() . "/seeds/cities.json";
        $cities = json_decode(file_get_contents($cities_path), true);

        DB::table('cities')->insert($cities);

    }
}
