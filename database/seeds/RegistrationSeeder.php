<?php

use Illuminate\Database\Seeder;

class RegistrationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $registrations = [
            ['id' => 1, 'type' => 'Alfabética', 'initial_character' => 'ABC'],
            ['id' => 2, 'type' => 'Numérica', 'initial_character' => '123'],
            ['id' => 3, 'type' => 'Alfanumérica', 'initial_character' => 'A1B2'],
        ];
        DB::table('registrations')->insert($registrations);
    }
}
