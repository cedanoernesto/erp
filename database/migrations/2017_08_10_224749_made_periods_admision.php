<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MadePeriodsAdmision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periods_admission', function (Blueprint $table) {
            $table->increments('id');
            $table->date('start_date');
            $table->date('end_date');
            $table->tinyInteger('status')->default('1');
            $table->integer('institution_id')->nullable()->unsigned();


//         $table->foreign('institution_id')->references('id')->on('institution');
         $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
  //      Schema::table('periods_admission', function(Blueprint $table) {
    //     $table->dropForeign(['institution_id']);
      //  });
        Schema::dropIfExists('periods_admission');
    }
}

