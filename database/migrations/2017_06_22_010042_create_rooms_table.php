<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /* Función ejecutada para crear la tabla en la base de datos.
        En esta se definen los campos que la compondran, junto con los tipos de dato que cada uno.
        Tambien las relaciones si esta llegace a contar*/
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->unsignedInteger('capacity');
            $table->string('ubication', 45);
            $table->tinyInteger('status')->length(1)->default(1);
            $table->unsignedInteger('infrastructure_id');
            $table->unsignedInteger('subarea_id');
            $table->timestamps();

            $table->foreign('infrastructure_id')->references('id')->on('infrastructures');
            $table->foreign('subarea_id')->references('id')->on('subareas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

     # Función ejecutada para elminar la tabla en la base de datos.
    public function down()
    {
        Schema::table('rooms', function (Blueprint $table) {
            $table->dropForeign(['infrastructure_id']);
            $table->dropForeign(['subarea_id']);
        });
        Schema::dropIfExists('rooms');
    }
}
