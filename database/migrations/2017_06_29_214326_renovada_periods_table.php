<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenovadaPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->date('start_date');
            $table->date('end_date');
            $table->tinyInteger('status')->default('1');
            $table->integer('inscription_period_id')->nullable()->unsigned();
            $table->integer('type_period_id')->nullable()->unsigned();


            $table->foreign('inscription_period_id')->references('id')->on('inscription_periods');
            $table->foreign('type_period_id')->references('id')->on('type_periods');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('periods', function(Blueprint $table) {
            $table->dropForeign(['inscription_period_id']);
            $table->dropForeign(['type_period_id']);
        });
        Schema::dropIfExists('periods');
    }
}
