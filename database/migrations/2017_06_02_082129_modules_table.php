<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /* Función ejecutada para crear la tabla en la base de datos.
        En esta se definen los campos que la compondran, junto con los tipos de dato que cada uno.
        Tambien las relaciones si esta llegace a contar*/
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('url');
            $table->string('icon');
            $table->tinyInteger('status')->length(1)->default(1);
            $table->tinyInteger('editable')->length(1)->default(1);
            $table->integer('section_id')->unsigned();
            $table->timestamps();

            $table->foreign('section_id')->references('id')->on('sections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

     # Función ejecutada para elminar la tabla en la base de datos.
    public function down()
    {
        Schema::table('modules', function(Blueprint $table) {
            $table->dropForeign('modules_section_id_foreign');
        });
        Schema::dropIfExists('modules');
    }
}
