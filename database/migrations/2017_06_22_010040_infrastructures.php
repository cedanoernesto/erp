<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Infrastructures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /* Función ejecutada para crear la tabla en la base de datos.
        En esta se definen los campos que la compondran, junto con los tipos de dato que cada uno.
        Tambien las relaciones si esta llegace a contar*/
    public function up()
    {
        Schema::create('infrastructures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->string('ubication', 45);
            $table->unsignedInteger('rooms');
            $table->unsignedInteger('floors');
            $table->tinyInteger('status')->length(1)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

     # Función ejecutada para elminar la tabla en la base de datos.
    public function down()
    {
        Schema::dropIfExists('infrastructures');
    }
}
