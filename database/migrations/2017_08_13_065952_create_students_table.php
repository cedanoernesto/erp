<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('students', function (Blueprint $table) {
          $table->increments('id');
          $table->string('enrollment')->length(45);
		  $table->integer('user_id')->unsigned();
          $table->tinyInteger('status')->length(1)->default(1);
          $table->integer('users_id')->unsigned();
          $table->timestamps();

          //$table->foreign('aspirants_id')->references('id')->on('aspirants');
          $table->foreign('user_id')->references('id')->on('users');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
