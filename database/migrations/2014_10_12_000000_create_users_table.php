<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            Schema::defaultStringLength(191);
            $table->increments('id');
            $table->string('user');
            $table->string('email');
            $table->string('password');
            $table->integer('profile_id')->unsigned();
            $table->rememberToken();
            $table->tinyInteger('status')->length(1)->default(1);
            $table->tinyInteger('editable')->length(1)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }

}
