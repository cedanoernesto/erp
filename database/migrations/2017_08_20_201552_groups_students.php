<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupsStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_student', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned();
            $table->integer('student_id')->unsigned();
            $table->tinyInteger('status')->length(1)->default(1);
            $table->timestamps();

            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('student_id')->references('id')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_student', function(Blueprint $table) {
            $table->dropForeign(['group_id']);
            $table->dropForeign(['student_id']);
        });
        Schema::dropIfExists('profile_role');
    }
}
