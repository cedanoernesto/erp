<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subareas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /* Función ejecutada para crear la tabla en la base de datos.
        En esta se definen los campos que la compondran, junto con los tipos de dato que cada uno.
        Tambien las relaciones si esta llegace a contar*/
    public function up()
    {
        Schema::create('subareas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->integer('area_id')->unsigned();
            $table->tinyInteger('status')->length(1)->default(1);
            $table->timestamps();

            $table->foreign('area_id')->references('id')->on('areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

     # Función ejecutada para elminar la tabla en la base de datos.
    public function down()
    {
        Schema::table('subareas', function (Blueprint $table) {
            $table->dropForeign(['area_id']);
        });
        Schema::dropIfExists('subareas');
    }
}
