<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('status')->length(1)->default(1);
            $table->string('address');
            $table->string('neighborhood');
            $table->string('phone')->length(13);
            $table->string('image_url');
            $table->string('email');
            $table->string('initial_character')->nullable();
            $table->integer('shift_id')->unsigned();
            $table->integer('registration_id')->unsigned();
            $table->integer('zip_code')->length(11);
            $table->integer('city_id')->unsigned();
            $table->timestamps();

            $table->foreign('registration_id')->references('id')->on('registrations');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('shift_id')->references('id')->on('shifts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutions');

    }
}
