<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModulesRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /* Función ejecutada para crear la tabla en la base de datos.
        En esta se definen los campos que la compondran, junto con los tipos de dato que cada uno.
        Tambien las relaciones si esta llegace a contar*/
    public function up()
    {
        Schema::create('module_role', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('module_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->timestamps();

            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

     # Función ejecutada para elminar la tabla en la base de datos.
    public function down()
    {
        Schema::table('module_role', function(Blueprint $table) {
            $table->dropForeign('module_role_module_id_foreign');
            $table->dropForeign('module_role_role_id_foreign');
        });
        Schema::dropIfExists('module_role');
    }
}
