<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buildings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->unsignedInteger('floors');
            $table->tinyInteger('status')->lenght(1)->default(1);
            $table->unsignedInteger('school_id');
            $table->timestamps();

            //$table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buildings', function(Blueprint $table) {
            //$table->dropForeign(['school_id']);
        });
        Schema::dropIfExists('buildings');
    }
}
