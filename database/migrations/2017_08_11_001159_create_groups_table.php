<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->length(45);
            $table->tinyInteger('status')->length(1)->default(1);
            $table->integer('carrer_id')->unsigned();
            $table->integer('period_id')->unsigned();
            $table->timestamps();

            $table->foreign('carrer_id')->references('id')->on('carrers');
            $table->foreign('period_id')->references('id')->on('periods');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->dropForeign(['carrer_id']);
            $table->dropForeign(['period_id']);
        });

        Schema::dropIfExists('groups');

    }
}
