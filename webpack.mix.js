const { mix } = require('laravel-mix');
const fs = require('fs');
const path = require('path');

var scriptsPaths = 'resources/assets/js';
var stylesPaths = 'resources/assets/css';

try {
    function getFolders (base_dir, find, callback, acc_folder = '') {
        var folders = fs.readdirSync(base_dir)
            .filter(function (file) {
                return fs.statSync(path.join(base_dir, file)).isDirectory();
            });

        folders.forEach(function (folder) {
            if (fs.existsSync(base_dir + '/' + folder + '/' + find)) {
                callback(acc_folder + '/' + folder);
            } else {
                getFolders(base_dir + '/' + folder, find, callback, acc_folder + folder);
            }
        });
    }

    /*
     |--------------------------------------------------------------------------
     | Mix Asset Management
     |--------------------------------------------------------------------------
     |
     | Mix provides a clean, fluent API for defining some Webpack build steps
     | for your Laravel application. By default, we are compiling the Sass
     | file for the application as well as bundling up all the JS files.
     |
     */
    getFolders(scriptsPaths + '/modules', 'index.js', function (folder) {
        mix.js(
            scriptsPaths + '/modules/' + folder + '/index.js',
            'public/js/modules/' + folder + '.js'
        );
    });

    mix.scripts(scriptsPaths + '/vendor/*.js', 'public/js/vendor.js');


    mix.styles(stylesPaths + '/vendor/*.css', stylesPaths + '/vendor.css');
    mix.styles([
        stylesPaths + '/normalize.css',
        stylesPaths + '/vendor.css',
        stylesPaths + '/main.css'
    ], 'public/css/master.css');

    getFolders(stylesPaths + '/modules', 'styles.css', function (folder) {
        mix.styles(
            [stylesPaths + '/modules/' + folder + '/styles.css'],
            'public/css/' + folder + '.css'
        );
    });
} catch(e) {
    console.log(e);
}
